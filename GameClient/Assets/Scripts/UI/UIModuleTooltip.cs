using Model;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UIModuleTooltip : MonoBehaviour
    {
        public TextMeshProUGUI Damage;
        public TextMeshProUGUI Delay;
        public TextMeshProUGUI Description;
        public TextMeshProUGUI Hp;

        [Header("Image labels")] public Image Icon;

        [Header("Text labels")] public TextMeshProUGUI Name;

        public TextMeshProUGUI Priority;

        public void Setup(Slot slot)
        {
            if (slot.HasModule)
            {
                var module = slot.ModuleState;

                Setup(slot.Module,
                    module.hp,
                    module.damage,
                    module.priority);
            }
        }

        public void Setup(ModuleMeta moduleMeta)
        {
            Setup(moduleMeta,
                moduleMeta.base_hp,
                moduleMeta.base_damage,
                moduleMeta.base_priority);
        }

        private void Setup(ModuleMeta moduleMeta,
            float currentHp,
            float currentDamage,
            float currentPriority)
        {
            if (Name != null)
                Name.text = moduleMeta.name;

            if (Description != null)
                Description.text = moduleMeta.description;

            if (Damage != null)
                Damage.text = ModText(moduleMeta.base_damage, currentDamage - moduleMeta.base_damage);

            if (Priority != null)
                Priority.text = ModText(moduleMeta.base_priority, currentPriority - moduleMeta.base_priority);

            if (Hp != null)
                Hp.text = HpText(moduleMeta.base_hp, currentHp);

            if (Delay != null)
                Delay.text = ModText(moduleMeta.fire_delay);

            if (Icon != null)
                Icon.sprite = Database.Instance.GetModuleDbInfo(moduleMeta.id)?.Icon;
        }

        private string ModText(float baseValue, float modValue = 0f, string goodColor = "green",
            string badColor = "red")
        {
            if (modValue > 0.1f)
                return $"{baseValue:0.#}<color={goodColor}>+{modValue:0.#}</color>";

            if (modValue < -0.1f)
                return $"{baseValue:0.#}<color={badColor}>{modValue:0.#}</color>";

            return baseValue.ToString("0.#");
        }

        private string HpText(float baseValue, float currentValue)
        {
            if (currentValue < baseValue)
                return $"<color=red>{currentValue:0.#}</color>";

            if (currentValue > baseValue)
                return $"<color=green>{currentValue:0.#}</color>";

            return currentValue.ToString("0.#");
        }
    }
}