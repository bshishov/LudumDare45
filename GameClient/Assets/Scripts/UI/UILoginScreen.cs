﻿using System.Collections.Generic;
using Model;
using Network;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI
{
    [RequireComponent(typeof(UICanvasGroupFader))]
    public class UILoginScreen : Singleton<UILoginScreen>
    {
        private const string LastHost = "LastHost";

        private const int MaxNameLength = 20;

        private UICanvasGroupFader _fader;
        private string _selectedHost = Common.LocalHost;
        public TMP_InputField NameField;
        public Button PlayButton;
        public TMP_Dropdown ServerSelectDropdown;

        [Header("Debug")] public UICanvasGroupFader ServerSelectFader;

        public TextMeshProUGUI StatusLabel;


        private void Awake()
        {
            _fader = GetComponent<UICanvasGroupFader>();
        }

        private void Start()
        {
            if (NameField != null)
            {
                NameField.onValueChanged.AddListener(NameChanged);

                if (PlayerPrefs.HasKey(Common.PlayerPrefsKeys.PlayerName))
                    NameField.text = PlayerPrefs.GetString(Common.PlayerPrefsKeys.PlayerName);
            }

            if (ServerSelectDropdown != null)
            {
                ServerSelectDropdown.ClearOptions();
                if (PlayerPrefs.HasKey(LastHost))
                {
                    _selectedHost = PlayerPrefs.GetString(LastHost);

                    ServerSelectDropdown.AddOptions(new List<string>
                    {
                        "LAST " + _selectedHost
                    });
                }

                ServerSelectDropdown.AddOptions(new List<string>
                {
                    "LOCAL " + Common.LocalHost,
                    "DEV " + Common.DebugHost,
                    "PROD: " + Common.ProdHost
                });

                ServerSelectDropdown.value = 0;
                ServerSelectDropdown.onValueChanged.AddListener(OnServerSelected);
            }

            if (PlayButton != null)
                PlayButton.onClick.AddListener(OnPlayClicked);

            Connection.Instance.Connected += OnConnected;
            Connection.Instance.Disconnected += OnDisconnected;
        }

        private void OnServerSelected(int index)
        {
            if (ServerSelectDropdown != null)
            {
                var parts = ServerSelectDropdown.options[index].text.Split(' ');
                if (parts.Length > 0)
                {
                    var selectedValue = parts[1].Trim();

                    if (!string.IsNullOrEmpty(selectedValue))
                    {
                        _selectedHost = selectedValue;
                        Debug.Log($"SELECTED server #{index}: {_selectedHost}");
                    }
                }
            }
        }

        private void OnDisconnected()
        {
            SetStatus("Disconnected");
            Show();
        }

        private void OnConnected()
        {
            SetStatus("Connected");
            Login();
        }

        private void Login()
        {
            string token = null;
            if (PlayerPrefs.HasKey(Common.PlayerPrefsKeys.AuthToken))
            {
                token = PlayerPrefs.GetString(Common.PlayerPrefsKeys.AuthToken);
                if (token == "")
                    token = null;
            }

            Connection.Instance.Send(new C2SHello
            {
                name = NameField.text,
                token = token,
                version = Database.Instance.Meta.version
            });
        }


        private void NameChanged(string value)
        {
            if (PlayButton != null)
                PlayButton.interactable = IsValidName();
        }

        private void SetStatus(string status)
        {
            if (StatusLabel != null)
                StatusLabel.text = status;
        }

        private bool IsValidName()
        {
            if (NameField == null)
                return false;

            var s = NameField.text;
            if (s == null)
                return false;

            if (s.Length >= MaxNameLength)
                return false;

            s = s.Trim();
            return !string.IsNullOrEmpty(s);
        }

        public void Show()
        {
            _fader.FadeIn();
        }

        public void Hide()
        {
            _fader.FadeOut();
        }

        [ContextMenu("Login")]
        private void OnPlayClicked()
        {
            if (IsValidName())
            {
                SetStatus("Connecting...");
                Connection.Instance.Connect(_selectedHost);
                PlayerPrefs.SetString(LastHost, _selectedHost);
            }
        }
    }
}