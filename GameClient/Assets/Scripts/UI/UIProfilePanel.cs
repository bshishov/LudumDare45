using Model;
using TMPro;
using UnityEngine;

namespace UI
{
    public class UIProfilePanel : MonoBehaviour
    {
        private Profile _current;
        public TextMeshProUGUI LossesLabel;
        public TextMeshProUGUI NameLabel;
        public TextMeshProUGUI RatingLabel;
        public TextMeshProUGUI WinsLabel;

        private void SetName(string value)
        {
            if (NameLabel != null)
                NameLabel.text = value;
        }

        private void SetWins(int value)
        {
            if (WinsLabel != null)
                WinsLabel.text = $"Wins: {value}";
        }

        private void SetRating(int value)
        {
            if (RatingLabel != null)
                RatingLabel.text = $"SR {value}";
        }

        private void SetLosses(int value)
        {
            if (LossesLabel != null)
                LossesLabel.text = $"Losses: {value}";
        }

        public void SetProfile(Profile profile)
        {
            if (profile == null)
            {
                SetName("-----");
                SetWins(0);
                SetLosses(0);
                SetRating(1000);
            }
            else
            {
                SetName(profile.name);
                SetWins(profile.games_won);
                SetLosses(profile.games_lost);
                SetRating(Mathf.RoundToInt(profile.rating));
            }
        }
    }
}