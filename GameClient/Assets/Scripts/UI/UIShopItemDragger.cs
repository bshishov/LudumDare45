using UI.DragDrop;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI
{
    [RequireComponent(typeof(UIModuleItem))]
    public class UIShopItemDragger : Dragger<ModuleDragData>
    {
        private UIModuleItem _item;
        public GameObject UIModuleItemPrefab;

        private void Start()
        {
            _item = GetComponent<UIModuleItem>();
        }

        protected override ModuleDragData DragStarted(PointerEventData e)
        {
            if (_item != null && UIModuleItemPrefab != null && _item.ModuleData != null)
            {
                // No dragging for pricey modules
                if (_item.ModuleData.price > GameManager.Instance.Money)
                    return null;

                var canvas = UISlotsOverlay.Instance.transform.root.GetComponent<Canvas>();
                var go = Instantiate(UIModuleItemPrefab, canvas.transform);

                var moduleItem = go.GetComponent<UIModuleItem>();
                if (moduleItem != null)
                {
                    moduleItem.Setup(_item.ModuleData);
                    return new ModuleDragData(go.transform, moduleItem, null, ModuleDragSource.Shop);
                }
            }

            return null;
        }

        protected override void DragWasReceived(DragReceiver<ModuleDragData> receiver, ModuleDragData data)
        {
            Destroy(data.Transform.gameObject);
        }

        protected override void DragWasCancelled(ModuleDragData data)
        {
            Destroy(data.Transform.gameObject);
        }
    }
}