using Model;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI
{
    [RequireComponent(typeof(UICanvasGroupFader))]
    public class UIGameOverScreen : Singleton<UIGameOverScreen>
    {
        private UICanvasGroupFader _fader;
        public TextMeshProUGUI SrText;
        public Button ToMenuButton;
        public TextMeshProUGUI WonLostText;

        private void Start()
        {
            _fader = GetComponent<UICanvasGroupFader>();
            ToMenuButton.onClick.AddListener(ToMenuClicked);
        }

        private void ToMenuClicked()
        {
            GameManager.Instance.ToMenu();
        }

        public void Show(int winner, Profile profile)
        {
            if (winner == 0)
                WonLostText.text = "Draw!";
            else if (winner == GameManager.Instance.PlayerIndex)
                WonLostText.text = "You won!";
            else
                WonLostText.text = "You lost!";

            SrText.text = $"New SR: {Mathf.Round(profile.rating)}";
            _fader.FadeIn();
        }

        public void Hide()
        {
            _fader.FadeOut();
        }
    }
}