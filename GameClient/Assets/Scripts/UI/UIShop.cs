﻿using System.Collections.Generic;
using Model;
using Network;
using UI.DragDrop;
using UnityEngine;

namespace UI
{
    public class UIShop : DragReceiver<ModuleDragData>
    {
        private readonly List<UIModuleItem> _items = new List<UIModuleItem>();
        public Transform TargetContainer;
        public GameObject UIModulePrefab;

        private void Start()
        {
            foreach (var moduleMeta in Database.Instance.Modules.Values)
                if (moduleMeta.can_be_bought)
                    Add(moduleMeta);

            Connection.Instance.MessageReceived.AddListener<S2CMoneyUpdated>(OnMoneyUpdated);
        }

        private void OnMoneyUpdated(S2CMoneyUpdated money)
        {
            foreach (var uiModuleItem in _items)
                if (uiModuleItem.ModuleData.price <= money.new_amount)
                    uiModuleItem.Fade(0f);
                else
                    uiModuleItem.Fade(0.75f);
        }

        private void Add(ModuleMeta module)
        {
            var newItem = Instantiate(UIModulePrefab, TargetContainer);
            var uiModuleItem = newItem.GetComponent<UIModuleItem>();
            if (uiModuleItem != null)
                uiModuleItem.Setup(module);
            else
                Debug.LogWarning($"Module item prefab {UIModulePrefab.name} does not have a UIModuleItem component");

            _items.Add(uiModuleItem);
        }

        public override bool CanReceive(ModuleDragData data)
        {
            return data.Source == ModuleDragSource.Field && data.Slot != null;
        }

        public override void Receive(ModuleDragData data)
        {
            Connection.Instance.Send(new C2SModuleSell
            {
                slot = data.Slot.Id
            });
        }
    }
}