﻿using Model;
using Network;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI
{
    [RequireComponent(typeof(UICanvasGroupFader))]
    public class UIMainMenuScreen : Singleton<UIMainMenuScreen>
    {
        private UICanvasGroupFader _fader;
        public Button ExitButton;
        public UIProfilePanel MainProfile;
        public Button SettingsButton;
        public Button StartBotMatchMakingButton;
        public Button StartMatchMakingButton;

        private void Awake()
        {
            _fader = GetComponent<UICanvasGroupFader>();

            StartMatchMakingButton.onClick.AddListener(StartMatchMaking);
            SettingsButton.onClick.AddListener(UISettingsScreen.Instance.Show);
            ExitButton.onClick.AddListener(Application.Quit);

            StartBotMatchMakingButton.onClick.AddListener(StartBotMatchMaking);

            Connection.Instance.Disconnected += _fader.FadeOut;
        }

        private void StartBotMatchMaking()
        {
            Connection.Instance.Send(new C2SStartMatchmaking
            {
                queue_type = QueueType.BotMatch
            });
        }

        [ContextMenu("Start MM")]
        public void StartMatchMaking()
        {
            Connection.Instance.Send(new C2SStartMatchmaking
            {
                queue_type = QueueType.RankedMatch
            });
        }

        public void Show()
        {
            _fader.FadeIn();
        }

        public void Hide()
        {
            _fader.FadeOut();
        }
    }
}