using Model;
using Network;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI
{
    [RequireComponent(typeof(UICanvasGroupFader))]
    public class UIRoundToolbar : Singleton<UIRoundToolbar>
    {
        private UICanvasGroupFader _fader;
        public Image P1Ready;
        public Image P2Ready;
        public TextMeshProUGUI Player1;
        public UIRoundStatPanel Player1Rounds;
        public TextMeshProUGUI Player2;
        public UIRoundStatPanel Player2Rounds;
        public Button ReadyButton;
        public UICanvasGroupFader ReadyCanvas;
        public TextMeshProUGUI RoundNumber;
        public UITimer Timer;

        private void Awake()
        {
            _fader = GetComponent<UICanvasGroupFader>();

            if (ReadyButton != null) ReadyButton.onClick.AddListener(ReadyPressed);
        }

        private void ReadyPressed()
        {
            Connection.Instance.Send(new C2SReady());

            if (ReadyButton != null)
                ReadyButton.interactable = false;
        }

        private void Start()
        {
            Connection.Instance.MessageReceived.AddListener<S2CPlayerReady>(OnPlayerReady);
        }

        private void OnPlayerReady(S2CPlayerReady e)
        {
            if (e.player_index == GameManager.Instance.PlayerIndex)
                P1Ready.gameObject.SetActive(true);
            else
                P2Ready.gameObject.SetActive(true);
        }

        public void SetTimer(float time)
        {
            if (Timer != null)
                Timer.Countdown(time);

            if (ReadyButton != null)
                ReadyButton.interactable = true;

            if (P1Ready != null)
                P1Ready.gameObject.SetActive(false);

            if (P2Ready != null)
                P2Ready.gameObject.SetActive(false);

            if (ReadyCanvas != null)
                ReadyCanvas.FadeIn();
        }

        public void StopAndHideTimer()
        {
            if (Timer != null)
                Timer.StopAndHide();

            if (ReadyButton != null)
                ReadyButton.interactable = false;

            if (ReadyCanvas != null)
                ReadyCanvas.FadeOut();
        }

        public void Show()
        {
            _fader.FadeIn();
        }

        public void Hide()
        {
            _fader.FadeOut();
        }

        public void SetRound(int round)
        {
            if (RoundNumber != null)
                RoundNumber.text = round.ToString();
        }

        public void SetRoundsToWin(int roundsToWin)
        {
            Player1Rounds.Setup(roundsToWin);
            Player2Rounds.Setup(roundsToWin);
        }

        public void UpdateRoundHistory(int[] eHistory)
        {
            var p1Index = 0;
            var p2Index = 0;
            for (var i = 0; i < eHistory.Length; i++)
            {
                var winner = eHistory[i];
                var roundShip = Database.Instance.Meta.ship;

                if (winner == GameManager.Instance.PlayerIndex)
                {
                    Player1Rounds.Set(p1Index++, roundShip);
                }
                else if (winner == 0)
                {
                    Player1Rounds.Set(p1Index++, roundShip);
                    Player2Rounds.Set(p2Index++, roundShip);
                }
                else
                {
                    Player2Rounds.Set(p2Index++, roundShip);
                }
            }
        }
    }
}