﻿using TMPro;
using UnityEngine;

namespace UI
{
    public class UIPopup : MonoBehaviour
    {
        private Color _color;

        private float _remaining;
        public float FadeTime = 1f;
        public Vector2 RandomPosition = Vector2.zero;
        public float ShowTime = 1f;
        public Vector2 Speed = Vector3.up;
        public TextMeshProUGUI Text;

        private void Start()
        {
            Destroy(gameObject, ShowTime + FadeTime);

            transform.position += new Vector3(
                Random.Range(-RandomPosition.x, RandomPosition.x),
                Random.Range(-RandomPosition.y, RandomPosition.y),
                0);

            _remaining = ShowTime;
        }

        public void Setup(string text, Color color)
        {
            if (Text != null)
            {
                Text.text = text;
                Text.color = color;
                _color = color;
            }
        }

        private void Update()
        {
            if (Text == null || string.IsNullOrEmpty(Text.text))
                return;

            _remaining -= Time.deltaTime;
            if (_remaining < 0f)
            {
                if (FadeTime > 0)
                    _color.a -= Time.deltaTime / FadeTime;
                else
                    _color.a = 0;

                Text.color = _color;
            }

            transform.position += (Vector3) Speed * Time.deltaTime;
        }
    }
}