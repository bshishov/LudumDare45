using UnityEngine;

namespace UI.DragDrop
{
    public interface IDragData
    {
        Transform Transform { get; }
        Dragger DraggedBy { get; set; }
    }
}