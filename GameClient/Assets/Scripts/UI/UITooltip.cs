﻿using Model;
using UnityEngine;
using Utils;

namespace UI
{
    [RequireComponent(typeof(UICanvasGroupFader))]
    public class UITooltip : Singleton<UITooltip>
    {
        private GameObject _activeTooltip;
        private Canvas _canvas;

        private UICanvasGroupFader _fader;
        public UIModuleTooltip ModuleTooltip;

        private void Awake()
        {
            _fader = GetComponent<UICanvasGroupFader>();
            _canvas = _fader.GetComponentInParent<Canvas>();

            if (ModuleTooltip != null)
                ModuleTooltip.gameObject.SetActive(false);

            Hide();
        }

        private void Update()
        {
            if (_activeTooltip != null)
            {
                var mouse = Input.mousePosition;
                mouse += new Vector3(20f, 0, 0);

                var height = ((RectTransform) _activeTooltip.transform).rect.height * _canvas.scaleFactor;
                var width = ((RectTransform) _activeTooltip.transform).rect.width * _canvas.scaleFactor;

                if (mouse.x > Screen.width - width)
                    mouse += new Vector3(-width - 40f, 0, 0);

                if (mouse.y < height)
                    mouse = new Vector3(mouse.x, height, 0);


                transform.position = mouse;
            }
        }

        private void HideCurrentTooltip()
        {
            if (_activeTooltip != null)
            {
                _activeTooltip.SetActive(false);
                _activeTooltip = null;
            }
        }

        public void Show(Slot state)
        {
            if (ModuleTooltip != null && state != null && state.HasModule)
            {
                HideCurrentTooltip();
                ModuleTooltip.gameObject.SetActive(true);
                ModuleTooltip.Setup(state);
                _activeTooltip = ModuleTooltip.gameObject;
                _fader.FadeIn();
            }
        }

        public void Show(ModuleMeta state)
        {
            if (ModuleTooltip != null && state != null)
            {
                HideCurrentTooltip();
                ModuleTooltip.gameObject.SetActive(true);
                ModuleTooltip.Setup(state);
                _activeTooltip = ModuleTooltip.gameObject;
                _fader.FadeIn();
            }
        }

        public void Hide()
        {
            HideCurrentTooltip();
            _fader.FadeOut();
        }
    }
}