using TMPro;
using UnityEngine;

namespace UI
{
    public class UIHealthBar : MonoBehaviour
    {
        public TextMeshProUGUI HpText;
        public TextMeshProUGUI PlayerNameText;
        public UIProgressBar ProgressBar;
    }
}