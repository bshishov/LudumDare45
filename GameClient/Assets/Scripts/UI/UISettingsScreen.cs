using Network;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI
{
    [RequireComponent(typeof(UICanvasGroupFader))]
    public class UISettingsScreen : Singleton<UISettingsScreen>
    {
        private UICanvasGroupFader _fader;
        public Button DisconnectButton;
        public Button ExitButton;
        public Button OkButton;

        private void Start()
        {
            _fader = GetComponent<UICanvasGroupFader>();

            if (OkButton != null)
                OkButton.onClick.AddListener(_fader.FadeOut);

            if (ExitButton != null)
                ExitButton.onClick.AddListener(Application.Quit);

            if (DisconnectButton != null)
                DisconnectButton.onClick.AddListener(() =>
                {
                    _fader.FadeOut();
                    Connection.Instance.Close();
                });
        }

        private void Update()
        {
            if (Input.GetButtonDown(Common.InputAxes.Menu))
            {
                if (_fader.State == UICanvasGroupFader.FaderState.FadedIn)
                    _fader.FadeOut();
                else if (_fader.State == UICanvasGroupFader.FaderState.FadedOut)
                    _fader.FadeIn();
            }
        }

        public void Show()
        {
            _fader.FadeIn();
        }

        public void Hide()
        {
            _fader.FadeOut();
        }
    }
}