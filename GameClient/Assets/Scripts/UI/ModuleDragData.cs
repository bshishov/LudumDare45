using Model;
using UI.DragDrop;
using UnityEngine;

namespace UI
{
    public enum ModuleDragSource
    {
        Field,
        Shop
    }

    public class ModuleDragData : IDragData
    {
        public readonly ModuleMeta Module;
        public readonly UIModuleItem ModuleItem;
        public readonly Slot Slot;
        public readonly ModuleDragSource Source;

        public ModuleDragData(Transform transform, UIModuleItem moduleItem, Slot slot, ModuleDragSource dragSource)
        {
            Transform = transform;
            Module = moduleItem.ModuleData;
            ModuleItem = moduleItem;
            Source = dragSource;
            Slot = slot;
        }

        public Transform Transform { get; }
        public Dragger DraggedBy { get; set; }
    }
}