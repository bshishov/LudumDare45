﻿using System.Collections.Generic;
using Model;
using UnityEngine;

namespace UI
{
    public class UIShipsList : MonoBehaviour
    {
        private readonly List<UIShipIcon> _items = new List<UIShipIcon>();
        public GameObject ItemPrefab;

        public void Add(ShipMeta s)
        {
            if (ItemPrefab != null)
            {
                var go = Instantiate(ItemPrefab, transform);
                var item = go.GetComponent<UIShipIcon>();
                item.Setup(s);
                _items.Add(item);
            }
        }

        public void Clear()
        {
            foreach (var item in _items)
                if (item != null)
                    Destroy(item.gameObject);
            _items.Clear();
        }

        public bool TryGetActiveIndex(out int index, out string id)
        {
            for (var i = 0; i < _items.Count; i++)
            {
                var item = _items[i];
                if (item.IsEnabled)
                {
                    index = i;
                    id = item.ShipId;
                    return true;
                }
            }

            id = null;
            index = -1;
            return false;
        }

        public void SetInactive(int playerShipIndex)
        {
            _items[playerShipIndex].Disable();
        }
    }
}