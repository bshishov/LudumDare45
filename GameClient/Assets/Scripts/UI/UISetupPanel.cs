using UnityEngine;
using Utils;

namespace UI
{
    [RequireComponent(typeof(UICanvasGroupFader))]
    public class UISetupPanel : Singleton<UISetupPanel>
    {
        private UICanvasGroupFader _fader;
        public UIModulesList ArmoryModules;

        private void Awake()
        {
            _fader = GetComponent<UICanvasGroupFader>();
        }

        public void Show()
        {
            _fader.FadeIn();
        }

        public void Hide()
        {
            _fader.FadeOut();
        }
    }
}