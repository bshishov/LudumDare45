using Model;
using UnityEngine;
using Utils;

namespace UI
{
    [RequireComponent(typeof(UICanvasGroupFader))]
    public class UIGameCreatedScreen : Singleton<UIGameCreatedScreen>
    {
        private UICanvasGroupFader _fader;
        public UIProfilePanel OpponentProfile;
        public UIProfilePanel PlayerProfile;

        private void Start()
        {
            _fader = GetComponent<UICanvasGroupFader>();
        }

        public void Show(Profile player, Profile opponent)
        {
            PlayerProfile.SetProfile(player);
            OpponentProfile.SetProfile(opponent);
            _fader.FadeIn();
        }

        public void Hide()
        {
            _fader.FadeOut();
        }
    }
}