using System;
using System.Collections;
using TMPro;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(UICanvasGroupFader))]
    public class UITimer : MonoBehaviour
    {
        private UICanvasGroupFader _fader;
        private IEnumerator _routine;
        public TextMeshProUGUI Label;

        private void Awake()
        {
            _fader = GetComponent<UICanvasGroupFader>();
        }

        private void UpdateText(float totalSeconds)
        {
            if (Label == null) return;
            var timespan = TimeSpan.FromSeconds(totalSeconds);
            Label.text = timespan.ToString(@"mm\:ss");
        }

        public void Countdown(float seconds)
        {
            if (_routine != null)
                StopCoroutine(_routine);

            _routine = CountdownRoutine(seconds);
            StartCoroutine(_routine);
        }

        private IEnumerator CountdownRoutine(float time)
        {
            _fader.FadeIn();
            while (time > 1)
            {
                yield return new WaitForSeconds(1f);
                time -= 1f;
                UpdateText(time);
            }

            // Remaining
            yield return new WaitForSeconds(time);
            time -= time;
            UpdateText(time);
            _fader.FadeOut();
        }

        public void StopAndHide()
        {
            if (_routine != null)
                StopCoroutine(_routine);

            _fader.FadeOut();
        }
    }
}