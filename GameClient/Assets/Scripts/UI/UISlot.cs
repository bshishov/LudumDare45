using Model;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UISlot : MonoBehaviour, IModule
    {
        public Image DropArea;
        public TextMeshProUGUI HpText;

        public UICanvasGroupFader ModulePanel;

        [Header("Notifications")] public RectTransform NotificationCanvas;

        public GameObject NotificationPrefab;
        public TextMeshProUGUI TierText;
        public GameObject UIModuleItemPrefab;

        public void OnMounted(Slot slot)
        {
            Debug.Log($"[UISlot] Fading UI for slot {slot.Id}");

            if (TierText != null)
                TierText.text = GetTierText(slot.Module.upgrade_tier);

            ModulePanel.FadeIn();
        }

        public void OnUnmounted()
        {
            ModulePanel.FadeOut();
            if (TierText != null)
                TierText.text = string.Empty;
        }

        public void OnFiredEvent(GameEvent e)
        {
        }

        public void OnReceivedEvent(GameEvent e)
        {
            if (e.Name == GameEvent.Death)
                ModulePanel.FadeOut();
            else if (e.Name == GameEvent.Hit)
                DamagePopup(e.Value);
            else if (e.Name == GameEvent.Heal)
                HealPopup(e.Value);
        }

        public void OnStateChanged(ModuleState state)
        {
            if (HpText != null && state != null)
            {
                var moduleMeta = Database.Instance.Modules[state.id];

                if (state.hp > 0)
                    ModulePanel.FadeIn();

                if (state.hp > moduleMeta.base_hp)
                    HpText.color = Color.green;
                else if (state.hp < moduleMeta.base_hp)
                    HpText.color = Color.red;
                else
                    HpText.color = Color.white;

                HpText.text = Mathf.CeilToInt(state.hp).ToString();
            }
        }

        private string GetTierText(int tier)
        {
            if (tier <= 1)
                return string.Empty;

            switch (tier)
            {
                case 2:
                    return "II";
                case 3:
                    return "III";
                case 4:
                    return "IV";
                case 5:
                    return "V";
            }

            return tier.ToString();
        }

        public void Popup(string text, Color color)
        {
            if (NotificationCanvas == null && NotificationPrefab == null)
                return;

            var notificationObj = Instantiate(NotificationPrefab, NotificationCanvas);
            var notification = notificationObj.GetComponent<UIPopup>();
            if (notification) notification.Setup(text, color);
        }

        public void DamagePopup(float amount)
        {
            Popup(amount.ToString("0.#"), Color.red);
        }

        public void HealPopup(float amount)
        {
            Popup(amount.ToString("0.#"), Color.green);
        }
    }
}