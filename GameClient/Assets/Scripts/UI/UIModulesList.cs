using System.Collections.Generic;
using Model;
using UnityEngine;

namespace UI
{
    public class UIModulesList : MonoBehaviour
    {
        public Transform TargetContainer;
        public GameObject UIModulePrefab;

        public UIModuleItem Add(ModuleMeta module)
        {
            var newItem = Instantiate(UIModulePrefab, TargetContainer);
            var uiModuleItem = newItem.GetComponent<UIModuleItem>();
            if (uiModuleItem != null)
                uiModuleItem.Setup(module);
            else
                Debug.LogWarning($"Module item prefab {UIModulePrefab.name} does not have a UIModuleItem component");

            return uiModuleItem;
        }

        public void Clear()
        {
            foreach (Transform child in TargetContainer) Destroy(child.gameObject);
        }

        public void UpdateModules(IEnumerable<string> moduleIds)
        {
            var remaining = new List<string>(moduleIds);
            foreach (Transform child in TargetContainer)
            {
                var moduleItem = child.GetComponent<UIModuleItem>();
                if (moduleItem != null)
                {
                    if (!remaining.Contains(moduleItem.ModuleData.id))
                        // Remove from list
                        Destroy(child.gameObject);
                    else
                        remaining.Remove(moduleItem.ModuleData.id);
                }
            }

            foreach (var moduleId in remaining) Add(Database.Instance.Modules[moduleId]);
        }
    }
}