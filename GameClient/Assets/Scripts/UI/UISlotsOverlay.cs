using UnityEngine;
using Utils;

namespace UI
{
    [RequireComponent(typeof(UICanvasGroupFader))]
    public class UISlotsOverlay : Singleton<UISlotsOverlay>
    {
        private UICanvasGroupFader _fader;
        public GameObject UISlotPrefab;

        private void Awake()
        {
            _fader = GetComponent<UICanvasGroupFader>();
        }

        public UISlot Add(Slot slot)
        {
            if (UISlotPrefab != null)
            {
                var go = Instantiate(UISlotPrefab, transform);
                var uiSlot = go.GetComponent<UISlot>();
                //uiSlot.Setup(slot);

                var follow = go.AddComponent<UIFollowSceneObject>();
                follow.SetTarget(slot.transform);

                return uiSlot;
            }

            return null;
        }

        public void Remove(Slot slot)
        {
        }

        public void Show()
        {
            _fader.FadeIn();
        }

        public void Hide()
        {
            _fader.FadeOut();
        }
    }
}