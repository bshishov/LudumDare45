using UnityEngine;

namespace UI
{
    public class UIProgressBar : MonoBehaviour
    {
        private Vector2 _initialSize;
        private float _target;

        private float _value;
        private float _velocity;
        public float ChangeTime = 0.5f;
        public RectTransform FillTransform;

        public float Initial = 1f;
        public UIShaker Shaker;

        private void Start()
        {
            _initialSize = FillTransform.sizeDelta;
            _target = Initial;
            SetValue(Initial);
        }

        private void Update()
        {
            SetValue(Mathf.SmoothDamp(_value, _target, ref _velocity, ChangeTime));

            if (Shaker != null)
                Shaker.Shake(Mathf.Abs(_velocity));
        }

        private void SetValue(float val)
        {
            if (FillTransform != null)
            {
                _value = Mathf.Clamp01(val);
                FillTransform.sizeDelta = new Vector2(_initialSize.x * _value, _initialSize.y);
            }
        }

        public void SetTarget(float value)
        {
            _target = value;
        }

        [ContextMenu("Random target")]
        public void RandomTarget()
        {
            _target = Random.value;
        }
    }
}