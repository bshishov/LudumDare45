﻿using System.Linq;
using Model;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class UIShipIcon : MonoBehaviour, IPointerClickHandler
    {
        public Image CrossedImage;
        public Image Icon;

        public string ShipId { get; private set; }

        public bool IsEnabled { get; private set; }

        public void OnPointerClick(PointerEventData eventData)
        {
            //if(IsEnabled)
            //GameManager.Instance.SelectShip(ShipId, _index);
        }

        private void Start()
        {
            if (CrossedImage != null)
                CrossedImage.enabled = false;
        }

        public void Setup(ShipMeta ship)
        {
            ShipId = ship.id;
            IsEnabled = true;

            var shipData = Database.Instance.ShipsData.FirstOrDefault(s => s.Id == ship.id);
            if (shipData != null) Icon.sprite = shipData.Icon;

            Icon.color = Color.white;
        }

        public void Clear()
        {
            Icon.color = new Color(0, 0, 0, 0);
        }

        public void Disable()
        {
            if (CrossedImage != null)
                CrossedImage.enabled = true;

            IsEnabled = false;
        }
    }
}