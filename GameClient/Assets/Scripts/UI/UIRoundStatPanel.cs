﻿using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class UIRoundStatPanel : MonoBehaviour
    {
        private readonly List<UIShipIcon> _icons = new List<UIShipIcon>();
        public RectTransform Container;
        public GameObject ItemPrefab;
        public bool NewItemsToTheLeft;


        public UIShipIcon Add(string shipName)
        {
            var go = Instantiate(ItemPrefab, Container);

            if (NewItemsToTheLeft) go.transform.SetSiblingIndex(0);

            var icon = go.GetComponent<UIShipIcon>();
            if (!string.IsNullOrEmpty(shipName))
            {
                var ship = Database.Instance.Ships[shipName];
                icon.Setup(ship);
            }

            _icons.Add(icon);
            return icon;
        }

        public void Clear()
        {
            _icons.Clear();
            foreach (Transform child in transform)
                Destroy(child.gameObject);
        }

        public void Setup(int roundsToWin)
        {
            Clear();
            for (var i = 0; i < roundsToWin; i++)
            {
                var icon = Add(null);
                icon.Clear();
            }
        }

        public void Set(int index, string shipName)
        {
            //if (NewItemsToTheLeft)
            index = _icons.Count - index - 1;


            if (index >= 0 && index < _icons.Count)
            {
                var ship = Database.Instance.Ships[shipName];
                var icon = _icons[index];
                icon.Setup(ship);
            }
        }
    }
}