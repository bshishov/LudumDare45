﻿using Model;
using Network;
using TMPro;
using UnityEngine;

namespace UI
{
    public class UIMoney : MonoBehaviour
    {
        public TextMeshProUGUI Label;

        private void Start()
        {
            Connection.Instance.MessageReceived.AddListener<S2CMoneyUpdated>(MoneyUpdated);
        }

        private void MoneyUpdated(S2CMoneyUpdated e)
        {
            if (Label != null)
                Label.text = e.new_amount.ToString();
        }
    }
}