using Model;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class UIModuleItem : MonoBehaviour,
        IPointerEnterHandler,
        IPointerExitHandler
    {
        public TextMeshProUGUI Damage;
        public Image Fader;

        [Header("Sounds")] public Sound HoverSound;

        public TextMeshProUGUI Hp;
        public Image Icon;
        public TextMeshProUGUI Name;
        public TextMeshProUGUI Price;
        public TextMeshProUGUI Priority;

        public ModuleMeta ModuleData { get; private set; }

        public void OnPointerEnter(PointerEventData eventData)
        {
            UITooltip.Instance.Show(ModuleData);
            SoundManager.Instance.Play(HoverSound);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            UITooltip.Instance.Hide();
        }

        public void Setup(ModuleMeta data)
        {
            ModuleData = data;

            if (Name != null)
                Name.text = data.name;

            if (Icon != null)
                Icon.sprite = Database.Instance.GetModuleDbInfo(data.id)?.Icon;

            if (Priority != null)
                Priority.text = ModuleData.base_priority.ToString();

            if (Damage != null)
                Damage.text = ModuleData.base_damage.ToString();

            if (Hp != null)
                Hp.text = ModuleData.base_hp.ToString();

            if (Price != null)
                Price.text = ModuleData.price.ToString();
        }

        public void Fade(float alpha)
        {
            if (Fader != null)
            {
                var color = Fader.color;
                Fader.color = new Color(color.r, color.g, color.b, alpha);
            }
        }
    }
}