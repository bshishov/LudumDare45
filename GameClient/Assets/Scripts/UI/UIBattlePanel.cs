using TMPro;
using UnityEngine;
using Utils;

namespace UI
{
    [RequireComponent(typeof(UICanvasGroupFader))]
    public class UIBattlePanel : Singleton<UIBattlePanel>
    {
        private UICanvasGroupFader _fader;
        public TextMeshProUGUI HpText1;
        public TextMeshProUGUI HpText2;
        public TextMeshProUGUI Name1;
        public TextMeshProUGUI Name2;
        public UIProgressBar ProgressBar1;
        public UIProgressBar ProgressBar2;

        private void Awake()
        {
            _fader = GetComponent<UICanvasGroupFader>();
        }

        public void Show()
        {
            _fader.FadeIn();
        }

        public void Hide()
        {
            _fader.FadeOut();
        }

        public void UpdatePlayerShipState()
        {
            UpdateShipState(GameManager.Instance.PlayerArea.Ship, ProgressBar1, HpText1);
        }

        public void UpdatePlayerProfile()
        {
            var profile = GameManager.Instance.PlayerProfile;
            if (Name1 != null)
                Name1.text = profile.name;
        }

        public void UpdateOpponentShipState()
        {
            UpdateShipState(GameManager.Instance.OpponentArea.Ship, ProgressBar2, HpText2);
        }

        private void UpdateShipState(Ship ship, UIProgressBar progressBar, TextMeshProUGUI text)
        {
            var maxHp = Mathf.Max(ship.GetMaxHp(), 0);
            var hp = Mathf.Max(ship.GetHp(), 0);

            if (progressBar != null)
                progressBar.SetTarget(hp / maxHp);

            if (text != null)
                text.text = $"{Mathf.CeilToInt(hp)} / {Mathf.CeilToInt(maxHp)}";
        }

        public void UpdateOpponentProfile()
        {
            var profile = GameManager.Instance.OpponentProfile;
            if (Name2 != null)
                Name2.text = profile.name;
        }
    }
}