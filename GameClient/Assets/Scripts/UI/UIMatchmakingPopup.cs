﻿using Model;
using Network;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(UICanvasGroupFader))]
    public class UIMatchmakingPopup : MonoBehaviour
    {
        private Connection _connection;

        private UICanvasGroupFader _fader;
        public Button CancelButton;
        public TextMeshProUGUI StatusText;

        private void Start()
        {
            _connection = Connection.Instance;

            _fader = GetComponent<UICanvasGroupFader>();

            _connection.MessageReceived.AddListener<S2CMatchmakingUpdate>(OnMatchmakingUpdate);
            _connection.MessageReceived.AddListener<S2CStoppedMatchmaking>(OnMatchmakingStopped);
            _connection.MessageReceived.AddListener<S2CStartedMatchmaking>(OnMatchmakingStarted);
            _connection.MessageReceived.AddListener<S2CGameCreated>(OnGameCreated);
            _connection.Disconnected += ConnectionDisconnected;

            CancelButton.onClick.AddListener(Cancel);
        }

        private void ConnectionDisconnected()
        {
            _fader.FadeOut();
        }

        public void Cancel()
        {
            if (_connection.IsConnected)
                _connection.Send(new C2SStopMatchmaking());
            else
                _fader.FadeOut();
        }

        private void OnGameCreated(S2CGameCreated e)
        {
            _fader.FadeOut();
        }

        private void OnMatchmakingStopped(S2CStoppedMatchmaking e)
        {
            _fader.FadeOut();
        }

        private void OnMatchmakingUpdate(S2CMatchmakingUpdate state)
        {
            if (StatusText != null)
                StatusText.text = $"Players online: {state.players_total} / In battle: {state.players_in_battle}";
        }

        private void OnMatchmakingStarted(S2CStartedMatchmaking obj)
        {
            if (StatusText != null)
                StatusText.text = "...";

            _fader.FadeIn();
        }
    }
}