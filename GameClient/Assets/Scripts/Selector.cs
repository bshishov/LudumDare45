﻿using UnityEngine;

public class Selector : MonoBehaviour
{
    public Animator Animator;
    public MeshRenderer Renderer;

    private void Awake()
    {
        Hide();
    }

    public void Show()
    {
        if (Renderer != null)
            Renderer.enabled = true;
    }

    public void Hide()
    {
        if (Renderer != null)
            Renderer.enabled = false;
    }

    public void Place()
    {
        if (Animator != null)
            Animator.SetTrigger("place");
    }
}