using System;
using System.Collections.Generic;
using System.Linq;
using Model;
using Newtonsoft.Json;
using UnityEngine;
using Utils;
using Utils.Debugger;

[Serializable]
public class Database : Singleton<Database>
{
    private const string MetaPath = "meta.json";

    [Header("Fallback")] public ModuleDbInfo FallbackModule;

    public IDictionary<string, ModuleMeta> Modules;
    public ModuleDbInfo[] ModulesData;
    public IDictionary<string, ShipMeta> Ships;

    public ShipDbInfo[] ShipsData;
    public Meta Meta { get; private set; }

    private void Awake()
    {
        if (!string.IsNullOrEmpty(MetaPath))
        {
            var rawDb = LoadResource(MetaPath);
            LoadFromDbString(rawDb);
        }
    }

    public void LoadFromDbString(string rawDb)
    {
        var db = JsonConvert.DeserializeObject<Meta>(rawDb);
        Meta = db;
        Debug.Log($"[DB] Loaded: {MetaPath}");
        Debug.Log($"[DB] {db.ships.Length} ships");
        Debug.Log($"[DB] {db.modules.Length} modules");

        Ships = new Dictionary<string, ShipMeta>();
        foreach (var ship in db.ships)
        {
            Ships.Add(ship.id, ship);
            var raw = JsonConvert.SerializeObject(ship, Formatting.Indented);
            Debugger.Default.Display($"DB/Ships/{ship.id}", new Vector2(500, 500),
                rect => { GUI.Label(rect, raw); });
        }

        Modules = new Dictionary<string, ModuleMeta>();
        foreach (var module in db.modules)
        {
            Modules.Add(module.id, module);
            var raw = JsonConvert.SerializeObject(module, Formatting.Indented);
            Debugger.Default.Display($"DB/Modules/{module.id}", new Vector2(500, 500),
                rect => { GUI.Label(rect, raw); });
        }
    }

    public static string LoadResource(string path)
    {
        var filePath = path.Replace(".json", "");
        var targetFile = Resources.Load<TextAsset>(filePath);
        return targetFile.text;
    }

    public ModuleDbInfo GetModuleDbInfo(string id)
    {
        var data = ModulesData.FirstOrDefault(s => s.Id == id);
        if (data != null)
            return data;

        Debug.LogWarning($"[DB] No such module db info with id: {id}, using fallback");
        return FallbackModule;
    }

    public ShipDbInfo GetShipDbInfo(string id)
    {
        var data = ShipsData.FirstOrDefault(s => s.Id == id);
        if (data != null)
            return data;

        Debug.LogWarning($"[DB] No such ship db info with id: {id}");
        return null;
    }

    public GameObject GetShipPrefab(string shipId)
    {
        var prefab = GetShipDbInfo(shipId)?.Prefab;
        if (prefab != null)
            return prefab;

        Debug.LogWarning($"[DB] Prefab for ship db info with id: {shipId}");
        return null;
    }

    public GameObject GetModulePrefab(string id)
    {
        var prefab = GetModuleDbInfo(id)?.Prefab;
        if (prefab != null)
            return prefab;

        Debug.LogWarning($"[DB] Prefab for module db info with id: {id}");
        return null;
    }

    [Serializable]
    public class ModuleDbInfo
    {
        public Sprite Icon;
        public string Id;
        public GameObject Prefab;
    }

    [Serializable]
    public class ShipDbInfo
    {
        public Sprite Icon;
        public string Id;
        public GameObject Prefab;
    }

    [Serializable]
    public class ModuleDbInfoDict : SerializableDictionary<string, ModuleDbInfo>
    {
    }

    [Serializable]
    public class ShipDataInfoDict : SerializableDictionary<string, ShipMeta>
    {
    }
}