using Model;
using Network;
using UI;
using UI.DragDrop;
using UnityEngine;

[RequireComponent(typeof(Slot))]
public class SlotDragReceiver : DragReceiver<ModuleDragData>
{
    private Slot _slot;

    private void Start()
    {
        _slot = GetComponent<Slot>();
    }

    public override bool CanReceive(ModuleDragData data)
    {
        if (data.Slot != null && data.Slot == _slot)
            return false;

        if (data.Source == ModuleDragSource.Field)
            return true;

        // If placing
        if (!_slot.HasModule)
            return true;

        if (_slot.HasModule)
            // If upgrading
            if (_slot.ModuleId == data.Module.id && !string.IsNullOrEmpty(data.Module.upgrades_to))
                return true;

        return false;
    }

    public override void Receive(ModuleDragData data)
    {
        if (data.Source == ModuleDragSource.Shop)
            Connection.Instance.Send(new C2SModuleBuy
            {
                module = data.Module.id,
                slot = _slot.Id
            });
        else if (data.Source == ModuleDragSource.Field)
            Connection.Instance.Send(new C2SModuleMove
            {
                slot_from = data.Slot.Id,
                slot_to = _slot.Id
            });
        _slot.Place(data.Module);
    }
}