public struct GameEvent
{
    public string Name;
    public float Value;
    public Ship SourceShip;
    public Slot SourceSlot;
    public Ship TargetShip;
    public Slot TargetSlot;

    public const string Fire = "fire";
    public const string Hit = "hit";
    public const string Heal = "heal";
    public const string DmgMul = "dmg_mul";
    public const string HpMul = "hp_mul";
    public const string PriorityAdd = "priority_add";
    public const string Death = "death";
}