using Model;
using UnityEngine;

public class ShipArea : MonoBehaviour
{
    public Vector3 SpawnOffset = new Vector3(0, 3, 0);
    public Ship Ship { get; private set; }

    private void ConstructShipById(string shipId)
    {
        if (Ship != null)
        {
            if (Ship.ShipKey == shipId)
                // Ship is the same no need to create new one
                return;

            Debug.Log($"[ShipArea:{gameObject.name}] destroying ship, because new one is different");
            Destroy(Ship);
        }

        var prefab = Database.Instance.GetShipPrefab(shipId);
        if (prefab == null)
        {
            Debug.LogWarning($"[ShipArea:{gameObject.name}] Cant find prefab for ship {shipId}");
            return;
        }

        var shipData = Database.Instance.Ships[shipId];
        if (shipData == null)
        {
            Debug.LogWarning($"[ShipArea:{gameObject.name}] Cant find shipData for ship {shipId}");
            return;
        }

        var obj = Instantiate(prefab, transform.TransformPoint(SpawnOffset), transform.rotation);
        obj.transform.SetParent(transform, true);
        var ship = obj.GetComponent<Ship>();
        if (ship != null)
            //ship.Setup(shipData); // Setup is done inside the ship
            Ship = ship;
        else
            Debug.LogWarning($"[ShipArea:{gameObject.name}] ship prefab does not have a ship component");
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(8, 4, 8));
    }

    public void SetShipState(ShipState shipState)
    {
        Debug.Log($"[ShipArea:{gameObject.name}] received state");
        if (shipState == null)
        {
            DestroyShip();
            return;
        }

        if (Ship != null)
        {
            if (Ship.ShipKey != shipState.id)
            {
                // Recreate if not the same
                DestroyShip();
                ConstructShipById(shipState.id);
            }
        }
        else
        {
            // create if there was no ship
            ConstructShipById(shipState.id);
        }

        // Just update the state
        if (Ship != null)
            Ship.SetState(shipState);
    }

    private void DestroyShip()
    {
        if (Ship != null)
        {
            Debug.Log("Destroying ship");
            Destroy(Ship.gameObject);
            Ship = null;
        }
    }
}