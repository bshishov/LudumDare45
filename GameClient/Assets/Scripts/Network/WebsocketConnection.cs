using System;
using System.Collections.Concurrent;
using System.IO;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace Network
{
    public class WebsocketConnection : MonoBehaviour
    {
        private const int BufferSize = 8192;
        private const int MaxMessagesPerFrame = 10;
        private readonly ConcurrentQueue<string> _messages = new ConcurrentQueue<string>();
        private bool _disconnectEventRequestedFlag;
        private ClientWebSocket _webSocket = new ClientWebSocket();

        public Action Connected;
        public Action Disconnected;
        public Action<string> OnMessage;
        public bool IsConnected => _webSocket.State == WebSocketState.Open;

        private void Start()
        {
            DontDestroyOnLoad(gameObject);
        }

        private void Update()
        {
            var messagesProcessed = 0;
            while (_messages.Count > 0)
            {
                if (_messages.TryDequeue(out var message))
                    OnMessage(message);

                messagesProcessed++;
                if (messagesProcessed > MaxMessagesPerFrame)
                    break;
            }

            if (_disconnectEventRequestedFlag)
            {
                Disconnected?.Invoke();
                _disconnectEventRequestedFlag = false;
            }
        }

        private void OnDestroy()
        {
            Close();
        }

        public async void RunAsync(string uri)
        {
            Debug.Log($"[WS] Connecting to {uri}");
            try
            {
                _webSocket = new ClientWebSocket();

                // ConfigureAwait(true) because we directly call the connected event so we want
                // to capture execution context
                await _webSocket.ConnectAsync(new Uri(uri), CancellationToken.None).ConfigureAwait(true);
                Debug.Log("[WS] Connected");
                Connected?.Invoke();

                // ConfigureAwait(false) because we don't care about the context the method will be executed in 
                await ReceiveAsync().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                _disconnectEventRequestedFlag = true;
            }
        }

        public void SendAsync(string message)
        {
            if (_webSocket != null && _webSocket.State == WebSocketState.Open)
            {
                var encoded = Encoding.UTF8.GetBytes(message);
                var buffer = new ArraySegment<byte>(encoded, 0, encoded.Length);

                //await _webSocket.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None);

                // TODO: PERFORMANCE use WAIT carefully!!! Unfortunately you cant send in parallel which sucks
                _webSocket
                    .SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None)
                    .Wait();
            }
        }

        private async Task ReceiveAsync()
        {
            var buffer = new ArraySegment<byte>(new byte[BufferSize]);

            while (_webSocket.State == WebSocketState.Open)
                using (var ms = new MemoryStream())
                {
                    WebSocketReceiveResult result;
                    do
                    {
                        result = await _webSocket.ReceiveAsync(buffer, CancellationToken.None);
                        ms.Write(buffer.Array, buffer.Offset, result.Count);
                    } while (!result.EndOfMessage);

                    ms.Seek(0, SeekOrigin.Begin);

                    if (result.MessageType == WebSocketMessageType.Text)
                    {
                        using (var reader = new StreamReader(ms, Encoding.UTF8))
                        {
                            var message = reader.ReadToEnd();
                            _messages.Enqueue(message);
                        }
                    }
                    else if (result.MessageType == WebSocketMessageType.Close)
                    {
                        await _webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, string.Empty,
                            CancellationToken.None);
                        _disconnectEventRequestedFlag = true;
                    }
                }
        }

        public void Close()
        {
            _webSocket?.Dispose();
            _disconnectEventRequestedFlag = true;
        }
    }
}