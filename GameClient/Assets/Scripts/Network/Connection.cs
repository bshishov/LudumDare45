﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model;
using Newtonsoft.Json;
using UnityEngine;
using Utils;
using Utils.Debugger;
using Logger = Utils.Debugger.Logger;

namespace Network
{
    [RequireComponent(typeof(WebsocketConnection))]
    public class Connection : Singleton<Connection>
    {
        private static readonly Dictionary<string, Type> MessageToPayload = new Dictionary<string, Type>
        {
            {MessageNames.C2SHello, typeof(C2SHello)},
            {MessageNames.C2SStartMatchmaking, typeof(C2SStartMatchmaking)},
            {MessageNames.C2SStopMatchmaking, typeof(C2SStopMatchmaking)},
            {MessageNames.C2SModuleMove, typeof(C2SModuleMove)},
            {MessageNames.C2SModuleBuy, typeof(C2SModuleBuy)},
            {MessageNames.C2SModuleSell, typeof(C2SModuleSell)},
            {MessageNames.C2SReady, typeof(C2SReady)},

            {MessageNames.S2CHello, typeof(S2CHello)},
            {MessageNames.S2CGameCreated, typeof(S2CGameCreated)},
            {MessageNames.S2CGameEnded, typeof(S2CGameEnded)},
            {MessageNames.S2CGameStarted, typeof(S2CGameStarted)},
            {MessageNames.S2CGameEvent, typeof(S2CGameEvent)},
            {MessageNames.S2CStartedMatchmaking, typeof(S2CStartedMatchmaking)},
            {MessageNames.S2CStoppedMatchmaking, typeof(S2CStoppedMatchmaking)},
            {MessageNames.S2CMatchmakingUpdate, typeof(S2CMatchmakingUpdate)},
            {MessageNames.S2CRoundStarted, typeof(S2CRoundStarted)},
            {MessageNames.S2CRoundEnded, typeof(S2CRoundEnded)},
            {MessageNames.S2CRoundBuildPhase, typeof(S2CRoundBuildPhase)},
            {MessageNames.S2CRoundBattlePhase, typeof(S2CRoundBattlePhase)},
            {MessageNames.S2CShipUpdated, typeof(S2CShipUpdated)},
            {MessageNames.S2CMoneyUpdated, typeof(S2CMoneyUpdated)},
            {MessageNames.S2CPlayerReady, typeof(S2CPlayerReady)}
        };

        private static readonly Dictionary<Type, string> PayloadToMessage =
            MessageToPayload.ToDictionary(x => x.Value, x => x.Key);

        private WebsocketConnection _connection;

        private string _debugHost = Common.DebugHost;
        private Logger _debugRecvLog;
        private Logger _debugSendLog;

        // Events
        public TypedEvent<IPayload> MessageReceived = new TypedEvent<IPayload>();
        public bool IsConnected => _connection.IsConnected;
        public event Action Connected;
        public event Action Disconnected;

        private void Start()
        {
            _connection = GetComponent<WebsocketConnection>();
            _connection.Disconnected += () => { Disconnected?.Invoke(); };
            _connection.OnMessage += OnMessageReceived;
            _connection.Connected += () => { Connected?.Invoke(); };

            DontDestroyOnLoad(gameObject);

            Debugger.Default.Display("WS/Host", new Vector2(200, 20),
                rect => { _debugHost = GUI.TextField(rect, _debugHost); });
            Debugger.Default.Display("Connect/Local", () => { Connect(Common.LocalHost); });
            Debugger.Default.Display("Connect/Debug", () => { Connect(_debugHost); });
            Debugger.Default.Display("Connect/Prod", () => { Connect(Common.ProdHost); });
            Debugger.Default.Display("WS/Close", Close);
            Debugger.Default.Display("WS/Start MM", () => Send(new C2SStartMatchmaking()));
            Debugger.Default.Display("WS/End MM", () => Send(new C2SStopMatchmaking()));
            _debugRecvLog = Debugger.Default.GetLogger("WS/Recv Log", unityLog: false);
            _debugSendLog = Debugger.Default.GetLogger("WS/Send Log", unityLog: false);
        }

        private void Update()
        {
            Debugger.Default.Display("WS/IsConnected", IsConnected.ToString());
        }

        public void Connect(string host)
        {
            if (_connection.IsConnected)
                _connection.Close();
            _connection.RunAsync(host);
        }

        private void OnMessageReceived(string messageRaw)
        {
            _debugRecvLog.Log(messageRaw);
            var msg = JsonConvert.DeserializeObject<Message>(messageRaw);

            if (MessageToPayload.TryGetValue(msg.type, out var messagePayloadType))
            {
                var payload = msg.LoadPayload(messagePayloadType);
                MessageReceived.Invoke(payload.GetType(), payload);
            }
            else
            {
                Debug.LogWarningFormat("No such message type: {0}", msg.type);
            }
        }

        public void Send<T>(T message)
            where T : IPayload
        {
            if (_connection != null && _connection.IsConnected)
            {
                if (PayloadToMessage.TryGetValue(typeof(T), out var messageName))
                {
                    var serialized = JsonConvert.SerializeObject(new Message(messageName, message));
                    _debugSendLog.Log(serialized);
                    _connection.SendAsync(serialized);
                }
                else
                {
                    Debug.LogWarningFormat("Cant resolve message name for type: {0}", typeof(T));
                }
            }
        }

        public void Close()
        {
            if (_connection != null)
                _connection.Close();
        }

        private void OnDestroy()
        {
            Close();
        }
    }
}