namespace Network
{
    public static class MessageNames
    {
        // General
        public const string S2CHello = "S2C_HELLO";
        public const string C2SHello = "C2S_HELLO";

        // Matchmaking
        public const string C2SStartMatchmaking = "C2S_START_MATCHMAKING";
        public const string C2SStopMatchmaking = "C2S_STOP_MATCHMAKING";
        public const string S2CStartedMatchmaking = "S2C_STARTED_MATCHMAKING";
        public const string S2CStoppedMatchmaking = "S2C_STOPPED_MATCHMAKING";
        public const string S2CMatchmakingUpdate = "S2C_MATCHMAKING_UPDATE";

        // Game
        public const string S2CGameCreated = "S2C_GAME_CREATED";
        public const string S2CGameStarted = "S2C_GAME_STARTED";

        public const string C2SReady = "C2S_READY";
        public const string C2SModuleBuy = "C2S_MODULE_BUY";
        public const string C2SModuleSell = "C2S_MODULE_SELL";
        public const string C2SModuleMove = "C2S_MODULE_MOVE";


        public const string S2CMoneyUpdated = "S2C_MONEY_UPDATED";
        public const string S2CShipUpdated = "S2C_SHIP_UPDATED";

        public const string S2CGameEvent = "S2C_GAME_EVENT";
        public const string S2CGameEnded = "S2C_GAME_ENDED";

        // RoundPhases
        public const string S2CRoundStarted = "S2C_ROUND_STARTED";
        public const string S2CRoundBuildPhase = "S2C_ROUND_BUILD_PHASE";
        public const string S2CRoundBattlePhase = "S2C_ROUND_BATTLE_PHASE";
        public const string S2CRoundEnded = "S2C_ROUND_ENDED";
        public const string S2CPlayerReady = "S2C_PLAYER_READY";
    }
}