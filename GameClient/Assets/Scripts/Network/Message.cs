using System;
using Model;
using Newtonsoft.Json;

// ReSharper disable InconsistentNaming

namespace Network
{
    [Serializable]
    public struct Message
    {
        public string type;
        public string payload;

        public Message(string type, IPayload payload)
        {
            this.type = type;
            this.payload = JsonConvert.SerializeObject(payload);
        }

        public Message(string type)
        {
            this.type = type;
            payload = null;
        }

        public T LoadPayload<T>()
        {
            return JsonConvert.DeserializeObject<T>(payload);
        }

        public IPayload LoadPayload(Type typ)
        {
            return (IPayload) JsonConvert.DeserializeObject(payload, typ);
        }
    }
}