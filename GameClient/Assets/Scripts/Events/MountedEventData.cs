using Model;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Events
{
    public class CustomEventData<T> : BaseEventData
    {
        public readonly T Data;

        public CustomEventData(EventSystem eventSystem, T data) : base(eventSystem)
        {
            Data = data;
        }

        public CustomEventData(T data) : base(EventSystem.current)
        {
            Data = data;
        }
    }

    public static class CustomEvents
    {
        public static readonly ExecuteEvents.EventFunction<IModule> MountedHandler =
            delegate(IModule handler, BaseEventData data)
            {
                var eventData = ExecuteEvents.ValidateEventData<CustomEventData<Slot>>(data);
                handler.OnMounted(eventData.Data);
            };

        public static readonly ExecuteEvents.EventFunction<IModule> StateChangedHandler =
            delegate(IModule handler, BaseEventData data)
            {
                var eventData = ExecuteEvents.ValidateEventData<CustomEventData<ModuleState>>(data);
                handler.OnStateChanged(eventData.Data);
            };

        public static readonly ExecuteEvents.EventFunction<IModule> UnmountedHandler =
            delegate(IModule handler, BaseEventData data) { handler.OnUnmounted(); };

        public static readonly ExecuteEvents.EventFunction<IModule> GameEventFiredHandler =
            delegate(IModule handler, BaseEventData data)
            {
                var eventData = ExecuteEvents.ValidateEventData<CustomEventData<GameEvent>>(data);
                handler.OnFiredEvent(eventData.Data);
            };

        public static readonly ExecuteEvents.EventFunction<IModule> GameEventReceivedHandler =
            delegate(IModule handler, BaseEventData data)
            {
                var eventData = ExecuteEvents.ValidateEventData<CustomEventData<GameEvent>>(data);
                handler.OnReceivedEvent(eventData.Data);
            };

        public static void Execute<T>(GameObject go, ExecuteEvents.EventFunction<IModule> handler, T data)
        {
            //ExecuteEvents.Execute<IModule>(go, new CustomEventData<T>(EventSystem.current, data), handler);

            var e = new CustomEventData<T>(data);
            foreach (var componentsInChild in go.GetComponentsInChildren<IModule>())
                handler.Invoke(componentsInChild, e);
        }

        public static void Execute(GameObject go, ExecuteEvents.EventFunction<IModule> handler)
        {
            //ExecuteEvents.Execute<IModule>(go, new CustomEventData<T>(EventSystem.current, data), handler);

            foreach (var componentsInChild in go.GetComponentsInChildren<IModule>())
                handler.Invoke(componentsInChild, null);
        }
    }
}