namespace GameStates
{
    public enum GameState
    {
        Login,
        MainMenu,
        GameCreated,
        BuildPhase,
        BattlePhase,
        GameEnded
    }
}