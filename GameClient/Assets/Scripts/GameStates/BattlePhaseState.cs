using System;
using Model;
using Network;
using UI;
using UnityEngine;
using Utils.Debugger;
using Utils.FSM;
using Logger = Utils.Debugger.Logger;

namespace GameStates
{
    public class BattlePhaseState : IStateBehaviour<GameState>
    {
        private readonly Logger _battleLog;
        private readonly ShipArea _opponentArea;
        private readonly ShipArea _playerArea;
        private bool _inBattle;

        public BattlePhaseState()
        {
            _battleLog = Debugger.Default.GetLogger("BattleLog");
            
            var connection = Connection.Instance;
            connection.MessageReceived.AddListener<S2CRoundBattlePhase>(OnBattleStarted);
            connection.MessageReceived.AddListener<S2CGameEvent>(OnGameEvent);
            connection.MessageReceived.AddListener<S2CRoundEnded>(OnRoundEnded);

            _playerArea = GameManager.Instance.PlayerArea;
            _opponentArea = GameManager.Instance.OpponentArea;
        }

        public void StateStarted()
        {
            _inBattle = true;

            UIBattlePanel.Instance.UpdatePlayerShipState();
            UIBattlePanel.Instance.UpdateOpponentShipState();
            UIBattlePanel.Instance.Show();
            UIRoundToolbar.Instance.StopAndHideTimer();

            CameraController.Instance.SetTarget(GameManager.Instance.BattleTransform);
        }

        public GameState? StateUpdate()
        {
            return null;
        }

        public void StateEnded()
        {
            _inBattle = false;
            UIBattlePanel.Instance.Hide();
        }

        private void OnBattleStarted(S2CRoundBattlePhase e)
        {
            _battleLog.Log("<b>Battle started</b>");
            SoundManager.Instance.Play(GameManager.Instance.BattleStartedSound);
            ShipState playerShip;
            ShipState opponentShip;

            if (GameManager.Instance.PlayerIndex == 1)
            {
                playerShip = e.player1_ship;
                opponentShip = e.player2_ship;
            }
            else
            {
                playerShip = e.player2_ship;
                opponentShip = e.player1_ship;
            }

            // Force set to submitted ships, might mismatch
            _playerArea.SetShipState(playerShip);
            _opponentArea.SetShipState(opponentShip);
        }

        private void OnGameEvent(S2CGameEvent e)
        {
            if (!_inBattle)
            {
                Debug.LogWarning("Received game event but not during battle phase");
                return;
            }

            var playerState = e.player1_ship;
            var opponentState = e.player2_ship;
            if (GameManager.Instance.PlayerIndex == 2)
            {
                playerState = e.player2_ship;
                opponentState = e.player1_ship;
            }

            _playerArea.SetShipState(playerState);
            _opponentArea.SetShipState(opponentState);
            UIBattlePanel.Instance.UpdatePlayerShipState();
            UIBattlePanel.Instance.UpdateOpponentShipState();

            Debug.Log($"[BattleManager] Game events t={e.time}, total {e.events.Length} events");
            foreach (var ePayload in e.events)
            {
                var sourceShip = GetShip(ePayload.source.player_index);
                var sourceSlot = sourceShip.GetSlot(ePayload.source.slot_id);

                var targetShip = GetShip(ePayload.target.player_index);
                var targetSlot = targetShip.GetSlot(ePayload.target.slot_id);

                // Sent event identifiers to actual objects
                var gameEvent = new GameEvent
                {
                    Name = ePayload.name,
                    Value = ePayload.value,
                    SourceShip = sourceShip,
                    SourceSlot = sourceSlot,
                    TargetShip = targetShip,
                    TargetSlot = targetSlot
                };

                _battleLog.Log($"<b>[{gameEvent.Name}:{ePayload.value}]</b> " +
                               $"p{ePayload.source.player_index}/{sourceSlot.Id}/{sourceSlot.ModuleId} -> " +
                               $"p{ePayload.target.player_index}/{targetSlot.Id}/{targetSlot.ModuleId}");

                // Dispatch event to slots
                sourceSlot.OnGameEventFired(gameEvent);
                targetSlot.OnGameEventReceived(gameEvent);
            }
        }

        private void OnRoundEnded(S2CRoundEnded e)
        {
            if (!_inBattle)
            {
                Debug.LogWarning("Round ended, but not during battle phase");
                return;
            }

            _battleLog.Log("<b>Battle ended</b>");

            // TODO: UI AND VISUALS
            if (e.winner == GameManager.Instance.PlayerIndex)
            {
                // You won :)
                Debug.Log("[BattleManager] Round ended: You won!");
                SoundManager.Instance.Play(GameManager.Instance.WinSound);
            }
            else if (e.IsDraw)
            {
                // Tie :|
                Debug.Log("[BattleManager] Round ended: Tie!");
                SoundManager.Instance.Play(GameManager.Instance.WinSound);
            }
            else
            {
                // You lost! :(
                Debug.Log("[BattleManager] Round ended: You lost!");
                SoundManager.Instance.Play(GameManager.Instance.LoseSound);
            }

            UIRoundToolbar.Instance.UpdateRoundHistory(e.history);
        }

        private Ship GetShip(int requestedIndex)
        {
            if (GameManager.Instance.PlayerIndex == 1)
                return requestedIndex == 1 ? _playerArea.Ship : _opponentArea.Ship;

            return requestedIndex == 1 ? _opponentArea.Ship : _playerArea.Ship;
        }
    }
}