using UI;
using Utils.FSM;

namespace GameStates
{
    public class LoginState : IStateBehaviour<GameState>
    {
        public void StateStarted()
        {
            CameraController.Instance.SetTarget(GameManager.Instance.MenuTransform);
            UILoginScreen.Instance.Show();
            UIRoundToolbar.Instance.Hide();
        }

        public GameState? StateUpdate()
        {
            return null;
        }

        public void StateEnded()
        {
            UILoginScreen.Instance.Hide();
        }
    }
}