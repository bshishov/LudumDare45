using Model;
using Network;
using UI;
using Utils.FSM;

namespace GameStates
{
    public class GameEndedState : IStateBehaviour<GameState>
    {
        public GameEndedState()
        {
            Connection.Instance.MessageReceived.AddListener<S2CGameEnded>(OnGameEnded);
        }

        public void StateStarted()
        {
            CameraController.Instance.SetTarget(GameManager.Instance.BattleTransform);
        }

        public GameState? StateUpdate()
        {
            return null;
        }

        public void StateEnded()
        {
            UIGameOverScreen.Instance.Hide();
        }

        private void OnGameEnded(S2CGameEnded e)
        {
            if (!e.interrupted)
            {
                // TODO: Do something about that. We need parametrized start
                if (GameManager.Instance.PlayerIndex == 1)
                    UIGameOverScreen.Instance.Show(e.winner, e.player1_profile);

                if (GameManager.Instance.PlayerIndex == 2)
                    UIGameOverScreen.Instance.Show(e.winner, e.player2_profile);
            }
        }
    }
}