using Model;
using UI;
using Utils.FSM;

namespace GameStates
{
    public class GameCreatedState : IStateBehaviour<GameState>
    {
        public void StateStarted()
        {
            CameraController.Instance.SetTarget(GameManager.Instance.BattleTransform);

            UIBattlePanel.Instance.UpdatePlayerProfile();
            UIBattlePanel.Instance.UpdateOpponentProfile();
            UISetupPanel.Instance.ArmoryModules.Clear();

            UIGameCreatedScreen.Instance.Show(Profile.Current, GameManager.Instance.OpponentProfile);
            UIRoundToolbar.Instance.SetRoundsToWin(Database.Instance.Meta.should_win);
        }

        public GameState? StateUpdate()
        {
            return null;
        }

        public void StateEnded()
        {
            UIGameCreatedScreen.Instance.Hide();
        }
    }
}