using UI;
using Utils.FSM;

namespace GameStates
{
    public class MainMenu : IStateBehaviour<GameState>
    {
        public void StateStarted()
        {
            CameraController.Instance.SetTarget(GameManager.Instance.MenuTransform);
            UIMainMenuScreen.Instance.Show();
            UIRoundToolbar.Instance.Hide();
        }

        public GameState? StateUpdate()
        {
            return null;
        }

        public void StateEnded()
        {
            UIMainMenuScreen.Instance.Hide();
        }
    }
}