using UI;
using Utils.FSM;

namespace GameStates
{
    public class BuildPhaseState : IStateBehaviour<GameState>
    {
        public void StateStarted()
        {
            CameraController.Instance.SetTarget(GameManager.Instance.SetupTransform);
            UIRoundToolbar.Instance.SetTimer(GameManager.Instance.CurrentRound.build_phase_time);
            UISetupPanel.Instance.Show();
        }

        public GameState? StateUpdate()
        {
            return null;
        }

        public void StateEnded()
        {
            UISetupPanel.Instance.Hide();
        }
    }
}