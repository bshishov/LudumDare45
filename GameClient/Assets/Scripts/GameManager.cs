﻿using GameStates;
using Model;
using Network;
using UI;
using UnityEngine;
using Utils;
using Utils.FSM;

public class GameManager : Singleton<GameManager>
{
    private readonly StateMachine<GameState> _stateMachine =
        new StateMachine<GameState>();

    private Connection _connection;

    [Header("Sounds")] public Sound BattleStartedSound;

    public Transform BattleTransform;
    public Sound LoseSound;
    public Transform MenuTransform;
    public ShipArea OpponentArea;

    [Header("Ship areas")] public ShipArea PlayerArea;

    [Header("Camera positions")] public Transform SetupTransform;

    public Sound WinSound;

    public int PlayerIndex { get; private set; }
    public Profile PlayerProfile => Profile.Current;
    public Profile OpponentProfile { get; private set; }
    public RoundMeta CurrentRound { get; private set; }
    public int RoundNumber { get; private set; }
    public int Money { get; private set; }

    public GameState? CurrentState => _stateMachine.CurrentState;

    private void Start()
    {
        _connection = Connection.Instance;
        _connection.MessageReceived.AddListener<S2CGameCreated>(OnGameCreated);
        _connection.MessageReceived.AddListener<S2CGameStarted>(OnGameStarted);
        _connection.MessageReceived.AddListener<S2CRoundStarted>(OnRoundStarted);
        _connection.MessageReceived.AddListener<S2CMoneyUpdated>(OnMoneyUpdated);
        _connection.MessageReceived.AddListener<S2CRoundBattlePhase>(OnBattlePhase);
        _connection.MessageReceived.AddListener<S2CRoundBuildPhase>(OnBuildPhase);
        _connection.MessageReceived.AddListener<S2CGameEnded>(OnGameEnded);
        _connection.MessageReceived.AddListener<S2CShipUpdated>(OnShipUpdated);
        _connection.MessageReceived.AddListener<S2CHello>(OnHello);

        _connection.Connected += OnConnected;
        _connection.Disconnected += OnDisconnect;

        // State machine
        _stateMachine.AddState(GameState.MainMenu, new MainMenu());
        _stateMachine.AddState(GameState.GameCreated, new GameCreatedState());
        _stateMachine.AddState(GameState.BattlePhase, new BattlePhaseState());
        _stateMachine.AddState(GameState.BuildPhase, new BuildPhaseState());
        _stateMachine.AddState(GameState.GameEnded, new GameEndedState());
        _stateMachine.AddState(GameState.Login, new LoginState());
        _stateMachine.SwitchToState(GameState.Login);
    }

    private void OnShipUpdated(S2CShipUpdated e)
    {
        Debug.Log("[GameManager] Ship updated");
        PlayerArea.SetShipState(e.ship);
    }

    private void OnHello(S2CHello e)
    {
        // Save token received from server
        PlayerPrefs.SetString(Common.PlayerPrefsKeys.AuthToken, e.auth_token);

        // Set profile
        Profile.Current = e.profile;
        UIMainMenuScreen.Instance.MainProfile.SetProfile(e.profile);

        _stateMachine.SwitchToState(GameState.MainMenu);
    }

    private void OnConnected()
    {
        Debug.Log("[GameManager] Connected");
    }

    private void OnMoneyUpdated(S2CMoneyUpdated e)
    {
        Money = e.new_amount;
        Debug.Log($"[GameManager] Money updated new_amount={e.new_amount}");
    }

    private void OnRoundStarted(S2CRoundStarted e)
    {
        Debug.Log("[GameManager] Round started");
        CurrentRound = e.round;
        RoundNumber = e.round_number;

        UIRoundToolbar.Instance.SetRound(e.round_number);
        UIRoundToolbar.Instance.Show();
    }

    private void OnBuildPhase(S2CRoundBuildPhase e)
    {
        Debug.Log("[GameManager] Build phase");
        _stateMachine.SwitchToState(GameState.BuildPhase);
    }

    private void OnBattlePhase(S2CRoundBattlePhase e)
    {
        Debug.Log("[GameManager] Battle phase");
        _stateMachine.SwitchToState(GameState.BattlePhase);
    }

    private void OnGameEnded(S2CGameEnded e)
    {
        Debug.Log("[GameManager] Game ended, " +
                  $"winner: {e.winner} " +
                  $"interrupted: {e.interrupted_reason} " +
                  $"reason: {e.interrupted_reason}");

        if (PlayerIndex == 1 && e.player1_profile != null)
            Profile.Current = e.player1_profile;

        if (PlayerIndex == 2 && e.player2_profile != null)
            Profile.Current = e.player2_profile;

        UIMainMenuScreen.Instance.MainProfile.SetProfile(Profile.Current);

        if (e.interrupted)
        {
            _stateMachine.SwitchToState(GameState.MainMenu);
            return;
        }

        _stateMachine.SwitchToState(GameState.GameEnded);
    }

    private void OnDisconnect()
    {
        Debug.Log("[GameManager] Disconnected");
        _stateMachine.SwitchToState(GameState.Login);
    }

    private void OnGameStarted(S2CGameStarted e)
    {
        Debug.Log("[GameManager] Game started");
    }

    private void OnGameCreated(S2CGameCreated e)
    {
        Debug.Log($"[GameManager] Game created, player: index: {e.player_index}, opponent: {e.opponent.name}");

        OpponentProfile = e.opponent;
        PlayerIndex = e.player_index;

        PlayerArea.SetShipState(null);
        OpponentArea.SetShipState(null);

        _stateMachine.SwitchToState(GameState.GameCreated);
    }

    private void Update()
    {
        _stateMachine.Update();
    }

    public void ToMenu()
    {
        _stateMachine.SwitchToState(GameState.MainMenu);
    }
}