using GameStates;
using UI;
using UI.DragDrop;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Slot))]
public class SlotDragger : Dragger<ModuleDragData>
{
    private Slot _slot;
    public GameObject UIModuleItemPrefab;

    private void Start()
    {
        _slot = GetComponent<Slot>();
    }

    protected override void DragWasReceived(DragReceiver<ModuleDragData> receiver, ModuleDragData data)
    {
        _slot.Unmount(true);
        Destroy(data.Transform.gameObject);
    }

    protected override void DragWasCancelled(ModuleDragData data)
    {
        Destroy(data.Transform.gameObject);
    }

    protected override ModuleDragData DragStarted(PointerEventData eventData)
    {
        if (_slot.HasModule && UIModuleItemPrefab != null)
        {
            // Dragging is only allowed during build phase
            var gameState = GameManager.Instance.CurrentState;
            if (!(gameState.HasValue && gameState.Value == GameState.BuildPhase))
                return null;

            var canvas = UISlotsOverlay.Instance.transform.root.GetComponent<Canvas>();
            var go = Instantiate(UIModuleItemPrefab, canvas.transform);

            var moduleItem = go.GetComponent<UIModuleItem>();
            if (moduleItem != null)
            {
                moduleItem.Setup(_slot.Module);
                return new ModuleDragData(go.transform, moduleItem, _slot, ModuleDragSource.Field);
            }
        }

        return null;
    }
}