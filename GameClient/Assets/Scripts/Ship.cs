using System.Collections.Generic;
using System.Linq;
using Model;
using UnityEngine;
using Utils.Debugger;
using WaterPhysics;

public class Ship : MonoBehaviour
{
    private Floating _floating;

    private List<Slot> _slots = new List<Slot>();
    public string ShipKey = "default";

    public Sound SinkSound;
    public IEnumerable<Slot> Slots => _slots;
    public ShipMeta Meta { get; private set; }

    private void Awake()
    {
        _slots = GetComponentsInChildren<Slot>().ToList();
        _floating = GetComponent<Floating>();
        Setup(Database.Instance.Ships[ShipKey]);
    }

    private void Update()
    {
        foreach (var slot in _slots)
        {
            var path = $"Ships/{Meta.name}/{slot.Id}";
            Debugger.Default.Display(path, slot.ModuleId);
            Debugger.Default.Display(path + "/Unmount", () => slot.Unmount());
        }
    }

    private void Setup(ShipMeta shipData)
    {
        // Remove old slots
        foreach (var slot in _slots) slot.Unmount();

        Debug.Log($"[Ship:{gameObject.name}] Constructing ship: {shipData.id}");
        Meta = shipData;

        foreach (var slot in Meta.slots)
        {
            var slotComponent = _slots.FirstOrDefault(s => s.SlotKey == slot.id);
            if (slotComponent != null)
                slotComponent.Setup(this, slot);
            else
                Debug.LogWarning($"[Ship:{gameObject.name}] Missing slot component for slot in db: {slot.id}");
        }
    }

    public void Impact(Vector3 target, float amount = 1f)
    {
        if (_floating != null)
            _floating.ApplyForce(target, Vector3.down * amount * 30f, duration: 0.1f);
    }

    public ShipState GetState()
    {
        return new ShipState
        {
            id = Meta.id,
            slots = _slots.Select(s => s.GetState()).ToArray()
        };
    }

    public void SetState(ShipState shipState)
    {
        Debug.Log($"[Ship:{gameObject.name}] Updating state ship_id {shipState.id}");
        foreach (var slotState in shipState.slots)
        {
            var slotComponent = _slots.FirstOrDefault(s => s.Id == slotState.id);
            if (slotComponent != null)
                slotComponent.SetState(slotState);
            else
                Debug.LogWarning($"[Ship:{gameObject.name}] No slot component found for slot {slotState.id}");
        }
    }

    public Slot GetSlot(string id)
    {
        return _slots.FirstOrDefault(s => s.Id == id);
    }

    public float GetMaxHp()
    {
        var maxHp = 0f;
        foreach (var slot in _slots)
            if (slot.HasModule)
                maxHp += slot.BaseHp;

        return maxHp;
    }

    public float GetHp()
    {
        var hp = 0f;
        foreach (var slot in _slots)
            if (slot.HasModule)
                hp += slot.Hp;
        return hp;
    }

    [ContextMenu("Clear")]
    public void Clear()
    {
        foreach (var slot in _slots)
            slot.Clear();
    }
}