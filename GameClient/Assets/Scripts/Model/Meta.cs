using System;
using UnityEngine;

// ReSharper disable InconsistentNaming

namespace Model
{
    [Serializable]
    public class Slot
    {
        public string id;
        public int line;
        public int position_x;
        public int position_y;
        public int priority;
    }

    [Serializable]
    public class ModulePriority
    {
        public int additional_priority;
        public string module_id;
    }

    [Serializable]
    public class ModuleMeta
    {
        public int base_damage;
        public int base_hp;
        public int base_priority;
        public bool can_be_bought;
        public string description;
        public float fire_additional_random_delay;
        public float fire_delay;
        public string id;
        public string name;
        public int price;
        public ModulePriority[] target_priorities;
        public string[] traits;
        public int upgrade_tier;
        public string upgrades_to;

        public ModuleState CreateDefaultState()
        {
            Debug.Log($"[ModuleData:{id}] Creating default state");
            return new ModuleState
            {
                id = id,
                hp = base_hp,
                damage = base_damage,
                fire_delay = fire_delay,
                priority = base_priority,
                random_delay = fire_additional_random_delay
            };
        }
    }

    [Serializable]
    public class ShipMeta
    {
        public string description;
        public string id;
        public string name;
        public Slot[] slots;
    }

    [Serializable]
    public class TraitMeta
    {
        public string description;
        public string id;
        public string name;
    }

    [Serializable]
    public class RoundMeta
    {
        public int build_phase_time;
    }

    [Serializable]
    public class Meta : IPayload
    {
        public ModuleMeta[] modules;
        public int pregame_time;
        public RoundMeta[] rounds;
        public string ship;
        public ShipMeta[] ships;
        public int should_win;
        public int start_money;
        public TraitMeta[] traits;
        public string version;
    }
}