using System;

// ReSharper disable InconsistentNaming

namespace Model
{
    public interface IPayload
    {
    }

    public static class QueueType
    {
        public static string RankedMatch = "ranked_match";
        public static string BotMatch = "bot_match";
    }

    [Serializable]
    public class Profile
    {
        // Stores current (logged in) player profile 
        public static Profile Current;
        public int games_left;
        public int games_lost;
        public int games_played;
        public int games_streak;
        public int games_tied;
        public int games_won;
        public float joined;
        public float last_login;
        public string name;
        public float rating;
    }

    /*
    [Serializable]
    public class PlayerState
    {
        public ShipState ship;
        public ArmoryState armory;
        public int index;        
        public int money;
    }*/

    [Serializable]
    public class ShipState
    {
        public string id;
        public SlotState[] slots;
    }

    [Serializable]
    public class ModuleState
    {
        public float damage;
        public float fire_delay;
        public float hit_delay;
        public float hp;
        public string id;
        public float priority;
        public float random_delay; // TODO: COULD BE RETRIEVED FROM DB
        public float stun_time;
    }

    [Serializable]
    public class SlotState
    {
        public string id;
        public int line;
        public ModuleState module;
        public int priority; // TODO: COULD BE RETRIEVED FROM DB
    }

    [Serializable]
    public class EventTarget
    {
        public int player_index;
        public string slot_id;
    }

    [Serializable]
    public class Event
    {
        public string name;
        public EventTarget source;
        public EventTarget target;
        public float value;
    }

    [Serializable]
    public class S2CHello : IPayload
    {
        public string auth_token;
        public Meta meta;
        public Profile profile;
    }

    [Serializable]
    public class S2CStartedMatchmaking : IPayload
    {
        public int active_players;
        public string queue_type;
    }

    [Serializable]
    public class S2CStoppedMatchmaking : IPayload
    {
        public string reason;
    }

    [Serializable]
    public class S2CMatchmakingUpdate : IPayload
    {
        public int players_in_battle;
        public int players_in_matchmaking;
        public int players_total;
    }

    [Serializable]
    public class S2CGameCreated : IPayload
    {
        public Profile opponent;
        public int player_index;
    }

    [Serializable]
    public class S2CGameStarted : IPayload
    {
    }

    [Serializable]
    public class S2CGameEvent : IPayload
    {
        public Event[] events;
        public ShipState player1_ship;
        public ShipState player2_ship;
        public float time;
    }

    [Serializable]
    public class S2CGameEnded : IPayload
    {
        public bool interrupted;
        public string interrupted_reason;
        public Profile player1_profile;
        public Profile player2_profile;
        public int winner;

        public bool IsDraw => winner == 0;
    }

    [Serializable]
    public class S2CRoundStarted : IPayload
    {
        public RoundMeta round;
        public int round_number;
    }

    [Serializable]
    public class S2CRoundBuildPhase : IPayload
    {
        public ShipState ship;
    }

    [Serializable]
    public class S2CRoundBattlePhase : IPayload
    {
        public ShipState player1_ship;
        public ShipState player2_ship;
    }

    [Serializable]
    public class S2CRoundEnded : IPayload
    {
        public int[] history;
        public int round_number;
        public int winner;

        public bool IsDraw => winner == 0;
    }

    [Serializable]
    public class S2CMoneyUpdated : IPayload
    {
        public int delta;
        public int new_amount;
    }

    [Serializable]
    public class S2CPlayerReady : IPayload
    {
        public int player_index;
    }

    [Serializable]
    public class S2CShipUpdated : IPayload
    {
        public ShipState ship;
    }

    [Serializable]
    public class C2SHello : IPayload
    {
        public string name;
        public string token;
        public string version;
    }

    [Serializable]
    public class C2SStartMatchmaking : IPayload
    {
        public string queue_type;
    }

    [Serializable]
    public class C2SStopMatchmaking : IPayload
    {
    }

    [Serializable]
    public class C2SModuleMove : IPayload
    {
        public string slot_from;
        public string slot_to;
    }

    [Serializable]
    public class C2SModuleBuy : IPayload
    {
        public string module;
        public string slot;
    }

    [Serializable]
    public class C2SModuleSell : IPayload
    {
        public string slot;
    }

    [Serializable]
    public class C2SReady : IPayload
    {
    }
}