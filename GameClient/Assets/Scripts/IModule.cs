using Model;
using UnityEngine.EventSystems;

public interface IModule : IEventSystemHandler
{
    void OnMounted(Slot slot);
    void OnUnmounted();
    void OnFiredEvent(GameEvent e);
    void OnReceivedEvent(GameEvent e);
    void OnStateChanged(ModuleState state);
}