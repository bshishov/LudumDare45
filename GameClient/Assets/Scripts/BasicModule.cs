using Model;
using UnityEngine;
using Utils;

public class BasicModule : MonoBehaviour, IModule
{
    private GameObject _smokeInstance;

    [Header("Animator")] public Animator Animator;

    [Header("Sounds")] public Sound FireSound;

    public GameObject Heal;
    public Sound MountedSound;

    [Header("FX")] public GameObject Smoke;

    public Sound UnmountedSound;

    public void OnMounted(Slot slot)
    {
        SoundManager.Instance.Play(MountedSound);
    }

    public void OnUnmounted()
    {
        SoundManager.Instance.Play(UnmountedSound);
    }

    public void OnFiredEvent(GameEvent e)
    {
        if (e.Name == GameEvent.Fire)
        {
            SoundManager.Instance.Play(FireSound);

            if (Animator != null)
                Animator.SetTrigger("fire");
        }
    }

    public void OnReceivedEvent(GameEvent e)
    {
        if (e.Name == GameEvent.Death)
            if (e.TargetSlot.HasModule)
            {
                if (_smokeInstance == null)
                    _smokeInstance = GameObjectUtils.InstantiateKeepScaleRotation(Smoke, transform);

                if (Animator != null)
                    Animator.SetTrigger("death");
            }

        if (e.Name == GameEvent.Heal)
            if (Heal != null)
                Instantiate(Heal, transform.position, Quaternion.identity);
    }

    public void OnStateChanged(ModuleState state)
    {
        if (state?.hp > 0 && _smokeInstance != null)
        {
            Destroy(_smokeInstance);
            _smokeInstance = null;

            if (Animator != null)
                Animator.SetTrigger("reset");
        }
    }
}