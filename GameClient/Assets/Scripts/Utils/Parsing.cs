using System.Globalization;

namespace Utils
{
    public static class Parsing
    {
        public static bool TryParseFloat(string str, out float value)
        {
            return float.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out value);
        }

        public static float? TryParseFloat(string str)
        {
            if (float.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out var value))
                return value;
            return null;
        }

        public static float TryParseFloat(string str, float defaultValue)
        {
            if (float.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out var value))
                return value;
            return defaultValue;
        }
    }
}