using System;
using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
    public class StateManager<T>
    {
        private readonly IDictionary<string, T> _state
            = new Dictionary<string, T>();

        private readonly IDictionary<string, List<Action<T>>> _subscriptions
            = new Dictionary<string, List<Action<T>>>();

        public void Subscribe(string path, Action<T> callback)
        {
            if (string.IsNullOrEmpty(path))
            {
                Debug.LogWarning("Empty subscription path");
                return;
            }

            if (!_subscriptions.TryGetValue(path, out var subscriptions))
            {
                subscriptions = new List<Action<T>>();
                _subscriptions.Add(path, subscriptions);
            }

            subscriptions.Add(callback);
        }

        public void Unsubscribe(string path, Action<T> callback)
        {
            if (_subscriptions.TryGetValue(path, out var subscriptions))
                subscriptions?.Remove(callback);
        }

        public void Set(string path, T value)
        {
            if (string.IsNullOrEmpty(path))
            {
                Debug.LogWarning("Empty path");
                return;
            }

            if (_state.TryGetValue(path, out var oldVal))
            {
                _state[path] = value;
                InvokeSubscriptions(path, value);
            }
            else
            {
                _state.Add(path, value);
                InvokeSubscriptions(path, value);
            }

            if (_state.ContainsKey(path))
                _state[path] = value;
            else
                _state.Add(path, value);
        }

        public bool TryGet(string path, out T value)
        {
            return _state.TryGetValue(path, out value);
        }

        public void ClearState()
        {
            _state.Clear();
        }

        private void InvokeSubscriptions(string path, T state)
        {
            if (_subscriptions.TryGetValue(path, out var subscriptions))
                foreach (var callback in subscriptions)
                    callback?.Invoke(state);
        }
    }
}