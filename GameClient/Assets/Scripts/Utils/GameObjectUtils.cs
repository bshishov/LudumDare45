using UnityEngine;

namespace Utils
{
    public static class GameObjectUtils
    {
        public static GameObject InstantiateKeepScaleRotation(GameObject prefab, Transform parent)
        {
            var go = Object.Instantiate(prefab, parent.position + prefab.transform.position, Quaternion.identity);
            go.transform.SetParent(parent, true);
            return go;
        }

        public static GameObject InstantiateKeepScaleRotation(GameObject prefab, Transform parent, float ttl)
        {
            if (prefab == null || parent == null)
                return null;
            var go = InstantiateKeepScaleRotation(prefab, parent);
            Object.Destroy(go, ttl);
            return go;
        }
    }
}