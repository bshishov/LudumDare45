﻿using System.Linq;
using UnityEngine;
using Utils.Debugger.Widgets;

namespace Utils.Debugger
{
    public class Logger : IWidget
    {
        private readonly FixedSizeStack<string> _messages;
        private readonly bool _unityLog;
        private readonly int _rows;
        private Vector2 _scrollPosition = Vector2.zero;

        public Logger(int historySize = 100, bool unityLog = true, int rows = 20)
        {
            _messages = new FixedSizeStack<string>(historySize);
            _unityLog = unityLog;
            _rows = rows;
        }

        public Vector2 GetSize(Style style)
        {
            return new Vector2(700f, style.LineHeight * _rows);
            ;
        }

        public void Draw(Rect rect, Style style)
        {
            const float width = 1200f;
            _scrollPosition = GUI.BeginScrollView(rect, _scrollPosition,
                new Rect(0, 0, width, style.LineHeight * _messages.Size), false, true);

            var currentY = 0f;
            foreach (var message in _messages.Reverse())
            {
                GUI.Label(new Rect(10, currentY, width, style.LineHeight), message);
                currentY += style.LineHeight;
            }

            GUI.EndScrollView();
        }

        public void Log(string message)
        {
            _messages.Push(message);
            if (_unityLog)
                Debug.Log(message);
        }

        public void LogFormat(string message, params object[] args)
        {
            _messages.Push(string.Format(message, args));
            if (_unityLog)
                Debug.LogFormat(message, args);
        }
    }
}