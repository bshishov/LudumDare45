using System;
using System.Collections.Generic;
using UnityEngine;
using Utils.Debugger;

namespace WaterPhysics
{
    public class Floating : MonoBehaviour
    {
        public enum Space
        {
            Local,
            World
        }

        private readonly List<ForceData> _forces = new List<ForceData>();
        private Vector3 _angularVelocity;

        private Vector3 _velocity;
        public float AirDrag = 1f;

        [Header("Debug")] public Vector3 DebugPushAt;

        public float DebugPushDuration;
        public Vector3 DebugPushForce;

        public Node[] Nodes;
        public float WaterDensity = 10f;
        public float WaterDrag = 1f;

        [Header("Water")] public float WaterLevel;
        public float WaveAmplitude = 0.1f;
        public Vector2 WaveSize = Vector2.one;
        public Vector2 WaveSpeed = Vector2.one;

        private void Start()
        {
            Debugger.Default.Display($"{gameObject.name}/Push", () =>
            {
                var at = transform.TransformPoint(DebugPushAt);
                ApplyForce(at, DebugPushForce, Space.World, DebugPushDuration);
                Debugger.Default.DrawRay(at, DebugPushForce, Color.black, DebugPushDuration);
            });
        }

        private void FixedUpdate()
        {
            var totalForce = Vector3.zero;
            var totalMass = 0f;
            var totalTorque = Vector3.zero;
            var momentOfInertia = CoMMomentOfInertia();

            foreach (var node in Nodes)
            {
                var nodePos = transform.TransformPoint(node.Position);
                var waterLevel = WaterLevel + Wave(nodePos);
                var nodeForce =
                    GetBuoyancyForce(nodePos, node.Size, waterLevel) +
                    GetGravityForce(node.Mass) +
                    GetDragForce(nodePos, node.Size, waterLevel);
                totalForce += nodeForce;
                totalMass += node.Mass;
                totalTorque += Vector3.Cross(nodePos - transform.position, nodeForce);
            }

            foreach (var force in _forces)
            {
                var position = force.Target;
                if (force.Space == Space.Local)
                    position = transform.TransformPoint(force.Target);

                totalForce += force.Force;
                totalTorque += Vector3.Cross(position - transform.position, force.Force);
                force.Duration -= Time.deltaTime;
            }

            var acc = totalForce / totalMass;
            var angularAcc = totalTorque / momentOfInertia;

            _velocity += acc * Time.deltaTime;
            _angularVelocity += angularAcc * Time.deltaTime;

            _velocity *= 0.999f;
            _angularVelocity *= 0.999f;

            transform.position += _velocity * Time.deltaTime;
            transform.rotation = DeltaRotation(_angularVelocity) * transform.rotation;

            _forces.RemoveAll(f => f.Duration < 0);
        }

        private Quaternion DeltaRotation(Vector3 angularVelocity)
        {
            var ha = 0.5f * Time.deltaTime * angularVelocity;
            var l = ha.magnitude;
            if (l > 0)
            {
                var ss = Mathf.Sin(l) / l;
                return new Quaternion(ha.x * ss, ha.y * ss, ha.z * ss, Mathf.Cos(l));
            }

            return new Quaternion(ha.x, ha.y, ha.z, 1.0f);
        }

        private Quaternion DeltaRotationApprox(Vector3 angularVelocity)
        {
            var ha = 0.5f * Time.deltaTime * angularVelocity;
            return new Quaternion(ha.x, ha.y, ha.z, 1.0f);
        }

        public ForceData ApplyForce(Vector3 at, Vector3 force, Space space = Space.World, float duration = 0f)
        {
            var f = new ForceData
            {
                Target = at,
                Force = force,
                Duration = duration,
                Space = space
            };
            _forces.Add(f);
            return f;
        }

        public void RemoveForce(ForceData force)
        {
            _forces.Remove(force);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;

            if (Nodes != null)
                foreach (var node in Nodes)
                {
                    var wPos = transform.TransformPoint(node.Position);
                    Gizmos.DrawWireCube(wPos, node.Size);
                }
        }

        private float CoMMomentOfInertia()
        {
            var momentOfInertia = 0f;
            foreach (var node in Nodes)
                momentOfInertia +=
                    node.Mass * (transform.TransformPoint(node.Position) - transform.position).sqrMagnitude;

            return momentOfInertia;
        }

        private Vector3 GetBuoyancyForce(Vector3 position, Vector3 boxSize, float waterLevel)
        {
            var area = boxSize.x * boxSize.z;
            var maxHeight = boxSize.y;

            var heightUnderWater = waterLevel - (position.y - maxHeight * 0.5f);
            if (heightUnderWater > 0)
            {
                heightUnderWater = Mathf.Clamp(heightUnderWater, 0, maxHeight);
                var underwaterVolume = area * heightUnderWater;
                return -WaterDensity * underwaterVolume * UnityEngine.Physics.gravity;
            }

            return Vector3.zero;
        }

        private Vector3 GetGravityForce(float mass)
        {
            return UnityEngine.Physics.gravity * mass;
        }

        private Vector3 VelocityAt(Vector3 position)
        {
            return Vector3.Cross(_angularVelocity, position - transform.position) + _velocity;
        }

        private Vector3 GetDragForce(Vector3 position, Vector3 boxSize, float waterLevel)
        {
            var heightUnderWater = waterLevel - (position.y - boxSize.y * 0.5f);
            var k = Mathf.Clamp01(heightUnderWater / boxSize.y); // 1 if fully inside, 0 fully outside water

            // Drag blending
            var drag = Mathf.Lerp(AirDrag, WaterDrag, k);

            // Apply drag
            var v = VelocityAt(position);
            return 0.5f * v.sqrMagnitude * drag * boxSize.x * boxSize.z * (-v).normalized;
        }

        private float Wave(Vector3 position)
        {
            return Mathf.Sin(position.x / WaveSize.x + WaveSpeed.x * Time.time) * WaveAmplitude +
                   Mathf.Cos(position.z / WaveSize.y + WaveSpeed.y * Time.time) * WaveAmplitude;
        }

        [ContextMenu("Reset")]
        public void Reset()
        {
            _forces.Clear();
            _velocity = Vector3.zero;
            _angularVelocity = Vector3.zero;
        }

        [Serializable]
        public class Node
        {
            public float Mass = 1f;
            public Vector3 Position;
            public Vector3 Size;
        }

        [Serializable]
        public class ForceData
        {
            public float Duration;
            public Vector3 Force;
            public Space Space;
            public Vector3 Target;
        }
    }
}