﻿using UnityEngine;
using WaterPhysics;

namespace Physics
{
    public class Joint : MonoBehaviour
    {
        private Floating.ForceData _force;

        public float K = 1;
        public Vector3 SourceAnchor;
        public Floating Target;
        public Vector3 TargetAnchor;

        [Header("Dimensions")] public bool X = true;

        public bool Y = true;
        public bool Z = true;

        private void FixedUpdate()
        {
            if (Target != null)
            {
                var dst = Target.transform.TransformPoint(TargetAnchor);
                var src = transform.TransformPoint(SourceAnchor);
                var dir = src - dst;
                if (!X) dir.x = 0;
                if (!Y) dir.y = 0;
                if (!Z) dir.z = 0;

                if (_force == null)
                {
                    _force = Target.ApplyForce(TargetAnchor, dir * K, duration: 0.1f);
                }
                else
                {
                    _force.Duration = 0.1f;
                    _force.Force = dir * K;
                    _force.Target = dst;
                }
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (Target != null)
            {
                Gizmos.color = Color.red;
                var src = transform.TransformPoint(SourceAnchor);
                var dst = Target.transform.TransformPoint(TargetAnchor);

                Gizmos.DrawSphere(src, 0.1f);
                Gizmos.DrawSphere(dst, 0.1f);
                Gizmos.DrawLine(src, dst);
            }
        }
    }
}