using System;
using Events;
using Model;
using Newtonsoft.Json;
using UI;
using UnityEngine;
using UnityEngine.EventSystems;
using Utils;
using Utils.Debugger;

public class Slot :
    MonoBehaviour,
    IPointerEnterHandler,
    IPointerExitHandler
{
    private static int _debugId;
    private GameObject _moduleObject;

    private Ship _ship;
    private UISlot _uiSlot;
    public GameObject MountedFX;

    [Header("Sound")] public Sound MountedSound;

    public Selector Selector;

    [Header("Meta")] public string SlotKey;

    public Sound UnmountedSound;

    public Model.Slot Meta { get; private set; }

    public bool HasModule => ModuleState != null;
    public string Id => Meta?.id;
    public string ModuleId => ModuleState?.id;
    public ModuleMeta Module => Database.Instance.Modules[ModuleState.id];
    public float Hp => Math.Max(0, ModuleState.hp);
    public float BaseHp => Module.base_hp;
    public ModuleState ModuleState { get; private set; }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (HasModule)
            UITooltip.Instance.Show(this);

        if (Selector != null)
            Selector.Show();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        UITooltip.Instance.Hide();

        if (Selector != null)
            Selector.Hide();
    }

    private void Awake()
    {
        if (UISlotsOverlay.Instance != null)
            _uiSlot = UISlotsOverlay.Instance.Add(this);
    }

    public void Setup(Ship ship, Model.Slot slot)
    {
        _ship = ship;
        Meta = slot;

        Debugger.Default.Display($"{gameObject.name}/slot-{slot.id}/ModuleState", new Vector2(400, 600),
            rect => { GUI.TextArea(rect, JsonConvert.SerializeObject(ModuleState, Formatting.Indented)); });

        Debugger.Default.Display($"{gameObject.name}/slot-{slot.id}/GetState()", new Vector2(400, 600),
            rect => { GUI.TextArea(rect, JsonConvert.SerializeObject(GetState(), Formatting.Indented)); });
    }

    public void Place(ModuleMeta moduleMeta)
    {
        var data = Database.Instance.Modules[moduleMeta.id];
        SetState(data.CreateDefaultState());

        SoundManager.Instance.Play(MountedSound);
        GameObjectUtils.InstantiateKeepScaleRotation(MountedFX, transform, 1f);

        if (Selector != null)
            Selector.Place();
    }

    public void Mount(ModuleState state)
    {
        Debug.Log($"[Slot:{Meta.id}] Mounting module {state.id}");

        ModuleState = state;

        var prefab = Database.Instance.GetModulePrefab(state.id);
        if (prefab != null)
        {
            _moduleObject = Instantiate(prefab, transform);
            _moduleObject.transform.position += Vector3.down * 0.5f;

            // Notify module gameObject
            CustomEvents.Execute(_moduleObject, CustomEvents.MountedHandler, this);
            CustomEvents.Execute(_moduleObject, CustomEvents.StateChangedHandler, state);
        }

        if (_uiSlot != null)
        {
            // Notify UI
            CustomEvents.Execute(_uiSlot.gameObject, CustomEvents.MountedHandler, this);
            CustomEvents.Execute(_uiSlot.gameObject, CustomEvents.StateChangedHandler, state);
        }
    }

    public void Unmount(bool byHand = false)
    {
        if (ModuleState != null)
            Debug.Log($"[Slot:{Meta.id}] Unmounted module: {ModuleState.id}");

        ModuleState = null;
        if (_moduleObject != null)
        {
            // Notify module gameObject
            CustomEvents.Execute(_moduleObject, CustomEvents.StateChangedHandler, ModuleState);
            CustomEvents.Execute(_moduleObject, CustomEvents.UnmountedHandler);

            Destroy(_moduleObject);
        }

        if (_uiSlot != null)
        {
            // Notify UI
            CustomEvents.Execute(_uiSlot.gameObject, CustomEvents.StateChangedHandler, ModuleState);
            CustomEvents.Execute(_uiSlot.gameObject, CustomEvents.UnmountedHandler);
        }

        if (byHand) SoundManager.Instance.Play(UnmountedSound);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position, Vector3.one);
        Gizmos.DrawLine(transform.position, transform.position + transform.forward);
    }

    public SlotState GetState()
    {
        return new SlotState
        {
            id = Id,
            priority = Meta.priority,
            line = Meta.line,
            module = ModuleState
        };
    }

    public void SetState(SlotState slotState)
    {
        //Debug.Log($"[Slot:{_slotMeta.id}] Updating state with slot state {slotState.id}");
        SetState(slotState.module);
    }

    private void SetState(ModuleState moduleState)
    {
        if (moduleState == null)
        {
            Unmount();
            return;
        }

        // New module placed -> mount
        if (ModuleState == null)
        {
            Mount(moduleState);
            return;
        }

        // If modules are different -> remount
        if (moduleState.id != ModuleState.id)
        {
            Unmount();
            Mount(moduleState);
            return;
        }

        // Update state of the same module
        ModuleState = moduleState;

        // Notify module gameObject
        if (_moduleObject != null)
            CustomEvents.Execute(_moduleObject, CustomEvents.StateChangedHandler, moduleState);

        // Notify UI
        if (_uiSlot != null)
            CustomEvents.Execute(_uiSlot.gameObject, CustomEvents.StateChangedHandler, moduleState);
    }

    private void OnDestroy()
    {
        if (_uiSlot != null)
            Destroy(_uiSlot.gameObject);
    }

    public void Clear()
    {
        Unmount();
    }

    public void OnGameEventFired(GameEvent gameEvent)
    {
        if (_moduleObject != null)
            CustomEvents.Execute(_moduleObject, CustomEvents.GameEventFiredHandler, gameEvent);

        if (_uiSlot != null)
            CustomEvents.Execute(_uiSlot.gameObject, CustomEvents.GameEventFiredHandler, gameEvent);
    }

    public void OnGameEventReceived(GameEvent gameEvent)
    {
        if (HasModule && gameEvent.Name == GameEvent.Hit) _ship.Impact(transform.position, 0.75f);

        if (_moduleObject != null)
            CustomEvents.Execute(_moduleObject, CustomEvents.GameEventReceivedHandler, gameEvent);
        if (_uiSlot != null)
            CustomEvents.Execute(_uiSlot.gameObject, CustomEvents.GameEventReceivedHandler, gameEvent);
    }
}