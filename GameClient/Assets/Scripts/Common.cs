public static class Common
{
    public static string LocalHost = "ws://localhost:6789/";
    public static string DebugHost = "ws://shishov.me:6790/";
    public static string ProdHost = "ws://fruitwars.shishov.me/";

    public static class PlayerPrefsKeys
    {
        public static string PlayerName = "PlayerName";
        public static string AuthToken = "AuthToken";
    }

    public static class SoundParameters
    {
        // You should set those in AudioMixer by exposing

        public static string MusicVolume = "MusicVolume";
        public static string SoundVolume = "SoundVolume";
    }

    public static class InputAxes
    {
        public static string Menu = "Menu";
    }
}