using System.Collections;
using Model;
using UnityEngine;

namespace Modules
{
    public class ShootProjectile : MonoBehaviour, IModule
    {
        public float DelayBeforeFire;
        public float FlyDuration = 1f;
        public AnimationCurve HeightCurve = AnimationCurve.Constant(0, 0, 0);
        public GameObject HitFx;
        public GameObject Projectile;

        public void OnMounted(Slot slot)
        {
        }

        public void OnUnmounted()
        {
        }

        public void OnFiredEvent(GameEvent e)
        {
            if (e.Name == GameEvent.Fire)
            {
                var source = e.SourceSlot.transform.position;
                var target = e.TargetSlot.transform.position;

                StartCoroutine(FlyRoutine(source, target));
            }
        }

        public void OnReceivedEvent(GameEvent e)
        {
        }

        public void OnStateChanged(ModuleState state)
        {
            // TODO: Remove projectile
        }

        private IEnumerator FlyRoutine(Vector3 sourcePosition, Vector3 targetPosition)
        {
            yield return new WaitForSeconds(DelayBeforeFire);
            var projectile = Instantiate(Projectile, sourcePosition, Quaternion.identity);
            var t = FlyDuration;

            while (t > 0)
            {
                var progress = 1f - Mathf.Clamp01(t / FlyDuration);
                var s = Vector3.Lerp(sourcePosition, targetPosition, progress);
                s += new Vector3(0, HeightCurve.Evaluate(progress), 0);

                projectile.transform.position = s;

                t -= Time.deltaTime;
                yield return null;
            }

            if (HitFx != null)
                Instantiate(HitFx, targetPosition, Quaternion.identity);

            Destroy(projectile);
        }
    }
}