﻿using System.Collections;
using Model;
using UnityEngine;

namespace Modules
{
    public class MachineGun : MoveProjectile, IModule
    {
        public float ArkHight, Margin = 2f, LifeTime = 1F; //LifeTime can not be < 1
        public GameObject MachineGunRound, SmokeAndShockWave, Explosion;
        public int NumberOfMachineGunRounds;
        public Transform s, t;

        public Vector3 startPos, targetPos;

        public void OnMounted(Slot slot)
        {
        }

        public void OnFiredEvent(GameEvent e)
        {
            if (e.Name == GameEvent.Fire)
            {
                startPos = transform.position;
                targetPos = e.TargetSlot.transform.position;
                StartCoroutine(ShootSequence());
            }
        }

        public void OnReceivedEvent(GameEvent e)
        {
        }

        public void OnUnmounted()
        {
        }

        public void OnStateChanged(ModuleState state)
        {
        }

        private void Awake()
        {
            if (s && t)
            {
                startPos = s.position;
                targetPos = t.position;
                InvokeRepeating(nameof(FireMachineGun), Time.deltaTime, LifeTime);
                InvokeRepeating(nameof(FireMachineGun), Time.deltaTime + 0.05F, LifeTime);
                InvokeRepeating(nameof(FireMachineGun), Time.deltaTime + 0.1F, LifeTime);
                InvokeRepeating(nameof(FireMachineGun), Time.deltaTime + 0.15F, LifeTime);

                InvokeRepeating(nameof(FireMachineGun), Time.deltaTime + 0.25F, LifeTime);
                InvokeRepeating(nameof(FireMachineGun), Time.deltaTime + 0.3F, LifeTime);
                InvokeRepeating(nameof(FireMachineGun), Time.deltaTime + 0.35F, LifeTime);
            }
        }

        public void FireMachineGun()
        {
            if (LifeTime < 1) LifeTime = 1;

            var instance = Instantiate(MachineGunRound);

            var middlePos = startPos + (targetPos - startPos) / 2 + Vector3.up * (ArkHight + Random.Range(0, Margin)) +
                            Vector3.forward * Random.Range(0, Margin);
            instance.transform.LookAt(targetPos + Vector3.up * 5);
            var instanceLifetime = LifeTime;
            StartCoroutine(FlyMortar(instance, startPos, middlePos, targetPos, instanceLifetime));
            StartCoroutine(WaitToDisableGO(instance.transform.GetChild(0).gameObject, instanceLifetime));

            Destroy(instance, instanceLifetime + 1F);

            StartCoroutine(WaitToInstantiateThenDestroy(Explosion, targetPos, LifeTime - 0.10F, 1F));

            var smokeInstance = Instantiate(SmokeAndShockWave, transform.position, transform.rotation);
            Destroy(smokeInstance, 6F);
        }

        public IEnumerator ShootSequence()
        {
            FireMachineGun();

            yield return null;
        }
    }
}