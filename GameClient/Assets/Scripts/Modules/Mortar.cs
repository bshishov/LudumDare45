﻿using Model;
using UnityEngine;

namespace Modules
{
    public class Mortar : MoveProjectile, IModule
    {
        public float ArkHight = 5.0f, Margin = 2f, LifeTime = 2f;
        public GameObject MortarRound, SmokeAndShockWave, Explosion;
        public int NumberOfMortarRounds;
        public Transform s, t;
        public Vector3 startPos, targetPos;

        public void OnMounted(Slot slot)
        {
        }

        public void OnUnmounted()
        {
        }

        public void OnFiredEvent(GameEvent e)
        {
            if (e.Name == GameEvent.Fire)
            {
                startPos = transform.position;
                targetPos = e.TargetSlot.transform.position;
                FireMortar();
            }
        }

        public void OnReceivedEvent(GameEvent e)
        {
        }

        public void OnStateChanged(ModuleState state)
        {
        }

        private void Awake()
        {
            if (s && t)
            {
                startPos = s.position;
                targetPos = t.position;
                FireMortar();
            }
        }

        [ContextMenu("Fire")]
        public void FireMortar()
        {
            for (var i = 0; i < NumberOfMortarRounds; i++)
            {
                var instance = Instantiate(MortarRound);

                var MiddlePos = startPos + (targetPos - startPos) / 2 +
                                Vector3.up * (ArkHight + Random.Range(0, Margin)) +
                                Vector3.forward * Random.Range(0, Margin);
                instance.transform.LookAt(targetPos + Vector3.up * 5);
                var instance_lifetime = LifeTime - i / 20;
                StartCoroutine(FlyMortar(instance, startPos, MiddlePos, targetPos, instance_lifetime));
                StartCoroutine(WaitToDisableGO(instance.transform.GetChild(0).gameObject, instance_lifetime));
                StartCoroutine(WaitToInstantiateThenDestroy(Explosion, targetPos, instance_lifetime - 0.1F, 1F));

                Destroy(instance, instance_lifetime + 1F);
            }

            var smokeinstance = Instantiate(SmokeAndShockWave, transform.position, transform.rotation);
            Destroy(smokeinstance, 6F);
        }
    }
}