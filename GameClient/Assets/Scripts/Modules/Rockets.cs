﻿using Model;
using UnityEngine;

namespace Modules
{
    public class Rockets : MoveProjectile, IModule
    {
        public float ArkHight = 5.0f, Margin = 1f, LifeTime = 2f;
        public int NumberOfRockets;
        public GameObject Rocket, SmokeRing, Explosion;
        public Transform s, t;
        public Vector3 startPos, targetPos;

        public void OnMounted(Slot slot)
        {
        }

        public void OnUnmounted()
        {
        }

        public void OnFiredEvent(GameEvent e)
        {
            if (e.Name == GameEvent.Fire)
            {
                startPos = transform.position;
                targetPos = e.TargetSlot.transform.position;
                FireRockets();
            }
        }

        public void OnReceivedEvent(GameEvent e)
        {
        }

        public void OnStateChanged(ModuleState state)
        {
        }

        private void Awake()
        {
            if (s && t)
            {
                startPos = s.position;
                targetPos = t.position;
                FireRockets();
            }
        }

        public void FireRockets()
        {
            for (var i = 0; i < NumberOfRockets; i++)
            {
                var instance = Instantiate(Rocket);

                var MiddlePos = startPos + (targetPos - startPos) / 2 +
                                Vector3.up * (ArkHight + Random.Range(0, Margin)) +
                                Vector3.forward * Random.Range(0, Margin);
                instance.transform.LookAt(targetPos + Vector3.up * ArkHight * 2);
                var instance_lifetime = LifeTime - i / 20;
                StartCoroutine(FlyRocket(instance, startPos, MiddlePos, targetPos, instance_lifetime));
                StartCoroutine(WaitToDisableGO(instance.transform.GetChild(0).gameObject, instance_lifetime));


                Destroy(instance, instance_lifetime + 2F);
            }

            StartCoroutine(WaitToInstantiateThenDestroy(Explosion, targetPos, LifeTime - 0.2F, 1F));

            var smokeinstance = Instantiate(SmokeRing, transform.position, transform.rotation);
            Destroy(smokeinstance, 6F);
        }
    }
}