﻿using Model;
using UnityEngine;

namespace Modules
{
    public class ForceFieldBlock : MonoBehaviour, IModule
    {
        private Material _mat;
        public GameObject ShieldBlockEffect;

        public void OnMounted(Slot slot)
        {
        }

        public void OnUnmounted()
        {
        }

        public void OnFiredEvent(GameEvent e)
        {
        }

        public void OnReceivedEvent(GameEvent e)
        {
        }

        public void OnStateChanged(ModuleState state)
        {
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Projectile"))
            {
                var go = Instantiate(ShieldBlockEffect, transform);
                _mat = go.GetComponent<ParticleSystemRenderer>().material;
                _mat.SetVector("_SphereCenter", collision.contacts[0].point);
                Destroy(go, 1.1F);
            }
        }
    }
}