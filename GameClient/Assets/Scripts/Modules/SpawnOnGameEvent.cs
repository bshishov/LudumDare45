using System;
using System.Collections;
using Model;
using UnityEngine;

namespace Modules
{
    public class SpawnOnGameEvent : MonoBehaviour, IModule
    {
        public enum ModuleEvent
        {
            Fired,
            Received
        }

        public enum Target
        {
            SourceSlot,
            TargetSlot,
            SourceShip,
            TargetShip
        }

        public enum ValueComparison
        {
            DontCompare,
            GreaterThan,
            GreaterOrEqual,
            Equal,
            LessThan,
            LessOrEqual
        }

        [Header("Value test")] public ValueComparison Comparison = ValueComparison.DontCompare;

        public float Delay;
        public string Event = "fire";
        public ModuleEvent EventType = ModuleEvent.Fired;
        public Vector3 Offset = Vector3.zero;
        public GameObject Prefab;
        public Target SpawnAt = Target.TargetSlot;
        public float Value;

        public void OnMounted(Slot slot)
        {
        }

        public void OnUnmounted()
        {
        }

        public void OnFiredEvent(GameEvent e)
        {
            if (EventType == ModuleEvent.Fired)
                if (e.Name == Event && TestValue(e.Value))
                    StartCoroutine(Spawn(e));
        }

        public void OnReceivedEvent(GameEvent e)
        {
            if (EventType == ModuleEvent.Received)
                if (e.Name == Event && TestValue(e.Value))
                    StartCoroutine(Spawn(e));
        }

        public void OnStateChanged(ModuleState state)
        {
        }

        private bool TestValue(float eventValue)
        {
            switch (Comparison)
            {
                case ValueComparison.GreaterThan:
                    return eventValue > Value;
                case ValueComparison.GreaterOrEqual:
                    return eventValue >= Value;
                case ValueComparison.LessThan:
                    return eventValue < Value;
                case ValueComparison.LessOrEqual:
                    return eventValue <= Value;
                case ValueComparison.Equal:
                    return Math.Abs(eventValue - Value) < 1e-4f;
                case ValueComparison.DontCompare:
                default:
                    return true;
            }
        }

        private IEnumerator Spawn(GameEvent e)
        {
            yield return new WaitForSeconds(Delay);
            var position = ResolvePosition(e);
            if (position.HasValue)
                Instantiate(Prefab, position.Value + Offset, Quaternion.identity);
        }

        private Vector3? ResolvePosition(GameEvent e)
        {
            if (SpawnAt == Target.SourceSlot && e.SourceSlot != null)
                return e.SourceSlot.transform.position;

            if (SpawnAt == Target.TargetSlot && e.TargetSlot != null)
                return e.TargetSlot.transform.position;

            if (SpawnAt == Target.TargetShip && e.TargetShip != null)
                return e.TargetShip.transform.position;

            if (SpawnAt == Target.SourceShip && e.SourceShip != null)
                return e.SourceShip.transform.position;

            return null;
        }
    }
}