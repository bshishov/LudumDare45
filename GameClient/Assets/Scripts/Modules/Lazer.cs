﻿using System.Collections;
using Model;
using UnityEngine;

namespace Modules
{
    public class Lazer : MoveProjectile, IModule
    {
        public LineRenderer lineRenderer;

        public Vector3 SourceAnchor;

        [Header("Effects")] public GameObject StartLazerParticles, Explosion;

        public Vector3 TargetAnchor;

        public void OnMounted(Slot slot)
        {
        }

        public void OnUnmounted()
        {
        }

        public void OnFiredEvent(GameEvent e)
        {
            if (e.TargetSlot != null)
                ShootLazer(transform.TransformPoint(SourceAnchor),
                    e.TargetSlot.transform.TransformPoint(TargetAnchor));
        }

        public void OnReceivedEvent(GameEvent e)
        {
        }

        public void OnStateChanged(ModuleState state)
        {
        }

        public void ShootLazer(Vector3 startPos, Vector3 targetPos)
        {
            lineRenderer.SetPosition(0, startPos);
            lineRenderer.SetPosition(1, targetPos);
            lineRenderer.enabled = false;
            StartCoroutine(WaitToInstantiateThenDestroy(StartLazerParticles, startPos, 0F, 2F));
            StartCoroutine(EnableComponentAfterDelayForTime(lineRenderer, 1.6F, 0.4F));
            StartCoroutine(WaitToInstantiateThenDestroy(Explosion, targetPos, 2F, 2F));
        }

        public IEnumerator EnableComponentAfterDelayForTime(LineRenderer line, float delay, float lifeTime)
        {
            yield return new WaitForSeconds(delay);
            line.enabled = true;
            yield return new WaitForSeconds(lifeTime);
            lineRenderer.enabled = false;
            yield return null;
        }

        private void OnDrawGizmosSelected()
        {
            // Local anchor
            Gizmos.DrawSphere(transform.TransformPoint(SourceAnchor), 0.1f);
        }
    }
}