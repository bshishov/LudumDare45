﻿using System.Collections;
using UnityEngine;

namespace Modules
{
    public class MoveProjectile : MonoBehaviour
    {
        public IEnumerator FlyRocket(GameObject instance, Vector3 startPos, Vector3 middlePos, Vector3 targetPos,
            float lifeTime)
        {
            var count = 0.0f;
            while (count < lifeTime)
            {
                if (count < lifeTime / 4)
                {
                    var targetRotation = Quaternion.LookRotation(targetPos - instance.transform.position);
                    instance.transform.rotation =
                        Quaternion.Slerp(instance.transform.rotation, targetRotation, 4 * Time.deltaTime);
                    count += Time.fixedDeltaTime / (lifeTime * 1.5F);
                }
                else
                {
                    count += Time.fixedDeltaTime / (lifeTime / 2F);
                }


                var m1 = Vector3.Lerp(startPos, middlePos, count);
                var m2 = Vector3.Lerp(middlePos, targetPos, count);
                instance.transform.position = Vector3.Lerp(m1, m2, count);


                yield return new WaitForFixedUpdate();
            }
        }

        public IEnumerator FlyMortar(GameObject instance, Vector3 startPos, Vector3 middlePos, Vector3 targetPos,
            float lifeTime)
        {
            var count = 0.0f;
            while (count < lifeTime && instance != null)
            {
                if (count < lifeTime / 2)
                {
                    var targetRotation = Quaternion.LookRotation(targetPos - instance.transform.position);
                    instance.transform.rotation =
                        Quaternion.Slerp(instance.transform.rotation, targetRotation, 1 * Time.deltaTime);
                }

                count += Time.deltaTime / lifeTime;

                var m1 = Vector3.Lerp(startPos, middlePos, count);
                var m2 = Vector3.Lerp(middlePos, targetPos, count);

                instance.transform.position = Vector3.Lerp(m1, m2, count);

                yield return new WaitForFixedUpdate();
            }
        }

        protected IEnumerator WaitToDisableGO(GameObject instance, float time)
        {
            yield return new WaitForSeconds(time);
            instance.SetActive(false);
            yield return null;
        }

        protected IEnumerator WaitToInstantiateThenDestroy(GameObject go, Vector3 targetPos, float time,
            float destroyDelay)
        {
            yield return new WaitForSeconds(time);
            var Instance = Instantiate(go, targetPos, Quaternion.identity);
            yield return new WaitForSeconds(destroyDelay);
            Destroy(Instance);
            yield return null;
        }
    }
}