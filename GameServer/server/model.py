import os
import time
from typing import List, Optional
import uuid
import logging
import random
from enum import IntEnum

from core.validation import *
from server.core.model import *
from server.core.registry import Registry
from server.core.lazy import lazy_property
from server.core.adapters import PickleDbAdapter
from server.meta import META, Meta, RoundMeta, SlotMeta, ModuleMeta, ShipMeta, TraitMeta
from server.glicko2 import *


__all__ = [
    'PLAYER_DB_PATH',
    'PROFILE_DB',
    'GLICKO2_SYSTEM',
    'C2S_MESSAGES',
    'S2C_MESSAGES',
    'Outcome',
    'QueueType',
    'Profile',
    'ModuleState',
    'SlotState',
    'ShipState',
    'PlayerState',
    'EventTarget',
    'Event',
    'Payload'
]


PLAYER_DB_PATH = os.environ.get('PLAYER_DB_PATH', 'players.db')
PROFILE_DB = PickleDbAdapter(PLAYER_DB_PATH)
GLICKO2_SYSTEM = Glicko2()

C2S_MESSAGES = Registry()
S2C_MESSAGES = Registry()


class Outcome(IntEnum):
    PLAYER1_WON = 1
    PLAYER2_WON = 2
    DRAW = 0


class QueueType:
    RANKED_MATCH = 'ranked_match'
    BOT_MATCH = 'bot_match'

    @classmethod
    def available(cls) -> List[str]:
        res = []
        for attr_name, val in cls.__dict__.items():
            if not attr_name.startswith('_'):
                res.append(val)
        return res


class Profile(JsonSerializableModel):
    MAX_NAME_LEN = 20

    name: str = Field('name')
    rating: float = ProxyField('_rating', 'rating', allow_empty=True)
    joined: float = Field('joined')
    last_login: float = Field('last_login')
    games_won: int = Field('games_won', default=0)
    games_lost: int = Field('games_lost', default=0)
    games_tied: int = Field('games_tied', default=0)
    games_played: int = Field('games_played', default=0)
    games_left: int = Field('games_left', default=0)
    games_streak: int = Field('games_streak', default=0)

    # Not a field so it is not serialized directly
    auth_token = None
    glicko2_rating: Rating = None

    @property
    def _rating(self):
        return self.glicko2_rating.mu

    def __str__(self):
        return f'Player: {self.name}'

    @classmethod
    def create_new(cls, name: str, auth_token: Optional[str]) -> Optional['Profile']:
        p = Profile()
        p.glicko2_rating = GLICKO2_SYSTEM.create_rating()

        if not name:
            return None

        name = name.strip()

        if len(name) > Profile.MAX_NAME_LEN:
            return None

        logging.info(f'New player: {name}')
        p.name = name
        p.joined = time.time()
        p.last_login = time.time()

        if not auth_token:
            p.auth_token = str(uuid.uuid4())[:8]
        else:
            p.auth_token = auth_token

        p.save()
        return p

    @classmethod
    def load(cls, auth_token: str) -> Optional['Profile']:
        try:
            data: dict = PROFILE_DB.get(auth_token)
            if not data:
                return None

            rating_mu = data.pop('rating_mu', GLICKO2_SYSTEM.mu)
            rating_phi = data.pop('rating_phi', GLICKO2_SYSTEM.phi)
            rating_sigma = data.pop('rating_sigma', GLICKO2_SYSTEM.epsilon)

            profile = Profile()
            profile.auth_token = auth_token
            profile.from_dict(data)
            profile.glicko2_rating = Rating(rating_mu, rating_phi, rating_sigma)
            logging.debug(f'Loaded player {profile.name} from database')
            return profile
        except KeyError:
            return None

    def save(self):
        data = {
            **self.to_dict(),
            'rating_mu': self.glicko2_rating.mu,
            'rating_phi': self.glicko2_rating.phi,
            'rating_sigma': self.glicko2_rating.sigma,
        }
        PROFILE_DB.set(self.auth_token, data)


class ModuleStats(JsonSerializableModel):
    damage_done: float = Field('damage_done', default=0)
    healing_done: float = Field('healing_done', default=0)
    total_stun: float = Field('total_stun', default=0)
    init_time: float = Field('init_time', default=-1)
    death_time: float = Field('death_time', default=-1)


class ModuleState(JsonSerializableModel):
    id: str = Field('id')
    hp: float = Field('hp')
    damage: float = Field('damage')
    fire_delay: float = Field('fire_delay')
    priority: int = Field('priority')
    hit_delay: float = Field('hit_delay')
    stun_time: float = Field('stun_time', default=0)
    stats: ModuleStats = ObjectField('stats', ModuleStats, default=ModuleStats)

    def has_trait(self, trait: str) -> bool:
        return trait in self.meta.traits

    def get_trait(self, mechanic_id: str) -> Optional[TraitMeta]:
        for trait in self.traits:
            if trait.mechanic == mechanic_id:
                return trait
        return None

    @lazy_property
    def traits(self) -> List[TraitMeta]:
        return [META.get_trait(trait_id=t) for t in self.meta.traits]

    @property
    def is_alive(self):
        return self.hp > 0

    @lazy_property
    def meta(self) -> ModuleMeta:
        return META.get_module(self.id)

    @classmethod
    def from_meta(cls, module_id, slot: 'SlotState') -> Optional['ModuleState']:
        module_meta = META.get_module(module_id)
        if not module_meta:
            logging.warning(f'No such module {module_id}')
            return None
        module_state = ModuleState(id=module_meta.id)
        module_state.hp = module_meta.start_hp
        module_state.damage = module_meta.base_damage
        module_state.priority = module_meta.base_priority + slot.priority
        module_state.fire_delay = module_meta.roll_fire_delay()  # Initial fire delay
        module_state.hit_delay = module_meta.hit_delay
        module_state.stun_time = 0
        module_state.stats = ModuleStats()
        return module_state


class SlotState(JsonSerializableModel):
    id: str = Field('id')
    line: int = Field('line')
    priority: int = Field('priority')
    module: Optional[ModuleState] = ObjectField('module', ModuleState, allow_none=True,
                                                default=None)

    @property
    def has_module(self):
        return self.module is not None

    @property
    def has_alive_module(self):
        return self.module is not None and self.module.is_alive

    def get_meta(self, ship_id) -> SlotMeta:
        # TODO: Remove shipId
        return META.get_ship(ship_id).get_slot(self.id)


class ShipState(JsonSerializableModel):
    id: str = Field('id')
    slots: List[SlotState] = ListCollectionField('slots', SlotState)

    def get_slot(self, slot_id: str) -> Optional[SlotState]:
        for slot in self.slots:
            if slot.id == slot_id:
                return slot
        return None

    def clear_slots(self):
        for slot in self.slots:
            slot.module = None

    @lazy_property
    def meta(self) -> ShipMeta:
        return META.get_ship(self.id)


class PlayerState(JsonSerializableModel):
    ship: ShipState = ObjectField('ship', ShipState, allow_none=True)
    index: int = Field('index')
    money: int = Field('money', default=0, validator=is_positive_integer)


class EventTarget(JsonSerializableModel):
    player_index: int = Field('player_index')
    slot_id: str = Field('slot_id')


class Event(JsonSerializableModel):
    name: str = Field('name')
    value: int = Field('value')
    source: EventTarget = ObjectField('source', EventTarget)
    target: EventTarget = ObjectField('target', EventTarget)


class Payload(JsonSerializableModel):
    MESSAGE_TYPE = None

    def __str__(self):
        return f'{self.MESSAGE_TYPE} {self.to_dict()}'


@S2C_MESSAGES.register('S2C_HELLO')
class S2CHello(Payload):
    profile: Profile = ObjectField('profile', Profile)
    auth_token: str = Field('auth_token')
    meta: Meta = ObjectField('meta', Meta)


@S2C_MESSAGES.register('S2C_GAME_CREATED')
class S2CGameCreated(Payload):
    id: str = Field('id')
    player_index: int = Field('player_index')
    opponent: Profile = ObjectField('opponent', Profile)


@S2C_MESSAGES.register('S2C_GAME_ENDED')
class S2CGameEnded(Payload):
    interrupted: bool = Field('interrupted')
    interrupted_reason: str = Field('interrupted_reason', allow_none=True)
    winner: int = Field('winner')
    player1_profile: Profile = ObjectField('player1_profile', Profile)
    player2_profile: Profile = ObjectField('player2_profile', Profile)


@S2C_MESSAGES.register('S2C_GAME_STARTED')
class S2CGameStarted(Payload):
    pass


@S2C_MESSAGES.register('S2C_GAME_EVENT')
class S2CGameEvent(Payload):
    time: int = Field('time')
    events: List[Event] = ListCollectionField('events', Event)
    player1_ship: ShipState = ObjectField('player1_ship', ShipState)
    player2_ship: ShipState = ObjectField('player2_ship', ShipState)


@S2C_MESSAGES.register('S2C_STARTED_MATCHMAKING')
class S2CStartedMatchMaking(Payload):
    active_players: int = Field('active_players')
    queue_type: str = Field('queue_type',
                            default=QueueType.RANKED_MATCH,
                            validator=value_in(QueueType.available()))


@S2C_MESSAGES.register('S2C_STOPPED_MATCHMAKING')
class S2CStoppedMatchMaking(Payload):
    reason: str = Field('reason')


@S2C_MESSAGES.register('S2C_MATCHMAKING_UPDATE')
class S2CMatchmakingUpdate(Payload):
    players_total: int = Field('players_total')
    players_in_battle: int = Field('players_in_battle')
    players_in_matchmaking: int = Field('players_in_matchmaking')


@S2C_MESSAGES.register('S2C_ROUND_STARTED')
class S2CRoundStarted(Payload):
    round_number: int = Field('round_number')
    round: RoundMeta = ObjectField('round', model=RoundMeta)


@S2C_MESSAGES.register('S2C_ROUND_ENDED')
class S2CRoundEnded(Payload):
    round_number: int = Field('round_number')
    winner: int = Field('winner')
    history: List[int] = Field('history')


@S2C_MESSAGES.register('S2C_ROUND_BUILD_PHASE')
class S2CRoundBuildPhase(Payload):
    ship: ShipState = ObjectField('ship', model=ShipState)


@S2C_MESSAGES.register('S2C_ROUND_BATTLE_PHASE')
class S2CRoundBattlePhase(Payload):
    player1_ship: ShipState = ObjectField('player1_ship', ShipState)
    player2_ship: ShipState = ObjectField('player2_ship', ShipState)


@S2C_MESSAGES.register('S2C_MONEY_UPDATED')
class S2CMoneyUpdated(Payload):
    delta: int = Field('delta')
    new_amount: int = Field('new_amount')


@S2C_MESSAGES.register('S2C_SHIP_UPDATED')
class S2CShipUpdated(Payload):
    ship: ShipState = ObjectField('ship', model=ShipState)


@S2C_MESSAGES.register('S2C_PLAYER_READY')
class S2CPlayerReady(Payload):
    player_index: int = Field('player_index')


@C2S_MESSAGES.register('C2S_HELLO')
class C2SHello(Payload):
    token: str = Field('token', validator=OfType(str, allow_none=True), allow_none=True)
    name: str = Field('name', validator=is_not_empty_string_of_max_len(Profile.MAX_NAME_LEN))
    version: str = Field('version', validator=is_not_empty_string)


@C2S_MESSAGES.register('C2S_START_MATCHMAKING')
class C2SStartMatchmaking(Payload):
    queue_type: str = Field('queue_type',
                            default=QueueType.RANKED_MATCH,
                            validator=value_in(QueueType.available()))


@C2S_MESSAGES.register('C2S_STOP_MATCHMAKING')
class C2SStopMatchmaking(Payload):
    pass


@C2S_MESSAGES.register('C2S_MODULE_BUY')
class C2SModuleBuy(Payload):
    module: str = Field('module', validator=is_not_empty_string)
    slot: str = Field('slot', is_not_empty_string)


@C2S_MESSAGES.register('C2S_MODULE_SELL')
class C2SModuleSell(Payload):
    slot: str = Field('slot', is_not_empty_string)


@C2S_MESSAGES.register('C2S_MODULE_MOVE')
class C2SModuleMove(Payload):
    slot_from: str = Field('slot_from', is_not_empty_string)
    slot_to: str = Field('slot_to', is_not_empty_string)


@C2S_MESSAGES.register('C2S_READY')
class C2SReady(Payload):
    pass


# Patch message classes and assign correct MESSAGE_TYPE to each
for msg_type, msg_class in (*C2S_MESSAGES.items(), *S2C_MESSAGES.items()):
    msg_class.MESSAGE_TYPE = msg_type
    __all__.append(msg_class.__name__)
