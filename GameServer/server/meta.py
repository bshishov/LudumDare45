import os
from typing import List, Dict, Optional, Any
import random
import logging


from core.validation import *
from server.core.model import *

__all__ = [
    'Meta',
    'SlotMeta',
    'ShipMeta',
    'RoundMeta',
    'Mechanics',
    'ModuleMeta',
    'ModuleTargetPriorityMeta',
    'TraitMeta',
    'META'
]


class Mechanics:
    ATTRACTION = 'attraction'   # on init + on death
    DAMAGE_AURA = 'damage_aura'  # on init + on death
    HEALTH_AURA = 'health_aura'  # on init + on death
    REGEN_AURA = 'regen_aura'  # on fire
    FIRE_DELAY_AURA = 'fire_delay_aura'
    REGEN_SELF = 'regen_self'  # on fire
    SPLASH_DAMAGE = 'splash_damage'  # on fire
    GOLD_GAIN_PER_FIRE = 'gold_gain_per_fire'  # on fire
    TARGET_LEAST_HEALTH = 'target_least_health'  # special targeting
    TARGET_SELF = 'target_self'  # special targeting
    NO_FIRE = 'no_fire'  # special update  / on fire?
    HURT_SELF = 'hurt_self'  # on fire
    PRIORITIZE_LINE_BY_LINE = 'prioritize_line_by_line'  # special targeting
    PRIORITIZE_SAME_ROW = 'prioritize_same_row'  # special targeting
    ATTACK_ONLY = 'attack_only'  # special targeting
    SUMMON_NEARBY = 'summon_nearby'  # on fire
    TEMPORARY = 'temporary'
    STUN_TARGET_ON_HIT = 'stun_target_on_hit'

    # deprecated
    GOLD_GAIN_AFTER_ROUND = 'gold_gain_after_round'
    IGNORES_PRIORITY_OF_EVERYTHING_BUT_SHIELD = 'ignores_priority_of_everything_but_shield'
    SAME_LINE = 'same_line'
    IGNORE_PRIORITIES = 'ignore_priorities'

    @classmethod
    def available(cls) -> List[str]:
        res = []
        for attr_name, val in cls.__dict__.items():
            if not attr_name.startswith('_'):
                res.append(val)
        return res


class SlotMeta(JsonSerializableModel):
    id: str = Field('id', validator=is_not_empty_string)
    line: int = Field('line', validator=is_positive_integer)
    priority: int = Field('priority', validator=is_positive_integer)
    position_x: int = Field('position_x', validator=is_integer)
    position_y: int = Field('position_y', validator=is_integer)


class ShipMeta(JsonSerializableModel):
    id: str = Field('id', validator=is_not_empty_string)
    name: str = Field('name', validator=is_not_empty_string)
    description: str = Field('description', validator=is_not_empty_string)
    slots: List[SlotMeta] = ListCollectionField('slots', item_model=SlotMeta,
                                                validator=is_not_empty_list_of(SlotMeta))

    def get_slot(self, slot_id: str) -> Optional[SlotMeta]:
        for slot in self.slots:
            if slot.id == slot_id:
                return slot
        return None


class ModuleTargetPriorityMeta(JsonSerializableModel):
    module_id: str = Field('module_id', validator=is_not_empty_string)
    additional_priority: int = Field('additional_priority', validator=is_numeric)


class ModuleMeta(JsonSerializableModel):
    id: str = Field('id', validator=is_not_empty_string)
    name: str = Field('name', validator=is_not_empty_string)
    description: str = Field('description', validator=is_not_empty_string)
    start_hp: int = Field('start_hp', validator=is_positive_numeric)
    base_hp: int = Field('base_hp', validator=is_positive_numeric)
    base_damage: int = Field('base_damage', validator=is_positive_numeric)
    fire_delay: int = Field('fire_delay', validator=is_positive_numeric)
    fire_additional_random_delay: int = Field('fire_additional_random_delay',
                                              validator=is_positive_numeric)
    base_priority: int = Field('base_priority', validator=is_positive_numeric)
    hit_delay: float = Field('hit_delay', validator=is_positive_numeric)
    target_priorities: List[ModuleTargetPriorityMeta] = \
        ListCollectionField('target_priorities', item_model=ModuleTargetPriorityMeta,
                            validator=is_list_of(ModuleTargetPriorityMeta))
    traits: List[str] = Field('traits', validator=is_list_of(str))

    # Shop
    can_be_bought: bool = Field('can_be_bought', validator=is_bool)
    price: int = Field('price', validator=is_positive_integer)

    # Upgrades
    upgrades_to: str = Field('upgrades_to', allow_none=True,
                             validator=is_of_type(str, allow_none=True))
    upgrade_tier: int = Field('upgrade_tier', validator=is_positive_integer)

    def roll_fire_delay(self) -> float:
        return self.fire_delay + random.random() * self.fire_additional_random_delay


class RoundMeta(JsonSerializableModel):
    build_phase_time: int = Field('build_phase_time', validator=is_positive_numeric)
    pre_battle_time: int = Field('pre_battle_time', validator=is_positive_numeric)
    post_battle_time: int = Field('post_battle_time', validator=is_positive_numeric)
    post_round_time: int = Field('post_round_time', validator=is_positive_numeric)
    money_gain: int = Field('money_gain', validator=is_positive_integer)
    max_time: int = Field('max_time', validator=is_positive_numeric)


class TraitMeta(JsonSerializableModel):
    id: str = Field('id', validator=is_not_empty_string)
    name: str = Field('name', validator=is_not_empty_string)
    description: str = Field('description', validator=is_not_empty_string)
    mechanic: str = Field('mechanic',
                          validator=ChainValidator(is_not_empty_string,
                                                   value_in(Mechanics.available()))
                          )
    params: Dict[str, Any] = Field('params', validator=is_of_type(dict))


class Config(JsonSerializableModel):
    matchmaking_update_delay: float = Field('matchmaking_update_delay',
                                            default=5.0,
                                            validator=is_positive_numeric)
    game_update_delay: float = Field('game_update_delay', default=2.0,
                                     validator=is_positive_numeric)


class Meta(JsonSerializableModel):
    version: str = Field('version', validator=is_not_empty_string)
    ships: List[ShipMeta] = ListCollectionField('ships', item_model=ShipMeta,
                                                validator=is_not_empty_list_of(ShipMeta))
    modules: List[ModuleMeta] = ListCollectionField('modules', item_model=ModuleMeta,
                                                    validator=is_not_empty_list_of(ModuleMeta))
    rounds: List[RoundMeta] = ListCollectionField('rounds',
                                                  item_model=RoundMeta,
                                                  validator=is_not_empty_list_of(RoundMeta))
    should_win: int = Field('should_win', validator=is_positive_integer)
    start_money: int = Field('start_money', validator=is_positive_integer)
    pregame_time: int = Field('pregame_time', validator=is_positive_numeric)
    traits: List[TraitMeta] = ListCollectionField('traits', item_model=TraitMeta,
                                                  validator=is_list_of(TraitMeta))
    ship: str = Field('ship', is_not_empty_string)
    state_after_round: str = Field('state_after_round',
                                   validator=value_in('keep', 'reset', 'clear'))
    config: Config = ObjectField('config',
                                 model=Config,
                                 validator=is_of_type(Config),
                                 default=Config())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._ships_dict = {}
        self._modules_dict = {}
        self._traits_dict = {}

    def get_ship(self, ship_id: str) -> Optional[ShipMeta]:
        return self._ships_dict.get(ship_id)

    def get_module(self, module_id: str) -> Optional[ModuleMeta]:
        return self._modules_dict.get(module_id)

    def has_module(self, module_id: str) -> bool:
        return module_id in self._modules_dict

    def get_trait(self, trait_id: str) -> TraitMeta:
        return self._traits_dict[trait_id]

    def deserialize(self, data):
        super().deserialize(data)
        self._ships_dict = {ship.id: ship for ship in self.ships}
        self._modules_dict = {module.id: module for module in self.modules}
        self._traits_dict = {trait.id: trait for trait in self.traits}


META_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', 'meta.json')

META: Meta = Meta()
with open(META_PATH, 'r') as f:
    META.from_json_file(f)
    META.validate(raize=True)
    logging.info(f'Loaded META from {META_PATH}.')
