import asyncio
import logging
import logging.config
import websockets
import argparse
import json
import os

from prometheus_client.exposition import start_http_server

from server.app import (
    on_connected,
    on_disconnected,
    on_message,
    matchmaking_update,
    update_games
)
from server.monitoring import (
    INVALID_MESSAGE_ERRORS,
    WEBSOCKET_MESSAGES_RECEIVED,
    WEBSOCKET_MESSAGES_SENT
)
from server.player import Player


async def consumer(player: Player, message: str):
    try:
        message = json.loads(message)
    except json.JSONDecodeError:
        logging.warning(f'Unable to parse message (expected json): {message}')
        INVALID_MESSAGE_ERRORS.inc()
        return
    try:
        await on_message(player, message)
    except Exception as err:
        logging.exception(err, exc_info=True)


async def producer(player: Player) -> str:
    return await player.message_queue.get()


async def consumer_handler(player: Player, path):
    async for message in player.socket:
        WEBSOCKET_MESSAGES_RECEIVED.inc()
        await consumer(player, message)


async def producer_handler(player: Player, path):
    while True:
        message = await producer(player)
        WEBSOCKET_MESSAGES_SENT.inc()
        await player.socket.send(message)


async def handler(websocket, path):
    player = Player(websocket)
    on_connected(player)
    try:
        consumer_task = asyncio.ensure_future(consumer_handler(player, path))
        producer_task = asyncio.ensure_future(producer_handler(player, path))
        done, pending = await asyncio.wait([consumer_task, producer_task],
                                           return_when=asyncio.FIRST_COMPLETED)
        for task in pending:
            task.cancel()
    except websockets.ConnectionClosedError:
        pass
    except Exception as err:
        logging.exception(err, exc_info=True)
    on_disconnected(player)


def main(args):
    logging_config = {
        'version': 1,
        'formatters': {
            'detailed': {
                'class': 'logging.Formatter',
                'format': '[%(levelname)s] %(asctime)-15s %(module)s %(funcName)s: %(message)s'
            }
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'level': 'DEBUG',
                'formatter': 'detailed'
            },
        },
        'loggers': {
            'protocol': {},
        },
        'root': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'formatter': 'detailed'
        },
    }

    logging.config.dictConfig(logging_config)
    logging.info(f'Running server on {args.host}:{args.port}')
    start_server = websockets.serve(handler, args.host, args.port)
    logging.info(f'Start server')

    prometheus_port = os.environ.get('PROMETHEUS_PORT', 8000)
    start_http_server(prometheus_port)
    logging.info(f'Started prometheus metrics on port {prometheus_port}')

    asyncio.run_coroutine_threadsafe(matchmaking_update(), asyncio.get_event_loop())
    asyncio.run_coroutine_threadsafe(update_games(), asyncio.get_event_loop())
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', type=int, default=6789, help='Port')
    parser.add_argument('--host', type=str, default='localhost', help='WebSockets host')
    main(parser.parse_args())
