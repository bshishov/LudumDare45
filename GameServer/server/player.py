from typing import Optional
import json
import asyncio
import logging

from server.model import Profile, Payload


class Player(object):
    def __init__(self, socket, is_bot=False):
        self.is_bot = is_bot
        self.socket = socket
        self.message_queue = asyncio.Queue()
        self.profile: Optional[Profile] = None
        self.game = None
        self.matchmaking_queue_type = None

    @property
    def is_connected(self):
        return self.socket.open

    def send(self, message: Payload):
        try:
            serialized = json.dumps({
                'type': message.MESSAGE_TYPE,
                'payload': message.to_json()
            })
            self.message_queue.put_nowait(serialized)
        except Exception as err:
            logging.exception(err, exc_info=True)

    @property
    def logged_in(self):
        return self.profile is not None

    def auth(self, name: str, token: Optional[str] = None):
        profile = Profile.load(token)
        if not profile:
            profile = Profile.create_new(name, token)

        if profile is None:
            logging.debug(f'Failed to auth name:{name}, token:{token}')
            self.profile = None
            return

        if name:
            name = name.strip()
            if name and name != profile.name:
                logging.info(f'Updating profile name of {profile.name} to {name}')
                profile.name = name

        profile.save()
        self.profile = profile
        logging.info(f'Player {profile.name} authorized')

    def logout(self):
        self.profile = None

    async def disconnect(self):
        await self.socket.close()

    @property
    def name(self) -> Optional[str]:
        if self.profile:
            return self.profile.name

    def __str__(self):
        return f'<Player(name={self.name}, address={self.socket.remote_address})>'

    def __repr__(self):
        return f'<Player(name={self.name}, address={self.socket.remote_address})>'



