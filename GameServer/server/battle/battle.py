from typing import Optional, List, Dict, Tuple, Iterator
import random
import logging
from dataclasses import dataclass
from collections import deque
from datetime import datetime

from model import PlayerState, ShipState, Outcome, SlotState, Event, EventTarget
from server.meta import META, ModuleMeta, Mechanics, ModuleTargetPriorityMeta
from server.core.registry import Registry
from battle.event import BattleEvent, EventName
from battle.mechaincs import MECHANICS, Mechanic
from battle.targeting import *


TIME_DELTA = META.config.game_update_delay
EVENT_HANDLERS = Registry()


@EVENT_HANDLERS.register(EventName.FIRE)
def handle_fire(battle: 'Battle', e: BattleEvent) -> bool:
    # Both targets should be alive, otherwise discard event
    if not e.target_slot.has_alive_module or not e.source_slot.has_alive_module:
        return False

    # Queue delayed hit event
    battle.queue_event(BattleEvent(
        name=EventName.HIT,
        source_slot=e.source_slot,
        target_slot=e.target_slot,
        source_player=e.source_player,
        target_player=e.target_player,
        value=e.value,
        delay=e.source_slot.module.hit_delay
    ))
    return True


@EVENT_HANDLERS.register(EventName.HIT)
def handle_hit(battle: 'Battle', e: BattleEvent) -> bool:
    # If the target slot does not have an alive module - discard event
    if not e.target_slot.has_alive_module:
        return False

    e.target_slot.module.hp = max(0.0, e.target_slot.module.hp - e.value)

    if e.source_slot.module:
        e.source_slot.module.stats.damage_done += e.value
    if not e.target_slot.module.is_alive:
        battle.queue_event(BattleEvent(
            name=EventName.DEATH,
            source_slot=e.source_slot,
            target_slot=e.target_slot,
            source_player=e.source_player,
            target_player=e.target_player
        ))
    return True


@EVENT_HANDLERS.register(EventName.DMG_MUL)
def handle_dmg_mul(battle: 'Battle', e: BattleEvent) -> bool:
    # If the target slot does not have an alive module - discard event
    if not e.target_slot.has_alive_module:
        return False
    e.target_slot.module.damage *= e.value
    return True


@EVENT_HANDLERS.register(EventName.HP_MUL)
def handle_hp_mul(battle: 'Battle', e: BattleEvent) -> bool:
    # If the target slot does not have an alive module - discard event
    if not e.target_slot.has_alive_module:
        return False
    e.target_slot.module.hp *= e.value
    return True


@EVENT_HANDLERS.register(EventName.FIRE_DELAY_MUL)
def handle_fire_delay_mul(battle: 'Battle', e: BattleEvent) -> bool:
    # If the target slot does not have an alive module - discard event
    if not e.target_slot.has_alive_module:
        return False
    e.target_slot.module.fire_delay *= e.value
    return True


@EVENT_HANDLERS.register(EventName.PRIORITY_ADD)
def handle_priority_add(battle: 'Battle', e: BattleEvent) -> bool:
    # If the target slot does not have an alive module - discard event
    if not e.target_slot.has_alive_module:
        return False
    e.target_slot.module.priority += e.value
    return True


@EVENT_HANDLERS.register(EventName.DEATH)
def handle_death(battle: 'Battle', e: BattleEvent) -> bool:
    e.target_slot.module.stats.death_time = datetime.now().timestamp()

    # Clear temporary on death
    if e.target_slot.has_module and e.target_slot.module.has_trait(Mechanics.TEMPORARY):
        e.target_slot.module = None

    return True


@EVENT_HANDLERS.register(EventName.MODULE_INIT)
def handle_init(battle: 'Battle', e: BattleEvent) -> bool:
    e.source_slot.module.stats.init_time = datetime.now().timestamp()
    return True


@EVENT_HANDLERS.register(EventName.HEAL)
def handle_heal(battle: 'Battle', e: BattleEvent) -> bool:
    # Nothing to heal
    if not e.target_slot.has_alive_module:
        return False

    # Invalid heal value, positive only
    if e.value <= 0:
        return False

    missing_hp = max(0.0, e.target_slot.module.meta.base_hp - e.target_slot.module.hp)
    amount = min(e.value, missing_hp)

    # Dont heal past max and for negative amount
    if amount <= 0:
        return False

    # Increase HP
    e.target_slot.module.hp += amount
    e.source_slot.module.stats.healing_done += amount

    return True


@EVENT_HANDLERS.register(EventName.STUN)
def handle_stun(battle: 'Battle', e: BattleEvent) -> bool:
    if not e.target_slot.has_alive_module:
        return False

    if e.value <= 0:
        return False

    e.target_slot.module.stun_time = max(e.value, e.target_slot.module.stun_time)
    e.source_slot.module.stats.total_stun += e.value
    return True


class Battle(object):
    def __init__(self, p1: PlayerState, p2: PlayerState):
        self.p1: PlayerState = p1
        self.p2: PlayerState = p2
        self._events = deque()

    def begin(self):
        self._init_relative(self.p1, self.p2)
        self._init_relative(self.p2, self.p1)

    def update(self) -> Tuple[Optional[Outcome], List[Event]]:
        self._update_relative(self.p1, self.p2)
        self._update_relative(self.p2, self.p1)

        # Update and find events that we can notify about
        send_events = self._process_events()

        # Return update outcome and events batch to send
        outcome = battle_outcome(self.p1.ship, self.p2.ship)
        if outcome is not None:
            def _clear_tmp(ship: ShipState):
                for slot in ship.slots:
                    if slot.has_module and slot.module.has_trait(Mechanics.TEMPORARY):
                        slot.module = None

            _clear_tmp(self.p1.ship)
            _clear_tmp(self.p2.ship)

        return outcome, send_events

    def _update_relative(self, source_player: PlayerState, target_player: PlayerState):
        """ Iterate over slots, update cooldowns and proc fire events """
        for slot in source_player.ship.slots:
            if not slot.has_alive_module:
                continue

            # If stunned - decrease stun time and skip update
            if slot.module.stun_time > 0:
                slot.module.stun_time -= TIME_DELTA
                continue

            # Skip non-firing modules
            if slot.module.has_trait(Mechanics.NO_FIRE):
                continue

            # Check cooldown
            if slot.module.fire_delay > 0:
                slot.module.fire_delay -= TIME_DELTA
                continue

            """ TARGETING """

            # Find all potential opponent targets (no filtering applied)
            targets = get_available_targets(slot, target_player)

            same_row_trait = slot.module.get_trait(Mechanics.PRIORITIZE_SAME_ROW)
            if same_row_trait:
                row = slot.get_meta(source_player.ship.id).position_y
                for t in targets:
                    if t.slot.get_meta(target_player.ship.id).position_y == (5 - row) + 1:
                        t.priority += same_row_trait.params['additional_priority']

            line_by_line_trait = slot.module.get_trait(Mechanics.PRIORITIZE_LINE_BY_LINE)
            if line_by_line_trait:
                additional_priority = line_by_line_trait.params['additional_priority']
                is_reverse = line_by_line_trait.params['reverse']
                for t in targets:
                    line = t.slot.get_meta(target_player.ship.id).position_x
                    if is_reverse:
                        line = (5 - line) + 1
                    t.priority += additional_priority * line

            if slot.module.has_trait(Mechanics.SAME_LINE):
                targets_on_same_line = filter_targets_on_line(targets, slot.line)
                e_target = select_target_with_highest_priority(targets_on_same_line)
            elif slot.module.has_trait(Mechanics.TARGET_SELF):
                e_target = PrioritisedTarget(slot=slot, priority=0, player=source_player)
            elif slot.module.has_trait(Mechanics.IGNORES_PRIORITY_OF_EVERYTHING_BUT_SHIELD):
                shields = filter_targets_of_module_id(targets, 'shield')
                if shields:
                    e_target = select_target_with_highest_priority(shields)
                else:
                    e_target = select_random_target(targets)
            elif slot.module.has_trait(Mechanics.IGNORE_PRIORITIES):
                e_target = select_random_target(targets)
            elif slot.module.has_trait(Mechanics.TARGET_LEAST_HEALTH) and targets:
                e_target = sorted(targets, key=lambda tgt: tgt.slot.module.hp)[0]
            elif slot.module.has_trait(Mechanics.ATTACK_ONLY) and targets:
                modules = slot.module.get_trait(Mechanics.ATTACK_ONLY).params['modules']
                targets = [tgt for tgt in targets if tgt.slot.module.id in modules]
                e_target = select_target_with_highest_priority(targets)
            else:
                e_target = select_target_with_highest_priority(targets)

            if e_target is not None and e_target.slot.has_alive_module:
                self.queue_event(BattleEvent(
                    name=EventName.FIRE,
                    source_player=source_player,
                    source_slot=slot,
                    target_player=e_target.player,
                    target_slot=e_target.slot,
                    value=slot.module.damage))

                # Start cooldown once we fired an event
                slot.module.fire_delay = slot.module.meta.roll_fire_delay()

    def _init_relative(self, source_player: PlayerState, target_player: PlayerState):
        for slot in source_player.ship.slots:
            if not slot.has_alive_module:
                continue
            self.queue_event(BattleEvent(
                name=EventName.MODULE_INIT,
                source_player=source_player,
                target_player=source_player,
                source_slot=slot,
                target_slot=slot
            ))

    def _process_events(self) -> List[Event]:
        events: List[Event] = []
        kept_events = deque()
        while self._events:
            e = self._events.popleft()
            if e.delay > 0:
                e.delay -= TIME_DELTA
                kept_events.append(e)
                continue

            event_handler = EVENT_HANDLERS[e.name]
            should_send = event_handler(self, e)
            if should_send:
                if e.source_slot.has_alive_module:
                    for trait in e.source_slot.module.traits:
                        handler: Mechanic = MECHANICS.get(trait.mechanic)
                        if handler:
                            handler.on_battle_event_fired(self, e, trait.params)
                if e.target_slot.has_alive_module:
                    for trait in e.target_slot.module.traits:
                        handler: Mechanic = MECHANICS.get(trait.mechanic)
                        if handler:
                            handler.on_battle_event_received(self, e, trait.params)

                events.append(e.to_send_event())
        self._events = kept_events
        return events

    def queue_event(self, e: BattleEvent):
        self._events.append(e)


def ship_is_dead(ship: ShipState):
    for slot in ship.slots:
        if not slot.has_module:
            continue
        if slot.module.hp > 0:
            return False
    return True


def battle_outcome(ship1: ShipState, ship2: ShipState) -> Optional[Outcome]:
    player1_dead = ship_is_dead(ship1)
    player2_dead = ship_is_dead(ship2)
    if player1_dead and player2_dead:
        return Outcome.DRAW
    elif player1_dead:
        return Outcome.PLAYER2_WON
    elif player2_dead:
        return Outcome.PLAYER1_WON
    return None
