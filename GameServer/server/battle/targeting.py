from typing import List, Optional, Iterator
from dataclasses import dataclass
import random

import numpy as np


from server.meta import ModuleTargetPriorityMeta
from model import PlayerState, SlotState, ShipState


__all__ = ['PrioritisedTarget',
           'get_available_targets',
           'get_total_target_priority',
           'get_subject_priority',
           'select_random_target',
           'select_target_with_proba',
           'select_target_with_highest_priority',
           'filter_targets_on_line',
           'filter_targets_of_module_id',
           'iter_nearby_slots']


@dataclass
class PrioritisedTarget:
    player: PlayerState
    slot: SlotState
    priority: float = 0


def get_available_targets(subject_slot: SlotState, target_player: PlayerState) -> \
        List[PrioritisedTarget]:
    targets: List[PrioritisedTarget] = []
    for target_slot in target_player.ship.slots:
        if not target_slot.has_alive_module:
            continue
        targets.append(
            PrioritisedTarget(slot=target_slot,
                              priority=get_total_target_priority(subject_slot, target_slot),
                              player=target_player))
    return targets


def get_total_target_priority(subject_slot: SlotState, target_slot: SlotState) -> float:
    subject_priority = get_subject_priority(subject_slot.module.meta.target_priorities,
                                            target_slot.module.id)
    return target_slot.module.priority + subject_priority


def get_subject_priority(
        subject_priorities: List[ModuleTargetPriorityMeta],
        target_id: str) -> int:
    for priority in subject_priorities:
        if priority.module_id == target_id:
            return priority.additional_priority
    return 0


def select_random_target(targets: List[PrioritisedTarget]) -> Optional[PrioritisedTarget]:
    if not targets:
        return None
    return random.choice(targets)


def select_target_with_proba(targets: List[PrioritisedTarget]) -> \
        Optional[PrioritisedTarget]:
    if not targets:
        return None
    p = [t.priority for t in targets]
    return np.random.choice(targets, p=np.exp(p) / np.sum(np.exp(p)))


def select_target_with_highest_priority(targets: List[PrioritisedTarget]) -> \
        Optional[PrioritisedTarget]:
    if not targets:
        return None
    return max(targets, key=lambda t: t.priority)


def filter_targets_of_module_id(targets: List[PrioritisedTarget], name: str) -> \
        List[PrioritisedTarget]:
    return [t for t in targets if t.slot.module.id == name]


def filter_targets_on_line(targets: List[PrioritisedTarget], line: int) -> List[PrioritisedTarget]:
    return [t for t in targets if t.slot.line == line]


def iter_nearby_slots(ship: ShipState,
                      reference: SlotState,
                      aoe: int,
                      only_alive: bool) -> Iterator[SlotState]:
    ship_meta = ship.meta
    ref_meta = ship_meta.get_slot(reference.id)

    for slot_meta in ship_meta.slots:
        if (slot_meta is not ref_meta
                and abs(slot_meta.position_x - ref_meta.position_x) <= aoe
                and abs(slot_meta.position_y - ref_meta.position_y) <= aoe):
            slot = ship.get_slot(slot_meta.id)
            if not only_alive or (only_alive and slot.has_alive_module):
                yield slot
