import random

from core.registry import Registry, SingletonRegistry
from meta import Mechanics
from model import ModuleState

from battle.event import BattleEvent, EventName
from battle.targeting import iter_nearby_slots


__all__ = ['MECHANICS', 'Mechanic']


MECHANICS = SingletonRegistry()
TARGETING_MECHANICS = Registry()


class Mechanic:
    def on_battle_init(self, battle):
        pass

    def on_battle_event_fired(self, battle, e: BattleEvent, params: dict):
        pass

    def on_battle_event_received(self, battle, e: BattleEvent, params: dict):
        pass


@MECHANICS.register(Mechanics.GOLD_GAIN_PER_FIRE)
class GoldGainPerFire(Mechanic):
    def on_battle_event_fired(self, battle, e: BattleEvent, params: dict):
        if e.name == EventName.FIRE:
            e.source_player.money += params['amount']
            # TODO send notification about money change


@MECHANICS.register(Mechanics.REGEN_AURA)
class RegenAura(Mechanic):
    def on_battle_event_fired(self, battle, e: BattleEvent, params: dict):
        if e.name == EventName.FIRE:
            for slot in iter_nearby_slots(e.source_player.ship,
                                          reference=e.source_slot,
                                          aoe=params['nearby'],
                                          only_alive=True):
                missing_hp = max(0.0, slot.module.meta.base_hp - slot.module.hp)
                amount = min(params['amount'], missing_hp)
                if amount > 0:
                    battle.queue_event(BattleEvent(
                        name=EventName.HEAL,
                        source_slot=e.source_slot,
                        target_slot=slot,
                        source_player=e.source_player,
                        target_player=e.target_player,
                        value=amount,
                        delay=0
                    ))


@MECHANICS.register(Mechanics.REGEN_SELF)
class RegenSelf(Mechanic):
    def on_battle_event_fired(self, battle, e: BattleEvent, params: dict):
        if e.name == EventName.FIRE:
            battle.queue_event(BattleEvent(
                name=EventName.HEAL,
                source_slot=e.source_slot,
                target_slot=e.source_slot,
                source_player=e.source_player,
                target_player=e.source_player,
                value=params['amount'],
                delay=0
            ))


@MECHANICS.register(Mechanics.HURT_SELF)
class HurtSelf(Mechanic):
    def on_battle_event_fired(self, battle, e: BattleEvent, params: dict):
        if e.name == EventName.FIRE:
            battle.queue_event(BattleEvent(
                name=EventName.HIT,
                source_slot=e.source_slot,
                target_slot=e.source_slot,
                source_player=e.source_player,
                target_player=e.source_player,
                value=params['amount'],
                delay=0
            ))


@MECHANICS.register(Mechanics.SPLASH_DAMAGE)
class SplashDamage(Mechanic):
    def on_battle_event_fired(self, battle, e: BattleEvent, params: dict):
        if e.name == EventName.FIRE:
            for slot in iter_nearby_slots(e.target_player.ship,
                                          reference=e.target_slot,
                                          aoe=params['nearby'],
                                          only_alive=True):
                battle.queue_event(BattleEvent(
                    name=EventName.HIT,
                    source_slot=e.source_slot,
                    target_slot=slot,
                    source_player=e.source_player,
                    target_player=e.target_player,
                    value=e.value * params['damage_multiplier'],
                    delay=e.source_slot.module.hit_delay
                ))


@MECHANICS.register(Mechanics.DAMAGE_AURA)
class DamageAura(Mechanic):
    def on_battle_event_fired(self, battle, e: BattleEvent, params: dict):
        if e.name == EventName.MODULE_INIT:
            for target_slot in iter_nearby_slots(e.source_player.ship,
                                                 e.source_slot,
                                                 aoe=params['nearby'],
                                                 only_alive=True):
                battle.queue_event(BattleEvent(
                    name=EventName.DMG_MUL,
                    value=params['damage_multiplier'],
                    source_slot=e.source_slot,
                    target_slot=target_slot,
                    source_player=e.source_player,
                    target_player=e.source_player
                ))
        if e.name == EventName.DEATH:
            if params['permanent']:
                # Skip on-death discard events if buff is permanent
                return

            for target_slot in iter_nearby_slots(e.target_player.ship, e.target_slot,
                                                 aoe=params['nearby'],
                                                 only_alive=True):
                battle.queue_event(BattleEvent(
                    name=EventName.DMG_MUL,
                    value=1.0 / params['damage_multiplier'],
                    source_slot=e.target_slot,
                    target_slot=target_slot,
                    source_player=e.target_player,
                    target_player=e.target_player
                ))


@MECHANICS.register(Mechanics.FIRE_DELAY_AURA)
class FireDelayAura(Mechanic):
    def on_battle_event_fired(self, battle, e: BattleEvent, params: dict):
        if e.name == EventName.MODULE_INIT:
            for target_slot in iter_nearby_slots(e.source_player.ship,
                                                 e.source_slot,
                                                 aoe=params['nearby'],
                                                 only_alive=True):
                battle.queue_event(BattleEvent(
                    name=EventName.FIRE_DELAY_MUL,
                    value=params['fire_delay_multiplier'],
                    source_slot=e.source_slot,
                    target_slot=target_slot,
                    source_player=e.source_player,
                    target_player=e.source_player
                ))
        if e.name == EventName.DEATH:
            if params['permanent']:
                # Skip on-death discard events if buff is permanent
                return

            for target_slot in iter_nearby_slots(e.target_player.ship, e.target_slot,
                                                 aoe=params['nearby'],
                                                 only_alive=True):
                battle.queue_event(BattleEvent(
                    name=EventName.FIRE_DELAY_MUL,
                    value=1.0 / params['fire_delay_multiplier'],
                    source_slot=e.target_slot,
                    target_slot=target_slot,
                    source_player=e.target_player,
                    target_player=e.target_player
                ))


@MECHANICS.register(Mechanics.HEALTH_AURA)
class HealthAura(Mechanic):
    PARAM_NEARBY = 'nearby'
    PARAM_HP_MULTIPLIER = 'hp_multiplier'
    PARAM_PERMANENT = 'permanent'

    def on_battle_event_fired(self, battle, e: BattleEvent, params: dict):
        if e.name == EventName.MODULE_INIT:
            for target_slot in iter_nearby_slots(e.source_player.ship,
                                                 e.source_slot,
                                                 aoe=params[self.PARAM_NEARBY],
                                                 only_alive=True):
                battle.queue_event(BattleEvent(
                    name=EventName.HP_MUL,
                    value=params[self.PARAM_HP_MULTIPLIER],
                    source_slot=e.source_slot,
                    target_slot=target_slot,
                    source_player=e.source_player,
                    target_player=e.source_player
                ))
        if e.name == EventName.DEATH:
            if params[self.PARAM_PERMANENT]:
                # Skip on-death discard events if buff is permanent
                return

            for target_slot in iter_nearby_slots(e.target_player.ship, e.target_slot,
                                                 aoe=params[self.PARAM_NEARBY],
                                                 only_alive=True):
                battle.queue_event(BattleEvent(
                    name=EventName.HP_MUL,
                    value=1.0 / params[self.PARAM_HP_MULTIPLIER],
                    source_slot=e.target_slot,
                    target_slot=target_slot,
                    source_player=e.target_player,
                    target_player=e.target_player
                ))


@MECHANICS.register(Mechanics.ATTRACTION)
class Attraction(Mechanic):
    PARAM_NEARBY = 'nearby'

    def on_battle_event_fired(self, battle, e: BattleEvent, params: dict):
        if e.name == EventName.MODULE_INIT:
            nearby_p = [s.priority for s in iter_nearby_slots(e.source_player.ship,
                                                              e.source_slot,
                                                              aoe=params[self.PARAM_NEARBY],
                                                              only_alive=True)]
            if nearby_p:
                highest_nearby_priority = max(nearby_p)
                additional = max(highest_nearby_priority, e.source_slot.priority)
                battle.queue_event(BattleEvent(
                    name=EventName.PRIORITY_ADD,
                    value=additional,
                    source_slot=e.source_slot,
                    target_slot=e.source_slot,
                    source_player=e.source_player,
                    target_player=e.source_player
                ))


@MECHANICS.register(Mechanics.SUMMON_NEARBY)
class SummonNearby(Mechanic):
    PARAM_MODULE = 'module'
    PARAM_NEARBY = 'nearby'

    def on_battle_event_fired(self, battle, e: BattleEvent, params: dict):
        if e.name == EventName.FIRE:
            slots_to_choose_from = []
            for slot in iter_nearby_slots(e.source_player.ship,
                                          aoe=params[self.PARAM_NEARBY],
                                          reference=e.source_slot,
                                          only_alive=False):
                if not slot.has_module:
                    slots_to_choose_from.append(slot)

            if slots_to_choose_from:
                slot = random.choice(slots_to_choose_from)
                slot.module = ModuleState.from_meta(params[self.PARAM_MODULE], slot)

                battle.queue_event(BattleEvent(
                    name=EventName.MODULE_INIT,
                    source_slot=e.source_slot,
                    target_slot=slot,
                    source_player=e.source_player,
                    target_player=e.target_player
                ))


@MECHANICS.register(Mechanics.STUN_TARGET_ON_HIT)
class StunTargetOnHit(Mechanic):
    PARAM_DURATION = 'duration'

    def on_battle_event_fired(self, battle, e: BattleEvent, params: dict):
        if e.name == EventName.HIT:
            battle.queue_event(BattleEvent(
                name=EventName.STUN,
                source_slot=e.source_slot,
                target_slot=e.target_slot,
                source_player=e.source_player,
                target_player=e.target_player,
                value=params[self.PARAM_DURATION]
            ))


def same_row_targeting(slot, targets: list, trait_params: dict):
    additional_priority = trait_params['additional_priority']
    row = slot.meta.position_y
    for target in targets:
        if target.slot.meta.position_y == (5 - row) + 1:
            target.priority += additional_priority
    return targets[0]
