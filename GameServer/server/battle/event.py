from dataclasses import dataclass


from model import PlayerState, SlotState, Event, EventTarget


__all__ = ['BattleEvent', 'EventName']


class EventName:
    MODULE_INIT = 'module_init'  # triggered on spawn and on round start
    FIRE = 'fire'
    HIT = 'hit'
    DMG_MUL = 'dmg_mul'
    HP_MUL = 'hp_mul'
    FIRE_DELAY_MUL = 'fire_delay_mul'
    PRIORITY_ADD = 'priority_add'
    DEATH = 'death'
    HEAL = 'heal'
    STUN = 'stun'


@dataclass
class BattleEvent:
    name: EventName
    source_player: PlayerState
    source_slot: SlotState
    target_player: PlayerState
    target_slot: SlotState
    delay: float = 0
    value: float = 0

    def to_send_event(self) -> Event:
        e = Event()
        e.name = self.name
        e.value = self.value
        e.source = EventTarget(slot_id=self.source_slot.id, player_index=self.source_player.index)
        e.target = EventTarget(slot_id=self.target_slot.id, player_index=self.target_player.index)
        return e
