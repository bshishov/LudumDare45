import logging
import uuid
from enum import Enum
from typing import List, Optional
import os

import server.model as models
from server.model import Outcome
from server.meta import META, ModuleMeta, Mechanics
from server.player import Player
from server.battle.battle import Battle
from server.core.registry import Registry
from server.core.adapters import JsonLinesLogger


TIME_DELTA = META.config.game_update_delay
MESSAGE_HANDLERS = Registry()

BATTLE_LOG_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', 'battle_stats.jsonl')
BATTLE_LOG = JsonLinesLogger(BATTLE_LOG_PATH)


@MESSAGE_HANDLERS.register(models.C2SReady)
def on_c2s_ready(game: 'Game', player: Player, message: models.C2SReady):
    game.set_ready(player)


@MESSAGE_HANDLERS.register(models.C2SModuleBuy)
def on_c2s_slot_module_buy(game: 'Game', player: Player, message: models.C2SModuleBuy):
    state = game.get_state(player)

    module: ModuleMeta = META.get_module(message.module)
    slot: models.SlotState = state.ship.get_slot(message.slot)

    if not module:
        logging.warning(f'Can\'t buy module {message.module}: no such module')
        return

    if not slot:
        logging.warning(f'Can\'t buy module {message.module}: no such slot')
        return

    if not module.can_be_bought:
        logging.warning(f'Cant buy module {message.module}: module can\'t be bought')
        return

    if state.money < module.price:
        logging.warning(f'Cant buy module {message.module}: Insufficient funds')
        return

    if not slot.has_module:
        # Place the new module to the empty slow
        logging.debug(f'Created new module {message.module} in slot {slot.id}')
        slot.module = models.ModuleState.from_meta(module.id, slot)
    else:
        # Upgrade
        if slot.module.id != module.id:
            logging.warning(f'Cant buy module {message.module}: '
                            f'slot {slot.id} has module {slot.module.id}')
            return

        if not module.upgrades_to:
            logging.warning(f'Cant buy module {message.module}: '
                            f'slot module is not upgradable')
            return

        logging.debug(f'Upgraded module {slot.module.id} in slot {slot.id} '
                      f'to {module.upgrades_to}')
        slot.module = models.ModuleState.from_meta(module.upgrades_to, slot)

    update_money(player, state, delta=-module.price)
    player.send(models.S2CShipUpdated(ship=state.ship))


@MESSAGE_HANDLERS.register(models.C2SModuleSell)
def on_c2s_module_sell(game: 'Game', player: Player, message: models.C2SModuleSell):
    state = game.get_state(player)
    slot: models.SlotState = state.ship.get_slot(message.slot)

    if not slot:
        logging.warning(f'Can\'t sell module in slot {message.slot}: no such slot')
        return

    if not slot.has_module:
        logging.warning(f'Can\'t sell module in slot {message.slot}: slot is empty')
        return

    update_money(player, state, delta=slot.module.meta.price)
    slot.module = None
    player.send(models.S2CShipUpdated(ship=state.ship))


@MESSAGE_HANDLERS.register(models.C2SModuleMove)
def on_c2s_module_move(game: 'Game', player: Player, message: models.C2SModuleMove):
    state = game.get_state(player)
    slot_from: models.SlotState = state.ship.get_slot(message.slot_from)
    slot_to: models.SlotState = state.ship.get_slot(message.slot_to)

    if not slot_from or not slot_to:
        logging.warning(f'Can\'t move module from slot {message.slot_from} to {message.slot_to}: '
                        f'no such slot')
        return

    if not slot_from.has_module:
        logging.warning(f'Can\'t move module from slot {message.slot_from} to {message.slot_to}: '
                        f'slot is empty')
        return

    if not slot_to.has_module:
        # Move to empty slot
        slot_to.module = models.ModuleState.from_meta(slot_from.module.id, slot_to)
        slot_from.module = None
    else:
        # Move to non-empty slot
        module = slot_to.module.meta

        module_from_id = slot_from.module.id
        module_to_id = slot_to.module.id

        if module.upgrades_to and module_from_id == module_to_id:
            # Upgrades
            logging.debug(f'Upgraded module {slot_to.module.id} in slot {slot_to.id} '
                          f'to {module.upgrades_to}')
            slot_to.module = models.ModuleState.from_meta(module.upgrades_to, slot_to)
            slot_from.module = None
        else:
            # Swap modules
            slot_to.module = models.ModuleState.from_meta(module_from_id, slot_to)
            slot_from.module = models.ModuleState.from_meta(module_to_id, slot_from)

    player.send(models.S2CShipUpdated(ship=state.ship))


class Phase(Enum):
    PRE = 0
    BUILD = 2
    BATTLE = 3
    POST = 4


class Game(object):
    def __init__(self, player1: Player, player2: Player, queue_type: str):
        self.id = str(uuid.uuid4())[:8]
        self.can_be_removed = False
        self.queue_type = queue_type

        self.player1: Player = player1
        self.player2: Player = player2

        self.state1 = models.PlayerState(index=1)
        self.state2 = models.PlayerState(index=2)

        self.player1.game = self
        self.player2.game = self

        self.p1_skip_requested = False
        self.p2_skip_requested = False

        self.game_time = -META.pregame_time
        self.phase = Phase.PRE

        self._outcome_history: List[Outcome] = []
        self._game_iterator = self._game_loop()

        logging.info(f'Game {self.id} created: {self.player1} vs {self.player2}')
        self.player1.send(models.S2CGameCreated(id=self.id,
                                                player_index=1,
                                                opponent=self.player2.profile))
        self.player2.send(models.S2CGameCreated(id=self.id,
                                                player_index=2,
                                                opponent=self.player1.profile))

    def update(self):
        if self.can_be_removed:
            # Game is over, no need to update
            return

        try:
            next(self._game_iterator)
            self.game_time += TIME_DELTA
        except StopIteration:
            self.can_be_removed = True

    def on_player_message(self, player: Player, msg: models.Payload):
        logging.info(f'Handling message for player: {player}, message: {msg}')
        if self.phase == Phase.BUILD:
            handler = MESSAGE_HANDLERS.get(type(msg))
            if handler:
                handler(self, player, msg)
            else:
                logging.warning(f'Received unknown message type: {msg.MESSAGE_TYPE}')

    def get_state(self, player) -> Optional[models.PlayerState]:
        if player is self.player1:
            return self.state1
        elif player is self.player2:
            return self.state2
        else:
            logging.warning(f'No state for player: {player}')
            return None

    def end(self, outcome: Optional[Outcome] = None, interrupted: bool = False,
            interrupted_reason: str = None):
        if not interrupted:
            logging.info(f'Game {self.id} ended. {outcome}')
        else:
            logging.info(f'Game {self.id} interrupted. Reason: {interrupted_reason}')

        payload = models.S2CGameEnded(interrupted=interrupted,
                                      interrupted_reason=interrupted_reason)

        if outcome is not None:
            payload.winner = outcome.value

            is_draw = outcome == Outcome.DRAW
            if outcome == Outcome.PLAYER1_WON:
                winner, loser = self.player1, self.player2
            else:
                # This case also covers DRAW
                winner, loser = self.player2, self.player1

            # Rating update
            w_rating, l_rating = models.GLICKO2_SYSTEM.rate_1vs1(
                winner.profile.glicko2_rating,
                loser.profile.glicko2_rating, drawn=is_draw)
            winner.profile.glicko2_rating = w_rating
            loser.profile.glicko2_rating = l_rating

            # Stats
            if is_draw:
                winner.profile.games_tied += 1
                loser.profile.games_tied += 1
            else:
                winner.profile.games_won += 1
                loser.profile.games_lost += 1
                winner.profile.games_streak = max(winner.profile.games_streak + 1, 1)
                loser.profile.games_streak = max(loser.profile.games_streak - 1, -1)

            winner.profile.games_played += 1
            loser.profile.games_played += 1

            winner.profile.save()
            loser.profile.save()
        else:
            payload.winner = 0

        payload.player1_profile = self.player1.profile
        payload.player2_profile = self.player2.profile

        if self.player1.is_connected:
            self.player1.send(payload)
        if self.player2.is_connected:
            self.player2.send(payload)
        self.can_be_removed = True

    def _game_loop(self):
        yield from self._wait(META.pregame_time)
        self.send_all(models.S2CGameStarted())
        logging.info(f'Game {self.id} started')

        # Initial funds
        update_money(self.player1, self.state1, META.start_money)
        update_money(self.player2, self.state2, META.start_money)

        # Create default ship states for the round and notify players
        self.state1.ship = create_ship_state(META.ship)
        self.state2.ship = create_ship_state(META.ship)
        self.player1.send(models.S2CShipUpdated(ship=self.state1.ship))
        self.player2.send(models.S2CShipUpdated(ship=self.state2.ship))

        match_outcome = None
        for round_number, r in enumerate(META.rounds):
            """ ROUND STARTED """
            round_number = round_number + 1
            logging.debug(f'Round {round_number} started')
            self.send_all(models.S2CRoundStarted(round_number=round_number, round=r))
            round_started_time = self.game_time

            # Reset ship states
            if META.state_after_round == 'reset':
                reset_ship_state(self.state1.ship)
                reset_ship_state(self.state2.ship)
            elif META.state_after_round == 'clear':
                clear_ship_state(self.state1.ship)
                clear_ship_state(self.state2.ship)
            self.player1.send(models.S2CShipUpdated(ship=self.state1.ship))
            self.player2.send(models.S2CShipUpdated(ship=self.state2.ship))

            """ BUILD PHASE """
            logging.debug('Entering build phase')
            self.phase = Phase.BUILD

            # Notify players
            self.player1.send(models.S2CRoundBuildPhase(ship=self.state1.ship))
            self.player2.send(models.S2CRoundBuildPhase(ship=self.state2.ship))

            # Wait for confirmation (players should confirm ship layouts before timer ends)
            yield from self._wait(r.build_phase_time)

            """ BATTLE PHASE """
            logging.debug('Entering battle phase')
            self.phase = Phase.BATTLE

            # Notify players
            self.send_all(models.S2CRoundBattlePhase(player1_ship=self.state1.ship,
                                                     player2_ship=self.state2.ship))

            yield from self._wait(r.pre_battle_time)
            battle = Battle(self.state1, self.state2)
            battle.begin()

            # Auto battle calculation
            while True:
                outcome, events = battle.update()

                if events or outcome is not None:
                    e = models.S2CGameEvent()
                    e.time = self.game_time
                    e.events = events
                    e.player1_ship = battle.p1.ship
                    e.player2_ship = battle.p2.ship
                    self.send_all(e)

                # If game is stale - finish match with draw
                if (self.game_time - round_started_time) > r.max_time:
                    outcome = Outcome.DRAW

                # Match finished
                if outcome is not None:
                    BATTLE_LOG.log({
                        'meta_version': META.version,
                        'round': round_number,
                        'queue_type': self.queue_type,
                        'outcome': outcome.value,
                        'time': self.game_time - round_started_time,
                        'p1_ship': self.state1.ship.to_dict(),
                        'p2_ship': self.state2.ship.to_dict(),
                    })
                    break

                yield

            yield from self._wait(r.post_battle_time)

            """ ROUND ENDED """
            logging.debug(f'Round {round_number} ended, outcome: {outcome}')
            self._outcome_history.append(outcome)
            self.send_all(models.S2CRoundEnded(
                round_number=round_number,
                winner=outcome.value,
                history=[o.value for o in self._outcome_history]))

            update_money(self.player1, self.state1, r.money_gain)
            update_money(self.player2, self.state2, r.money_gain)

            gold_gain_mechanic(self.player1, self.state1)
            gold_gain_mechanic(self.player2, self.state2)

            yield from self._wait(r.post_round_time)

            match_outcome = self._match_outcome()
            if match_outcome is not None:
                break

        self.phase = Phase.POST

        if match_outcome is not None:
            self.end(match_outcome)
        else:
            self.end(match_outcome, interrupted=True,
                     interrupted_reason='Can\'t determine match outcome')

    def _match_outcome(self) -> Optional[Outcome]:
        if len(self._outcome_history) < META.should_win:
            return None

        player1_score = 0
        player2_score = 0
        for o in self._outcome_history:
            if o == Outcome.PLAYER1_WON or o == Outcome.DRAW:
                player1_score += 1
            if o == Outcome.PLAYER2_WON or o == Outcome.DRAW:
                player2_score += 1
            if player1_score >= META.should_win and player2_score >= META.should_win:
                return Outcome.DRAW
            if player1_score >= META.should_win:
                return Outcome.PLAYER1_WON
            if player2_score >= META.should_win:
                return Outcome.PLAYER2_WON
        return None

    def send_all(self, message: models.Payload):
        self.player1.send(message)
        self.player2.send(message)

    def _wait(self, seconds: float):
        self.p1_skip_requested = False
        self.p2_skip_requested = False
        while seconds > 0:
            if self.p1_skip_requested and self.p2_skip_requested:
                break
            seconds -= TIME_DELTA
            yield

    def set_ready(self, player: Player):
        player_index = None
        if self.player1 == player and not self.p1_skip_requested:
            self.p1_skip_requested = True
            player_index = self.state1.index
        if self.player2 == player and not self.p2_skip_requested:
            self.p2_skip_requested = True
            player_index = self.state2.index
        if player_index:
            self.send_all(models.S2CPlayerReady(player_index=player_index))


def update_money(player: Player, state: models.PlayerState, delta: int):
    state.money += delta
    player.send(models.S2CMoneyUpdated(delta=delta, new_amount=state.money))


def create_ship_state(ship_id: str) -> Optional[models.ShipState]:
    ship_meta = META.get_ship(ship_id)
    if ship_meta is None:
        logging.warning(f'No such ship {ship_id}')
        return None
    ship_state = models.ShipState(id=ship_id, slots=[])
    for slot_meta in ship_meta.slots:
        slot = models.SlotState()
        slot.id = slot_meta.id
        slot.line = slot_meta.line
        slot.priority = slot_meta.priority
        slot.module = None
        ship_state.slots.append(slot)
    return ship_state


def reset_ship_state(state: models.ShipState):
    for slot in state.slots:
        if slot.has_module:
            slot.module = models.ModuleState.from_meta(slot.module.id, slot)


def clear_ship_state(state: models.ShipState):
    for slot in state.slots:
        if slot.has_module:
            slot.module = None


def gold_gain_mechanic(player: Player, state: models.PlayerState):
    gold_gained = 0
    for slot in state.ship.slots:
        if slot.has_module:
            module = slot.module
            gold_gain_trait = module.get_trait(mechanic_id=Mechanics.GOLD_GAIN_AFTER_ROUND)
            if gold_gain_trait:
                gold_gained += gold_gain_trait.params['amount']

    update_money(player, state, gold_gained)


def test_game():
    from unittest.mock import Mock

    logging.basicConfig(
        format='[%(levelname)s] %(asctime)-15s %(module)s %(funcName)s: %(message)s',
        level=logging.NOTSET)
    logging.root.setLevel(logging.NOTSET)

    socket = Mock()
    socket.open = True

    player_1 = Player(socket=socket)
    player_2 = Player(socket=socket)

    player_1.auth('player1_test', token='test_token_1')
    player_2.auth('player1_test', token='test_token_2')

    game = Game(player_1, player_2, queue_type='ranked_match')
    while not game.can_be_removed:
        logging.info(f'Game tick: {game.game_time}')
        game.update()


if __name__ == '__main__':
    test_game()
