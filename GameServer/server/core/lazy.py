from functools import wraps

__all__ = ['lazy_function', 'lazy_method', 'lazy_property']

__NOT_SET = object()


def lazy_function(fn):
    fn.__lazy = __NOT_SET

    @wraps(fn)
    def inner(*args, **kwargs):
        if fn.__lazy is __NOT_SET:
            result = fn(*args, **kwargs)
            fn.__lazy = result
            return result
        return fn.__lazy
    return inner


def lazy_method(fn):
    attr_name = f"__lazy_{fn.__name__}"

    @wraps(fn)
    def inner(self, *args, **kwargs):
        if not hasattr(self, attr_name):
            result = fn(self, *args, **kwargs)
            setattr(self, attr_name, result)
            return result
        return getattr(self, attr_name)
    return inner


def lazy_property(fn):
    attr_name = f'__lazy_{fn.__name__}'

    @property
    def _lazy_property(self):
        if not hasattr(self, attr_name):
            result = fn(self)
            setattr(self, attr_name, result)
            return result
        return getattr(self, attr_name)
    return _lazy_property


def test_lazy():
    @lazy_function
    def increment_by_key(o, key):
        o[key] = o.get(key) + 1
        return o[key]

    obj = {'foo': 0}
    assert increment_by_key(obj, 'foo') == 1
    assert obj.get('foo') == 1
    assert increment_by_key(obj, 'foo') == 1
    assert obj.get('foo') == 1
