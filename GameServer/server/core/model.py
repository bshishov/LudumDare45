import json
from typing import Any, List, Dict

__all__ = [
    'Field',
    'ObjectField',
    'ListCollectionField',
    'DictCollectionField',
    'DuplicateFieldDefinition',
    'ModelMeta',
    'Model',
    'JsonSerializableModel',
    'ProxyField'
]

_NOT_PROVIDED = object()


class Field:
    __slots__ = 'name', 'default', '_attr_name', 'allow_empty', 'allow_none', 'validator'

    def __init__(self, name: str,
                 default=_NOT_PROVIDED,
                 allow_empty: bool = False,
                 allow_none: bool = False,
                 validator=None):
        assert name and isinstance(name, str), 'String name must be specified'
        self.name = name
        self.default = default
        self.allow_empty = allow_empty
        self.allow_none = allow_none
        self.validator = validator
        if self.default is None and not self.allow_none:
            raise RuntimeError(f'Default is None but None is not allowed for '
                               f'field \'{self.name}\'. Check field settings')
        self._attr_name = f'__data_{name}'

    def __get__(self, model_instance, owner: type) -> Any:
        assert isinstance(model_instance, Model)
        if self._attr_name in model_instance._state:
            return model_instance._state[self._attr_name]
        elif self.default is not _NOT_PROVIDED:
            default = self.default
            if callable(default):
                default = default()
            self.__set__(model_instance, default)
            return default
        raise AttributeError(f'Field \'{self.name}\' of model \'{owner.__name__}\' is not set')

    def __set__(self, model_instance, value):
        assert isinstance(model_instance, Model)
        if value is None and not self.allow_none:
            raise AttributeError(f'Field \'{self.name}\' of model '
                                 f'\'{model_instance.__class__.__name__}\' cannot be None')
        model_instance._state[self._attr_name] = value

    def __delete__(self, model_instance):
        assert isinstance(model_instance, Model)
        del model_instance._state[self._attr_name]

    def serialize(self, model_instance: 'Model') -> Any:
        stored_value = self.__get__(model_instance, model_instance.__class__)
        return stored_value

    def deserialize(self, model_instance: 'Model', value):
        self.__set__(model_instance, value)

    def has_default(self) -> bool:
        return self.default is not _NOT_PROVIDED

    def has_value(self, model_instance: 'Model'):
        return self._attr_name in model_instance._state

    def validate(self, model_instance: 'Model', raise_on_error=False) -> bool:
        if self.validator:
            value = self.__get__(model_instance, model_instance.__class__)
            if not self.validator.validate(model_instance, self, value):
                if raise_on_error:
                    raise AttributeError(
                        f'Field {self.name} of model {model_instance.__class__.__name__} '
                        f'is not valid. Validator: {self.validator.__class__.__name__}. '
                        f'Got value: {value}')
                return False
        return True

    def clear(self, model_instance):
        self.__delete__(model_instance)

    def __repr__(self):
        return f'<Field(name={self.name})'


class ProxyField(Field):
    def __init__(self, attr_name: str, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._attr_name = attr_name

    def __get__(self, instance, owner):
        return getattr(instance, self._attr_name)

    def has_value(self, model_instance: 'Model'):
        return True


class ObjectField(Field):
    __slots__ = '_model'

    def __init__(self, name: str, model: type, *args, **kwargs):
        super().__init__(name, *args, **kwargs)
        assert issubclass(model, Model)
        self._model = model

    def serialize(self, model_instance: 'Model') -> Any:
        value = super().serialize(model_instance)
        if value is not None:
            return value.serialize()
        return None

    def deserialize(self, instance: 'Model', value):
        if value is not None:
            obj = self._model()
            obj.deserialize(value)
            super().deserialize(instance, obj)

    def validate(self, model_instance: 'Model', raise_on_error=False) -> bool:
        base_validation_result = super().validate(model_instance, raise_on_error)
        if not base_validation_result:
            return base_validation_result
        value: Model = self.__get__(model_instance, model_instance.__class__)
        return value.validate(raise_on_error)


class ListCollectionField(Field):
    __slots__ = '_model'

    def __init__(self, name: str, item_model: type, *args, **kwargs):
        super().__init__(name, *args, **kwargs)
        assert issubclass(item_model, Model)
        self._model = item_model

    def serialize(self, model_instance: 'Model') -> Any:
        value = super().serialize(model_instance)
        return [v.serialize() for v in value]

    def deserialize(self, instance: 'Model', value):
        deserialized_list = []
        for v in value:
            obj = self._model()
            obj.deserialize(v)
            deserialized_list.append(obj)
        super().deserialize(instance, deserialized_list)

    def validate(self, model_instance: 'Model', raise_on_error=False) -> bool:
        base_validation_result = super().validate(model_instance, raise_on_error)
        if not base_validation_result:
            return base_validation_result
        value: List[Model] = self.__get__(model_instance, model_instance.__class__)
        for item in value:
            if not item.validate(raise_on_error):
                return False
        return True


class DictCollectionField(Field):
    __slots__ = '_model', '_dict_factory'

    def __init__(self, name: str, item_model: type, dict_factory: callable = dict,
                 *args, **kwargs):
        super().__init__(name, *args, **kwargs)
        assert issubclass(item_model, Model)
        self._model = item_model
        self._dict_factory = dict_factory

    def serialize(self, model_instance: 'Model') -> Any:
        value = super().serialize(model_instance)
        return {k: v.serialize() for k, v in value.items()}

    def deserialize(self, instance: 'Model', value):
        deserialized_dict = self._dict_factory()
        for k, v in value.items():
            obj = self._model()
            obj.deserialize(v)
            deserialized_dict[k] = obj
        super().deserialize(instance, deserialized_dict)

    def validate(self, model_instance: 'Model', raise_on_error=False) -> bool:
        base_validation_result = super().validate(model_instance, raise_on_error)
        if not base_validation_result:
            return base_validation_result
        value: Dict[Any, Model] = self.__get__(model_instance, model_instance.__class__)
        for item in value.values():
            if not item.validate(raise_on_error):
                return False
        return True


class DuplicateFieldDefinition(AttributeError):
    def __init__(self, field_name, class_name):
        super().__init__(f'Duplicate field definition found during {class_name} initialization, '
                         f'field: {field_name}')


class ModelMeta(type):
    FIELDS_ATTR = 'fields'

    def __new__(mcs, name, bases, attrs):
        fields = attrs.get(ModelMeta.FIELDS_ATTR, {})
        for base in bases:
            if hasattr(base, ModelMeta.FIELDS_ATTR):
                base_fields = getattr(base, ModelMeta.FIELDS_ATTR)
                for field_name, field in base_fields.items():
                    if field_name in fields:
                        raise DuplicateFieldDefinition(field_name, name)
                    fields[field_name] = field

        for attr_name, attr in attrs.items():
            if isinstance(attr, Field):
                if attr.name in fields:
                    raise DuplicateFieldDefinition(attr.name, name)
                fields[attr.name] = attr

        attrs[ModelMeta.FIELDS_ATTR] = fields
        return super(ModelMeta, mcs).__new__(mcs, name, bases, attrs)


class Model(metaclass=ModelMeta):
    __slots__ = '_state', '_dict_factory'

    fields: Dict[str, Field]  # This class variable is autopropagated during class construction

    def __init__(self, dict_factory: callable = dict, **kwargs):
        self._state = {}
        self._dict_factory = dict_factory
        for attr_name, attr_value in kwargs.items():
            if attr_name not in self.fields:
                raise AttributeError(f'Unexpected argument: {attr_name}, '
                                     f'no such field in model {self.__class__.__name__}')
            setattr(self, attr_name, attr_value)

    def validate(self, raize=False) -> bool:
        for field in self.fields.values():
            if not field.validate(self, raize):
                return False
        return True

    def deserialize(self, data: Dict[str, Any]):
        for key, value in data.items():
            field = self.fields.get(key, None)
            if field:
                field.deserialize(self, value)

    def serialize(self) -> Dict[str, Any]:
        result = self._dict_factory()
        for field in self.fields.values():
            if field.has_default() or field.has_value(self):
                result[field.name] = field.serialize(self)
            else:
                if not field.allow_empty:
                    raise KeyError(f'Field {field.name} of '
                                   f'model {self.__class__.__name__} '
                                   f'cannot be empty')
        return result

    # Aliases
    to_dict = serialize
    from_dict = deserialize

    @property
    def raw(self) -> Dict[str, Any]:
        return self.serialize()

    def clear(self):
        for field in self.fields.values():
            field.clear(self)


class JsonSerializableModel(Model):
    def to_json(self) -> str:
        data = self.serialize()
        return json.dumps(data)

    def from_json(self, raw: str):
        data = json.loads(raw)
        self.deserialize(data)

    def from_json_file(self, file):
        data = json.load(file)
        self.deserialize(data)
