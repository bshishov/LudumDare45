from typing import Any, Optional, Dict


import pickledb
import json
import datetime
from threading import Lock


class Adapter:
    def get(self, key: str) -> Optional[Dict[str, Any]]:
        raise NotImplementedError

    def set(self, key: str, obj: Dict[str, Any]) -> bool:
        raise NotImplementedError


class PickleDbAdapter(Adapter):
    def __init__(self, path: str, auto_dump: bool = True):
        self.db = pickledb.load(path, auto_dump=auto_dump)

    def get(self, key: str) -> Optional[Dict[str, Any]]:
        return self.db.get(key=key)

    def set(self, key: str, obj: Dict[str, Any]):
        return self.db.set(key, obj)


class JsonLinesLogger:
    def __init__(self, path: str):
        self.path = path
        self.lock = Lock()

    def log(self, data: Dict[str, Any]):
        with self.lock:
            with open(self.path, 'a', encoding='utf-8') as fp:
                json.dump({
                    'timestamp': datetime.datetime.now().timestamp(),
                    **data,
                }, fp, indent=None)
                fp.write('\n')
