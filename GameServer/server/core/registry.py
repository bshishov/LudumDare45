class Registry(dict):
    def register(self, key):
        def _decorator(obj):
            self[key] = obj
            return obj
        return _decorator


class SingletonRegistry(dict):
    def register(self, key, *args, **kwargs):
        def _decorator(obj):
            self[key] = obj(*args, **kwargs)
            return obj
        return _decorator


def test_registry_function():
    registry = Registry()

    assert len(registry) == 0
    assert registry.get('handler1') is None

    @registry.register('handler1')
    def some_handler():
        return 42

    assert len(registry) == 1
    assert registry.get('handler1') == some_handler
    assert registry['handler1']() == 42


def test_class_method():
    registry = Registry()

    assert len(registry) == 0
    assert registry.get('handler1') is None

    class A:
        @registry.register('handler1')
        def some_handler(self):
            return 42

    assert len(registry) == 1
    assert registry.get('handler1') == A.some_handler
    assert registry['handler1'](A()) == 42


def test_class():
    registry = Registry()

    assert len(registry) == 0
    assert registry.get('A') is None

    @registry.register('A')
    class A:
        @classmethod
        def some_handler(cls):
            return 42

    assert len(registry) == 1
    assert registry.get('A') == A
    assert registry['A'].some_handler() == 42
