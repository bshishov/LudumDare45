from typing import List, Dict, Optional, Any, Set
import json
import asyncio
import logging


from server.player import Player
from server.game import Game
from server.model import *
from server.meta import META
from server.bot import BotPlayer
from server.monitoring import *

__all__ = [
    'stop_matchmaking',
    'start_matchmaking',
    'update_games',
    'matchmaking_update',
    'on_connected',
    'on_message',
    'on_disconnected',
    'QueueType'
]


logger = logging.getLogger(__name__)

MATCHMAKING_PLAYERS: List[Player] = []
GAMES: List[Game] = []
CONNECTED_PLAYERS: Set[Player] = set()


def start_matchmaking(player: Player, queue_type: str):
    if player not in MATCHMAKING_PLAYERS:
        player.matchmaking_queue_type = queue_type
        MATCHMAKING_PLAYERS.append(player)
        logging.info(f'Added player {player} to matchmaking queue {queue_type}')
        player.send(S2CStartedMatchMaking(
            active_players=len(MATCHMAKING_PLAYERS),
            queue_type=queue_type
        ))
    else:
        logging.debug(f'Player {player} is already in matchmaking queue')


def stop_matchmaking(player: Player):
    if player in MATCHMAKING_PLAYERS:
        MATCHMAKING_PLAYERS.remove(player)
        player.send(S2CStoppedMatchMaking(reason='Cancelled by player'))
        logging.info(f'Player {player} exited matchmaking queue')
    else:
        logging.warning(f'Player {player} attempted an exit, but he is not in matchmaking queue')


async def on_message(player: Player, msg: Dict[str, Any]):
    msg_type = msg.get('type')
    payload_raw = msg.get('payload')

    payload_class = C2S_MESSAGES.get(msg_type)
    if not payload_class:
        INVALID_MESSAGE_ERRORS.inc()
        logging.debug(f'Unknown C2S message type: {msg_type}')
        return

    try:
        msg: Payload = payload_class()
        if payload_raw:
            msg.from_json(payload_raw)
        if not msg.validate():
            INVALID_MESSAGE_ERRORS.inc()
            logging.warning(f'Received invalid message: {msg}')
            if not player.logged_in:
                await player.disconnect()
            return
        logging.debug(f'Received message: {msg}')
    except Exception as err:
        INVALID_MESSAGE_ERRORS.inc()
        logging.error(f'Invalid message: msg_type: {msg_type}, payload_raw: {payload_raw}',
                      exc_info=True)
        if not player.logged_in:
            await player.disconnect()
        return

    if player.logged_in:
        if isinstance(msg, C2SStartMatchmaking):
            start_matchmaking(player, msg.queue_type)
        elif isinstance(msg, C2SStopMatchmaking):
            stop_matchmaking(player)
        else:
            if player.game is not None:
                player.game.on_player_message(player, msg)
    else:
        if isinstance(msg, C2SHello):
            if msg.version == META.version:
                logging.debug(f'Authorizing player: {msg.name} with token {msg.token}')
                player.auth(msg.name, msg.token)
                if player.logged_in:
                    player.send(S2CHello(profile=player.profile, meta=META,
                                         auth_token=player.profile.auth_token))
                else:
                    logging.debug(f'Failed to authorize player {msg.name} with token {msg.token}')
                    AUTH_ERRORS.inc()
                    await player.disconnect()
            else:
                VERSION_MISMATCH_ERRORS.inc()
                logging.warning(f'Player\'s {player} version={msg.version} and '
                                f'server version={META.version} mismatch')
                await player.disconnect()
        else:
            logging.debug(f'Player {player} sent message with type: {msg_type} '
                          f'but he is not logged in')
            await player.disconnect()


def on_connected(player: Player):
    logging.info(f'Player {player} is connected')
    CONNECTED_PLAYERS.add(player)
    ACTIVE_PLAYERS_METRIC.inc()


def on_disconnected(player: Player):
    stop_matchmaking(player)
    logging.info(f'Player {player} is disconnected')
    CONNECTED_PLAYERS.remove(player)
    ACTIVE_PLAYERS_METRIC.dec()


def matchmaking_try_create_game(players: List[Player]) -> Optional[Game]:
    for human_player in players:
        if human_player.matchmaking_queue_type == QueueType.BOT_MATCH:
            players.remove(human_player)
            return Game(human_player, BotPlayer(), QueueType.BOT_MATCH)

    if len(players) < 2:
        return None

    humans = []
    online_bots = []
    for player in players:
        if player.name.startswith('[BOT]'):
            online_bots.append(player)
        else:
            humans.append(player)

    # Humans have higher priority
    sorted_players = humans + online_bots

    player1 = sorted_players[0]
    player2 = sorted_players[1]

    players.remove(player1)
    players.remove(player2)

    return Game(player1, player2, QueueType.RANKED_MATCH)


async def matchmaking_update():
    while True:
        try:
            await asyncio.sleep(META.config.matchmaking_update_delay)

            # Try create as many games as possible, and try again after some time
            while True:
                game = matchmaking_try_create_game(MATCHMAKING_PLAYERS)
                if game is not None:
                    GAMES.append(game)
                else:
                    break

            QUEUED_PLAYERS_METRIC.set(len(MATCHMAKING_PLAYERS))

            # Notifying all the waiters about the current matchmaking state
            if MATCHMAKING_PLAYERS:
                matchmaking_state = S2CMatchmakingUpdate()
                matchmaking_state.players_in_matchmaking = len(MATCHMAKING_PLAYERS)
                matchmaking_state.players_in_battle = len([p for p in CONNECTED_PLAYERS
                                                           if p.game is not None])
                matchmaking_state.players_total = len(CONNECTED_PLAYERS)

                for queued_player in MATCHMAKING_PLAYERS:
                    queued_player.send(matchmaking_state)

        except Exception as err:
            logging.exception(err, exc_info=True)


async def update_games():
    global GAMES  # type: List[Game]
    while True:
        await asyncio.sleep(META.config.game_update_delay)
        for game in GAMES:
            try:
                if not game.can_be_removed:
                    if not game.player1.is_connected:
                        game.end(interrupted=True, interrupted_reason='Player 1 disconnected')
                        game.player1.profile.games_left += 1
                        game.player1.profile.save()
                    elif not game.player2.is_connected:
                        game.end(interrupted=True, interrupted_reason='Player 2 disconnected')
                        game.player2.profile.games_left += 1
                        game.player2.profile.save()
                    else:
                        game.update()
            except Exception as err:
                logging.exception(err, exc_info=True)
                GAME_UPDATE_ERRORS.inc()
                try:
                    game.end(interrupted=True, interrupted_reason='Server error')
                except Exception as err_inner:
                    logging.exception(err_inner, exc_info=True)
                    game.can_be_removed = True

        # Tear down finished games
        for game in GAMES.copy():
            if game.can_be_removed:
                game.player1.game = None
                game.player2.game = None
                GAMES.remove(game)

        ACTIVE_GAMES_METRIC.set(len(GAMES))
