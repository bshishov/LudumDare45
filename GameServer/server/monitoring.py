from prometheus_client import Counter, Gauge

__all__ = [
    'ACTIVE_GAMES_METRIC',
    'ACTIVE_PLAYERS_METRIC',
    'QUEUED_PLAYERS_METRIC',
    'GAME_UPDATE_ERRORS',
    'INVALID_MESSAGE_ERRORS',
    'AUTH_ERRORS',
    'VERSION_MISMATCH_ERRORS',
    'WEBSOCKET_MESSAGES_RECEIVED',
    'WEBSOCKET_MESSAGES_SENT'
]


ACTIVE_PLAYERS_METRIC = Gauge(
    name='active_players',
    documentation='Number of active players'
)

ACTIVE_GAMES_METRIC = Gauge(
    name='active_games',
    documentation='Number of active games'
)

QUEUED_PLAYERS_METRIC = Gauge(
    name='queued_players',
    documentation='Number of players in queue'
)

GAME_UPDATE_ERRORS = Counter(
    name='game_update_errors_total',
    documentation='Number of errors during game update'
)

INVALID_MESSAGE_ERRORS = Counter(
    name='invalid_message_errors_total',
    documentation='Invalid message format'
)

AUTH_ERRORS = Counter(
    name='auth_errors_total',
    documentation='Number of errors during game update'
)

VERSION_MISMATCH_ERRORS = Counter(
    name='version_mismatch_errors_total',
    documentation='Increases when client attempts a connection with a different version'
)


WEBSOCKET_MESSAGES_RECEIVED = Counter(
    name='websocket_messages_received_total',
    documentation='Number of incoming websocket messages'
)

WEBSOCKET_MESSAGES_SENT = Counter(
    name='websocket_messages_sent_total',
    documentation='Number of outgoing websocket messages'
)
