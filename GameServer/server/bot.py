import json
import logging
import websockets
import asyncio
import random
import enum
from typing import Optional
from unittest import mock

from server.model import *
from server.meta import *
from server.player import Player


BOT_TOKEN = 'bot_token_123'
BOT_NAMES = [
    'Bi$marck',
    'BOaT',
    'Unsinkable III',
    'Admiral Breadbeard',
    'Floaty MacBoat',
    'Anyone the Second',
    'DuckHunter3000',
    'igorek_asassin_2010',
    'Row_row_my_trusty_boat',
    'SuperPirateCop_3D',
    'MachineHeartJelly',
    'WaterElon',
    'FineApple',
    'SunShaker-MoneyMaker',
    'Ok_boomerang',
    'Beany Bo',
    'Surf_to_Win',
    'Fruitless Harvest'
]


class BotState(enum.Enum):
    IN_LOBBY = 0
    IN_MATCHMAKING = 1
    IN_GAME = 2
    BUILD_PHASE = 3
    BATTLE_PHASE = 4


class BotLogic:
    def __init__(self):
        self.shop = [m for m in META.modules if m.can_be_bought]
        self.ship: Optional[ShipState] = None
        self.money: int = 0
        self.state = BotState.IN_GAME

    def handle(self, msg: Payload) -> Optional[Payload]:
        if isinstance(msg, S2CGameEnded):
            self.state = BotState.IN_LOBBY
        elif isinstance(msg, S2CGameCreated):
            self.state = BotState.IN_GAME
        elif isinstance(msg, S2CRoundStarted):
            self.state = BotState.IN_GAME
        elif isinstance(msg, S2CRoundEnded):
            self.state = BotState.IN_GAME
        elif isinstance(msg, S2CRoundBuildPhase):
            self.state = BotState.BUILD_PHASE
        elif isinstance(msg, S2CRoundBattlePhase):
            self.state = BotState.BATTLE_PHASE
        elif isinstance(msg, S2CStoppedMatchMaking):
            self.state = BotState.IN_LOBBY
        elif isinstance(msg, S2CStartedMatchMaking):
            self.state = BotState.IN_MATCHMAKING
        elif isinstance(msg, S2CMoneyUpdated):
            self.money = msg.new_amount
        elif isinstance(msg, S2CShipUpdated):
            self.ship = msg.ship

        if self.state == BotState.IN_LOBBY:
            return C2SStartMatchmaking()

        if self.state == BotState.BUILD_PHASE:
            if self.money > 0:
                logging.info(f'Bot has {self.money} money and is going shopping')
                for _ in range(20):
                    module_to_buy = random.choice(self.shop)
                    slot = find_best_slot_for_module(self.ship, module_to_buy)
                    if slot and module_to_buy.price <= self.money:
                        logging.info(f'Bot decided to buy {module_to_buy.id} '
                                     f'and place it in slot {slot.id}')
                        self.money -= module_to_buy.price
                        return C2SModuleBuy(module=module_to_buy.id, slot=slot.id)
            else:
                return C2SReady()

        # Do nothing
        return None


class BotPlayer(Player):
    def __init__(self):
        socket = mock.Mock()
        socket.open = True
        socket.remote_address = ('local', 'bot')
        super().__init__(socket, is_bot=True)

        self.auth(f'[BOT] {random.choice(BOT_NAMES)}', BOT_TOKEN)
        self.logic = BotLogic()

    def send(self, message: Payload):
        try:
            if self.logic:
                response = self.logic.handle(message)
                if response:
                    self.send_to_game(response)
        except Exception as err:
            # Bot is not responding
            logging.exception(err)
            self.socket.open = False

    def send_to_game(self, message: Payload):
        if self.game is not None and message.validate():
            self.game.on_player_message(player=self, msg=message)


def serialize(message: Payload):
    return json.dumps({
        'type': message.MESSAGE_TYPE,
        'payload': message.to_json()
    })


def deserialize(raw_msg: str) -> Optional[Payload]:
    msg_dict = json.loads(raw_msg)

    msg_type = msg_dict.get('type')
    payload_raw = msg_dict.get('payload')

    payload_class = S2C_MESSAGES.get(msg_type)
    if not payload_class:
        logging.debug(f'Unknown C2S message type: {msg_type}')
        return None

    try:
        msg: Payload = payload_class()
        if payload_raw:
            msg.from_json(payload_raw)
            return msg
        logging.debug(f'Received message: {msg}')
    except Exception as err:
        logging.exception(f'Invalid message: msg_type: {msg_type}, payload_raw: {payload_raw}',
                          exc_info=True)
        return None


async def bot_loop(uri):
    try:
        await asyncio.sleep(1)
        logging.info(f'Bot is connecting to {uri}')
        async with websockets.connect(uri) as websocket:
            async def send(m: Payload):
                await websocket.send(serialize(m))

            logging.info(f'Bot connected')

            hello = C2SHello()
            hello.version = META.version
            hello.name = f'[BOT] {random.choice(BOT_NAMES)}'
            hello.token = BOT_TOKEN

            logic = BotLogic()
            logic.state = BotState.IN_LOBBY

            await send(hello)

            while True:
                response = await websocket.recv()
                msg = deserialize(response)
                if not msg:
                    continue

                response = logic.handle(msg)
                if response:
                    await send(response)
                await asyncio.sleep(0.5)
    except Exception as err:
        logging.exception(err, exc_info=True)


def find_best_slot_for_module(ship: ShipState, module: ModuleMeta) -> Optional[SlotState]:
    slots_copy: List[SlotState] = ship.slots.copy()
    random.shuffle(slots_copy)

    priorities = []
    for slot in slots_copy:
        if not slot.has_module:
            priorities.append((slot, 1))
        else:
            if slot.module.id == module.id and module.upgrades_to:
                priorities.append((slot, 2))

    if not priorities:
        return None

    slot, p = sorted(priorities, key=lambda p: p[1])[0]
    return slot
