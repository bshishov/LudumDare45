import argparse

from server.bot import BotPlayer
from server.game import Game
from server.app import QueueType


def main(args):
    print(f'n games: {args.n}')
    for i in range(args.n):
        print(f'Playing game #{i}')
        game = Game(BotPlayer(), BotPlayer(), QueueType.BOT_MATCH)
        while True:
            game.update()
            if game.can_be_removed:
                break


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--n', type=int, default=1000, required=False)
    main(parser.parse_args())
