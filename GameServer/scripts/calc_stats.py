import json
import pandas as pd

battle_log_path = 'battle_stats.jsonl'
rows = []

with open(battle_log_path, 'r', encoding='utf-8') as f:
    for line in f:
        rows.append(json.loads(line))

print(f'Loaded {len(rows)} rows')

modules = []


def ship_to_modules(battle_i, ship, player_index, outcome, round_number, round_time):
    ship_modules = []

    for slot in ship['slots']:
        raw_m = slot['module']
        if not raw_m:
            continue
        m_stats = raw_m['stats']
        m_died = m_stats['death_time'] > 0
        m = {
            'battle': battle_i,
            'id': raw_m['id'],
            'won': True if player_index == outcome else False,
            'draw': True if outcome == 0 else False,
            'player': player_index,
            'round': round_number,
            'damage': m_stats['damage_done'],
            'heal': m_stats['healing_done'],
            'stun': m_stats['total_stun'],
            'died': m_died,
            'lifespan': (m_stats['death_time'] - m_stats['init_time']) if m_died else round_time
        }
        ship_modules.append(m)
    return ship_modules


for battle_i, row in enumerate(rows):
    modules += ship_to_modules(battle_i, row['p1_ship'], 1, row['outcome'], row['round'],
                               row['time'])
    modules += ship_to_modules(battle_i, row['p2_ship'], 2, row['outcome'], row['round'],
                               row['time'])

df = pd.DataFrame(modules)
df.to_csv('battle_stats.csv')


# By round and id
df_by_round_and_id = (
    df[['id', 'round', 'damage', 'heal', 'stun', 'died', 'won', 'draw']]
    .groupby(['round', 'id'])
    .agg('mean')
    .style.format({
        'damage': '{:,.2f}'.format,
        'heal': '{:,.2f}'.format,
        'stun': '{:,.2f}'.format,
        'died': '{:,.2%}'.format,
        'won': '{:,.2%}'.format,
        'draw': '{:,.2%}'.format
    })
)
df_by_round_and_id.to_excel('by_round_and_id.xlsx', index=False)


# By id
by_id = (
    df[['id', 'damage', 'heal', 'stun', 'died', 'won', 'draw']]
    .groupby('id')
    .agg('mean')
    .reset_index()
    .sort_values('won', ascending=False)
    .style.format({
        'damage': '{:,.2f}'.format,
        'heal': '{:,.2f}'.format,
        'stun': '{:,.2f}'.format,
        'died': '{:,.2%}'.format,
        'won': '{:,.2%}'.format,
        'draw': '{:,.2%}'.format
    })
)

by_id.to_excel('by_id.xlsx', index=False)
